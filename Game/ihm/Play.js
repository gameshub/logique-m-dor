/**
 * @classdesc IHM pour tester les exercices non valides
 * @author Léo Niederhauser
 * @version 1.1
 */

class Play extends Interface {

    /**
     * Constructeur de l'ihm de la création d'exercice
     * @param {Game} refGame la référence vers la classe de jeu
     */

    constructor(refGame) {

        super(refGame, "play");

    }

    /**
    * Point d'entrée pour l'affichage
    */
    show() {
        //Modifier le texte du tutoriel
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText('play'));
        this.exeSelected = -1;
        //Vider les variables et le canvas
        this.clear();
        this.exercices = {};
        //Initialiser les éléments
        this.elements = {
            title: new PIXI.Text('', { fontFamily: 'Arial', fontSize: 16, fill: 0x000000, align: 'left' }),
            scrollPane: new ScrollPane(0, 60, 600, 480),
            btn: new Button(300, 570, 'Jouer', 0x0088FF, 0x000000, true)
        };
        //Ajouter un événement au bouton
        this.elements.btn.setOnClick(function () {

            if (this.exeSelected != -1) {
                this.refGame.trainMode.init(false, 'playMode');
                this.refGame.trainMode.setRefPlay(this);
                this.refGame.trainMode.show(this.exeSelected, this.exercice[this.exeSelected].exercice);
            }
        }.bind(this));
        //Valeur pour placer les éléments sur la canvas
        this.elements.title.anchor.set(0.5);
        this.elements.title.x = 300;
        this.elements.title.y = 30;
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }
    init() {
        this.refGame.global.util.hideTutorialInputs('btnReset');
        super.init();
    }

    /**
     * @desc Affiche les exercices dans le canvas
     * @param {string} lang la langue de l'application
     */
    startExercices(lang) {
        //Récupérer les exercices
        this.exercice = this.refGame.global.resources.getExercicesEleves();
        //Créer un groupe de boutons
        let tg = new ToggleGroup('single');
        //Test si les exercices récupérés sont vides
        if (this.exercice) {
            //Convertion du JSON en objet
            this.exercice = JSON.parse(this.exercice);
            //Parcourir les exercices
            for (let exID of Object.keys(this.exercice)) {
                //Récupérer l'exercice dans une variable
                let ex = this.exercice[exID];
                //Test si le JSON de l'exercice n'est pas vide
                if (ex.exercice) {
                    let label = (ex.name);
                    let btn = new ToggleButton(0, 0, label, false, 580);
                    btn.enable();
                    tg.addButton(btn);
                    btn.setOnClick(function () {
                        this.exeSelected = exID;
                    }.bind(this));
                    this.elements.scrollPane.addElements(btn);
                }

            }
            this.elements.scrollPane.init();
        }

    }


    ///////////////////////
    refreshLang(lang) {
        this.startExercices(lang)
    }



}