class Train extends Interface {

    constructor(refGame) {
        super(refGame, "train");
        this.refGame = refGame;
        this.evaluate = false;
        this.background = new PIXI.Container();
        this.background.width = 600;
        this.background.height = 600;
        this.numAttemps = 0;
        this.evaluate = false;
        this.level = null;
        this.title = new PIXI.Text('', { fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center' });
        this.currentText = 'gameTrain';
    }

    setRefPlay(refPlay) {
        this.refPlay = refPlay
    }


    show(evaluate, id, exercice) {

        this.evaluate = evaluate;
        this.level = this.refGame.global.resources.getExercice();
        if (exercice == undefined) {
            this.currentExerciceId = this.level.id;
            this.level = JSON.parse(this.level.exercice);
        } else {
            this.currentExerciceId = id;
            this.level = JSON.parse(JSON.stringify(exercice));
        }




        this.refGame.global.util.hideTutorialInputs('next', 'btnReset');
        this.clear();
        this.elements.splice(0, this.elements.length);
        this.initGame();
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
        this.setupSignalement();
    }

    initGame() {
        this.map = null;
        this.guide = null;
        if (this.evaluate) {
            this.refGame.global.statistics.addStats(2);
        }
        switch (this.level["map"]) {
            case "flatNature":
                this.map = new FlatNature(this.refGame, this.level);
                break;
            case "ocean":
                this.map = new Ocean(this.refGame, this.level);
                break;
            case "beach":
                this.map = new Beach(this.refGame, this.level);
                break;
        }

        switch (this.level["guides"][0]) {
            case "4Tokens":
                this.guide = new Guide4Tokens(this.refGame, this.level, this.endGame.bind(this));
                break;
            case "6Tokens":
                this.guide = new Guide6Tokens(this.refGame, this.level, this.endGame.bind(this));
                break;
            case "5TokensLoop":
                this.guide = new Guide5TokensLoop(this.refGame, this.level, this.endGame.bind(this));
                break;
        }

        this.elements.push(this.map, this.guide);
        this.end = false;
    }

    endGame(numAttemps) {
        this.numAttemps = numAttemps;
        if (!this.end) {
            let text = this.refGame.global.resources.getOtherText('victory', { count: this.numAttemps });
            this.end = true;
            //calcule le nombre de points
            if (this.evaluate) {
                let mark = 0;
                let barem = JSON.parse(this.refGame.global.resources.getEvaluation()).notes;
                for (let b of barem) {
                    if (b.min <= this.numAttemps && b.max >= this.numAttemps) {
                        mark = b.note;
                    }
                }
                this.refGame.global.statistics.updateStats(mark);
            }

            this.refGame.global.util.showAlert(
                //Type d'alerte
                'success',
                //Texte
                text,
                //Titre
                this.refGame.global.resources.getOtherText('success'),
                //Footer
                undefined,
                //Callback
                this.show(this.evaluate)
            );
        }
    }


    refreshLang(lang) {
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
    }

    setupSignalement() {
        this.refGame.inputs.signal.style.display = 'block';
        this.refGame.inputs.signal.onclick = function () {
            Swal.fire({
                title: this.refGame.global.resources.getOtherText('signalementTitle'),
                cancelButtonText: this.refGame.global.resources.getOtherText('signalementCancel'),
                showCancelButton: true,
                html:
                    '<label>' + this.refGame.global.resources.getOtherText('signalementReason') + '</label><textarea id="swal-reason" class="swal2-textarea"></textarea>' +
                    '<label>' + this.refGame.global.resources.getOtherText('signalementPassword') + '</label><input type="password" id="swal-password" class="swal2-input">',
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        resolve([
                            $('#swal-reason').val(),
                            $('#swal-password').val()
                        ])
                    })
                },
                onOpen: function () {
                    $('#swal-reason').focus()
                }
            }).then(function (result) {
                if (result && result.value) {
                    this.refGame.global.resources.signaler(result.value[0], result.value[1], this.currentExerciceId, function (res) {
                        Swal.fire(res.message, '', res.type);
                        if (res.type === 'success') {
                            this.init(this.evaluation);
                            this.show();
                        }
                    }.bind(this));
                }
            }.bind(this));
        }.bind(this);
    }
}