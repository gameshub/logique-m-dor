class Explore extends Interface {

    constructor(refGame) {

        super(refGame, "explore");
        this.refGame = refGame;
        this.currentText = 'gameExplore';
        this.background = new PIXI.Container();
        this.background.width = 600;
        this.background.height = 600;
        let img_background = refGame.global.resources.getImage("flat_nature");
        let sprite_background = PIXI.Sprite.fromImage(img_background.image.src);
        sprite_background.anchor.x = 0.5;
        sprite_background.y = 0;
        sprite_background.x = 300;
        let largeur = 350 / img_background.getHeight() * img_background.getWidth(); 
        sprite_background.height = 350;
        sprite_background.width = largeur;
        this.background.addChild(sprite_background);

        //add background
        this.elements.push(this.background);

        //colors
        this.blue = 0x4f81bd;
        this.green = 0x9bbb59;
        this.red = 0xc0504d;
        this.black = 0x000000;

        this.XYPlacements = [[75,75],[300,75],[525,75],[75,275],[300,275],[525,275]];
        
        //Placements 
        this.firstPlacement = new Placement(this.XYPlacements[0][0], this.XYPlacements[0][1], 0, this.refGame, ['doggy_biscuit']);
        this.secondPlacement = new Placement(this.XYPlacements[1][0], this.XYPlacements[1][1], 1, this.refGame);
        this.thirdPlacement = new Placement(this.XYPlacements[2][0], this.XYPlacements[2][1], 2, this.refGame, ['dog_brown']);
        this.fourthPlacement = new Placement(this.XYPlacements[3][0], this.XYPlacements[3][1], 3, this.refGame);
        this.fifthPlacement = new Placement(this.XYPlacements[4][0], this.XYPlacements[4][1], 4, this.refGame, ['dog_house']);
        this.sixthPlacement = new Placement(this.XYPlacements[5][0], this.XYPlacements[5][1], 5, this.refGame);
        
        //Arrows between Placements
        this.arrow_first_second_red = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[1][0], this.XYPlacements[1][1], this.red);
        this.arrow_first_fifth_blue = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[4][0], this.XYPlacements[4][1], this.blue);
        this.arrow_second_third_blue = new Arrow(this.XYPlacements[1][0], this.XYPlacements[1][1], this.XYPlacements[2][0], this.XYPlacements[2][1], this.blue);
        this.arrow_second_fouth_green = new Arrow(this.XYPlacements[1][0], this.XYPlacements[1][1], this.XYPlacements[3][0], this.XYPlacements[3][1], this.green );
        this.arrow_fourth_fifth_red = new Arrow(this.XYPlacements[3][0], this.XYPlacements[3][1], this.XYPlacements[4][0], this.XYPlacements[4][1], this.red );
        this.arrow_fifth_sixth_green = new Arrow(this.XYPlacements[4][0], this.XYPlacements[4][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.green);
        this.arrow_first_third_green = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[2][0], this.XYPlacements[2][1], this.green, false, [100,0,500,0]);
        this.arrow_fourth_sixth_blue = new Arrow(this.XYPlacements[3][0], this.XYPlacements[3][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.blue, false, [100,350,500,350]);

        //add arrows between Placements
        this.elements.push(this.arrow_first_second_red, this.arrow_first_fifth_blue, this.arrow_second_third_blue, this.arrow_second_fouth_green, this.arrow_fourth_fifth_red, this.arrow_fifth_sixth_green, this.arrow_first_third_green, this.arrow_fourth_sixth_blue);
      
        //add placements
        this.elements.push(this.firstPlacement, this.secondPlacement, this.thirdPlacement, this.fourthPlacement, this.fifthPlacement, this.sixthPlacement);
        
        //Guides
        this.firtGuide = new Guide(165, 400, 40, "green");
        this.secondGuide = new Guide(255, 400, 40, "blue");
        this.thirdGuide = new Guide(345, 400, 40, "red");
        this.fourthGuide = new Guide(435, 400, 40, "red");
        
        //Arrow under guides
        this.arrowUnderGuides = new Arrow(100, 400, 550, 400, this.black, true);

        //add arrow under guides
        this.elements.push(this.arrowUnderGuides)

        //add guides
        this.elements.push(this.firtGuide, this.secondGuide, this.thirdGuide, this.fourthGuide);

        //other images
        this.dogBrown = new Shape(50, 400, 50, 50, this.refGame.global.resources.getImage('dog_brown'), '');
        this.dogHouse = new Shape(550, 400, 50, 50, this.refGame.global.resources.getImage('dog_house'), '');

        //Rect and arrow for tutorial
        this.rect = new PIXI.Graphics();
        this.triangle = new PIXI.Graphics();
        this.arrow_tokens_guides_Red = new Arrow(477, 550, 465, 430, this.red, true, [570,500,472,420]);
        this.arrow_tokens_guides_Red.setVisible(false);

        //add Rect and arrow for tutorial
        this.elements.push(this.rect, this.triangle, this.arrow_tokens_guides_Red);
        
        //tokens
        this.dogBrownSit = new Shape(0, 0, 40, 40, this.refGame.global.resources.getImage('dog_brown_sit'), 'blue', true, this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));
        this.dogBrownBend = new Shape(0, 0, 40, 40, this.refGame.global.resources.getImage('dog_brown_bend'), 'green', true, this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));
        this.dogBrownJump = new Shape(0, 0, 40, 40, this.refGame.global.resources.getImage('dog_brown_jump'), 'red', true, this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));
        this.dogBrownJumpSecond = new Shape(0, 0, 40, 40, this.refGame.global.resources.getImage('dog_brown_jump'), 'red', true, this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));

        this.dogBrownSit.setStopMove(true);
        this.dogBrownBend.setStopMove(true);
        this.dogBrownJump.setStopMove(true);
        this.dogBrownJumpSecond.setStopMove(true);

        this.shapes = [this.dogBrownSit, this.dogBrownJumpSecond, this.dogBrownBend, this.dogBrownJump];
        this.cells = [this.firtGuide, this.secondGuide, this.thirdGuide, this.fourthGuide];
         
        //add tokens
        this.elements.push(this.dogBrownSit, this.dogBrownJumpSecond, this.dogBrownBend, this.dogBrownJump);

        //add others images
        this.elements.push(this.dogBrown, this.dogHouse);

        let startBtn = new PIXI.Graphics();
            startBtn.beginFill(0x4bacc6, 1);
            startBtn.lineStyle(2, 0x357d91, 1);  
            startBtn.moveTo(35, 385)       
            startBtn.lineTo(65, 400)
            startBtn.lineTo(35, 415)
            startBtn.lineTo(35, 385);
            startBtn.endFill();
            startBtn.interactive = true;
            startBtn.on('pointerdown', this.onClick.bind(this));

        //add startBtn
        this.elements.push(startBtn);
        this.numAttemps = 0;

        //variables for tutorial
        this.XYRectangle = [[10, 10, 580, 330],[120,350,360,100],[120,505,360,90],[252,210,115,115],[120,505,360,90],[10,360,80,80]];
        this.texts = ["map","guide","token","kennel","tokensToGuide","play"];
        this.position = 0;       
        this.allTokensInGuide = true;
    }

    onClick(){
        if(this.currentText === "play"){
            this.rect.x = 0;
            this.rect.y = 0;
            document.getElementById("next").style.visibility = "hidden"; 
        }
        let juste = 0;
        let allFull = true;
        for (let i = 0; i < this.cells.length; i++) {
            let cell = this.cells[i];
            if(cell.getShape() != null){
                if(cell.getValue() === cell.getShape().getName()){
                    juste++;
                }
            }else{
                allFull = false;
            }
        }
        if(juste === this.cells.length){
            this.numAttemps++;
            this.endGame();
        }else if(allFull){
            this.numAttemps++;
            this.refGame.global.util.showAlert(
                //Type d'alerte
                'error',
                //Texte
                this.refGame.global.resources.getOtherText('blocked'),
                //Titre
                this.refGame.global.resources.getOtherText('error'),
                //Footer
                undefined,
                //Callback
                undefined
            );
        }else{
            this.refGame.global.util.showAlert(
                //Type d'alerte
                'error',
                //Texte
                this.refGame.global.resources.getOtherText('notFull'),
                //Titre
                this.refGame.global.resources.getOtherText('error'),
                //Footer
                undefined,
                //Callback
                undefined
            );
        }
    }

    show() {
        this.refGame.global.listenerManager.addListenerOn(document.getElementById('next'), ['click'], this.handleNext.bind(this));
        this.refGame.global.util.showTutorialInputs('next', 'btnReset');
        this.clear();
        this.initGame();
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    handleNext() {
        let ok = false;
        if(this.position < this.XYRectangle.length){
            this.rect.clear();
            this.rect.lineStyle(6, this.red, 1)
                .drawRoundedRect(this.XYRectangle[this.position][0], this.XYRectangle[this.position][1], this.XYRectangle[this.position][2], this.XYRectangle[this.position][3], 30);
            this.currentText = this.texts[this.position];
            if(this.currentText === "tokensToGuide"){
                if(this.allTokensInGuide){
                    this.triangle.beginFill(this.red, 1)
                        .lineStyle(1, this.red, 1)
                        .moveTo(457,423)
                        .lineTo(478,421)
                        .lineTo(471,440)
                        .lineTo(457,423)
                        .endFill();
                    this.arrow_tokens_guides_Red.setVisible(true);
                    this.allTokensInGuide = false;
                }
                this.dogBrownSit.setStopMove(false);
                this.dogBrownBend.setStopMove(false);
                this.dogBrownJump.setStopMove(false);
                this.dogBrownJumpSecond.setStopMove(false);
            }else{
                this.triangle.clear();
                this.arrow_tokens_guides_Red.setVisible(false);
            }
        }else{
            this.rect.x = 0;
            this.rect.y = 0;
            this.rect.width = 0;
            this.rect.height = 0;
        }
        if(this.allTokensInGuide){
            this.position++;
            document.getElementById("next").style.visibility = "visible"; 
        }else{
            this.position++;
            document.getElementById("next").style.visibility = "hidden"; 
        }
        if(this.currentText === "play"){
            document.getElementById("next").style.visibility = "hidden"; 
        }
        if(ok){
            this.handleNext();
        }
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
    }

    refreshLang(lang) {
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
    }

    refreshFont(isOpenDyslexic){

    }

    onShapeClick(shape) {
        Swal.fire(this.refGame.global.resources.getOtherText(shape.name), undefined, 'info');
    }

    onShapeMove(shape) {
        for (let i = 0; i < this.cells.length; i++) {
            let cell = this.cells[i];
            let minX = cell.x - cell.width / 1.7;
            let maxX = cell.x + cell.width / 1.7;
            let minY = cell.y - cell.width / 1.7;
            let maxY = cell.y + cell.width / 1.7;

            if(cell.getShape() === shape && !(minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY())){
                this.cells[i].setShape(null);
            }
            
            if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY() && (this.cells[i].getShape() === shape || !this.cells[i].getShape())) {
                shape.setX(cell.x);
                shape.setY(cell.y);
                this.cells[i].setShape(shape);
            }
        }
    }

    onShapeEndMove(shape){
        for (let i = 0; i < this.cells.length; i++) {
            let cell = this.cells[i];
            let minX = cell.x - cell.width / 1.7;
            let maxX = cell.x + cell.width / 1.7;
            let minY = cell.y - cell.width / 1.7;
            let maxY = cell.y + cell.width / 1.7;
            if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY()){
                shape.setX(cell.x);
                shape.setY(cell.y);
                if(this.cells[i].getShape() != null){
                    if(this.cells[i].getShape() !== shape){
                        this.cells[i].getShape().setX(this.cells[i].getShape().getBaseX());
                        this.cells[i].getShape().setY(this.cells[i].getShape().getBaseY());
                    }
                }
                this.cells[i].setShape(shape);
            }
        }
        let allIN = true;
        for(let i = 0; i < this.cells.length; i++){
            if(this.cells[i].getShape() == null){
                allIN = false;
            }
        }
        if(this.currentText === "tokensToGuide" && allIN){
            document.getElementById("next").style.visibility = "visible";
            this.allTokensInGuide = true;
        } 
    }

    initGame() {
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
        this.numAttemps = 0;
        for (let i = 0; i < this.cells.length; i++) {
            this.cells[i].setShape(null);
        }

        let xy = [165,255,345,435];
        for (let i = 0; i < this.shapes.length; i++) {
            let random = Math.floor(Math.random()*xy.length);
            this.shapes[i].setBaseX(xy[random]);
            this.shapes[i].setBaseY(550);
            xy.splice(random, 1);
        }
        //Réinitialiser le timer
        this.timer = undefined;
        this.end = false;
    }
    
    endGame() {
        if (!this.end) {
            let text = this.refGame.global.resources.getOtherText('victory', {count: this.numAttemps});
            this.refGame.global.util.showAlert(
                //Type d'alerte
                'success',
                //Texte
                text,
                //Titre
                this.refGame.global.resources.getOtherText('success'),
                //Footer
                undefined,
                //Callback
                this.initGame.bind(this)
            );
        }
        this.end = true;
    }

}