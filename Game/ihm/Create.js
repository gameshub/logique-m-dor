/**
 * @classdesc IHM de la création d'exercice
 * @author Niederhauser Léo
 * @version 1.1
 */


//Constantes pour les couleurs utilisés sur les différents boutons
const BLUE_COLOR = 0x007bff;
const WITHE_COLOR = 0xffffff;
const GREEN_COLOR = 0x28a745;
const GREY_COLOR = 0x6c757d;

//Boutons pour valider et revenir en arrière dans la création de l'exercice
const nextbtn = new Button(400, 585, "Valider", BLUE_COLOR, WITHE_COLOR, true);
const returnbtn = new Button(200, 585, "Retour", GREY_COLOR, WITHE_COLOR, true);

//Tableau qui contient les informations de l'exercice
let tableauInfos = { map: "", dog: "", niche: "", biscuits: [], guide: "", solution: [] };

//Constantes des positions dans le canvas pour les emplacements des boutons sur la carte de jeu
const FLATNATURE_XYPLACEMENTSBTN = [[75, 75], [300, 75], [525, 75], [75, 275], [300, 275], [525, 275]];
const BEACH_XYPLACEMENTSBTN = [[75, 180], [225, 95], [225, 250], [375, 180], [525, 65], [525, 280]];
const OCEAN_XYPLACEMENTSBTN = [[225, 65], [75, 180], [225, 180], [375, 180], [525, 180], [375, 300]];

//Variable des objets qui seront affichés en dessous de la carte de jeu
let dogBrown = null;
let dogHouse = null;
let doggyBiscuit = null;

//Constantes des positions dans le canvas pour les emplacements des shapes sur le guide
const XYGuides4Tokens = [[165, 400, ""], [255, 400, ""], [345, 400, ""], [435, 400, ""]];
const XYGuides5Tokens = [[155, 405, "2X"], [250, 405, ""], [250, 480, ""], [345, 450, ""], [440, 450, "2X"]];
const XYGuides6Tokens = [[145, 395, ""], [225, 395, ""], [260, 470, ""], [340, 470, ""], [370, 395, ""], [450, 395, ""]];


class Create extends Interface {

    constructor(refGame) {

        super(refGame, "create");

    }

    show() {

        //retire tous les éléments du canvas
        this.clear();
        //retire tous les éléments du tableau qui contient les éléments qui seront poussés sur la canvas à la fin grâce au this.init()
        this.elements = [];

        //change la langue du texte du tutoriel selon langue sélecionnée
        this.refreshLang(this.refGame.global.resources.getLanguage());
        //change le texte du tutoriel pour les explications du mode créer
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('startCreate'));
        //désactive tous les boutons inutile pour le mode créer
        this.refGame.global.util.hideTutorialInputs('btnResetTr', 'btnAudioGame', 'next', 'btnReset');

        //efface le tableau quand l'utilisateur revient à l'écran d'accueil
        tableauInfos.map = "";
        tableauInfos.dog = "";
        tableauInfos.niche = "";
        tableauInfos.biscuits = [];
        tableauInfos.guide = "";

        //creation du bouton pour démarrer la création de l'exercice et ajout de ce dernier sur le canvas
        this.startButton = new Button(300, 300, this.refGame.global.resources.getOtherText('start'), BLUE_COLOR, WITHE_COLOR, true);
        //ajout d'un evenement au bouton afin qu'il passe à la 1ère étape de la création de l'exercice
        this.startButton.setOnClick(this.choisirCarteDeJeu.bind(this));
        //ajout du bouton sur le canvas
        this.elements.push(this.startButton);

        //creation du texte de bienvenue
        let txtCreatMode = new PIXI.Text(this.refGame.global.resources.getOtherText('welcome'), {
            // fontFamily: 'Arial',
            fontSize: 24,
            fill: 0x000000,
            align: 'left',
            wordWrap: true,
            wordWrapWidth: 580,
            fontWeight: "bolder"
        });
        txtCreatMode.x = 135;
        txtCreatMode.y = 150;
        //ajout du texte sur le canvas
        this.elements.push(txtCreatMode)

        //ajout des élements du tableau sur le canvas
        this.init();
    }

    refreshLang(lang) {

    }

    refreshFont(isOpenDyslexic) {

    }

    /**
     * @desc Crée la page de choix de la carte de jeu avec tous les bons composants et leur fonctionnalités
     */
    choisirCarteDeJeu() {
        //Vider les éléments du canvas
        this.clear();
        //Vider le tableau qui contient les éléments qui seront poussés sur la canvas à la fin grâce au this.init()
        this.elements = [];
        //changement du texte du tutoriel 
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('ChoixCarte'));

        //creation des boutons pour choisir la carte de jeu et ajout de ceux-ci dans le tableau qui sera poussé sur le canvas
        this.flatnaturebtn = new Button(100, 400, this.refGame.global.resources.getOtherText('FlatNature'), BLUE_COLOR, WITHE_COLOR, true);
        this.elements.push(this.flatnaturebtn);
        this.oceanbtn = new Button(300, 400, this.refGame.global.resources.getOtherText('Ocean'), BLUE_COLOR, WITHE_COLOR, true);
        this.elements.push(this.oceanbtn);
        this.beachbtn = new Button(500, 400, this.refGame.global.resources.getOtherText('Beach'), BLUE_COLOR, WITHE_COLOR, true);
        this.elements.push(this.beachbtn);

        //ajout d'un evenement à chaque bouton pour qu'il affiche la carte de jeu correspondante
        this.flatnaturebtn.setOnClick(this.afficheCarteDeJeu.bind(this, "flat_nature"));
        this.beachbtn.setOnClick(this.afficheCarteDeJeu.bind(this, "beach"));
        this.oceanbtn.setOnClick(this.afficheCarteDeJeu.bind(this, "ocean"));

        //ajout d'un evenement au bouton retour pour revenir à l'écran d'accueil et ajout d'un evenement au bouton valider pour passer à l'étape suivante
        nextbtn.setOnClick(this.choisirEmplacementMedor.bind(this));
        returnbtn.setOnClick(this.show.bind(this));

        //test pour savoir si l'utilisateur a déjà choisi une carte de jeu et si oui, affiche la carte de jeu correspondante
        if (tableauInfos.map != "") {
            this.afficheCarteDeJeu(tableauInfos.map);
        } else {
            nextbtn.setdisabled(true);
        }

        //ajout des boutons dans le tableau qui sera poussé sur le canvas
        this.elements.push(returnbtn, nextbtn);

        //ajout les éléments du tableau sur le canvas
        this.init();

    }

    /**
     * @desc Affiche la carte de jeu correspondante au bouton cliqué
     * @param {string} carte - nom de la carte de jeu à afficher
     * @example afficheCarteDeJeu("flat_nature") 
     */
    afficheCarteDeJeu(carte) {

        //test si l'utilisateur a choisi une carte différente. Si oui alors on efface les données du chien et de la niche dans le tableau d'informations de l'exercice
        if (tableauInfos.map != carte) {
            tableauInfos.dog = "";
            tableauInfos.niche = "";
            tableauInfos.biscuits = [];
            tableauInfos.guide = "";
        }

        //appel de la classe CarteNeutre pour afficher la carte de jeu correspondante
        this.map = new CarteNeutre(this.refGame, carte);

        //ajout de la carte de jeu dans le tableau pour former le JSON plus tard
        tableauInfos.map = carte;

        //afficher la carte de jeu sur la canvas
        this.elements.push(this.map)
        this.init();

        //test pour changer la couleur du bouton en fonction de s'il est sélectionné ou non
        if (carte == "flat_nature") {
            this.flatnaturebtn.setbgColor(GREEN_COLOR);
            this.beachbtn.setbgColor(BLUE_COLOR);
            this.oceanbtn.setbgColor(BLUE_COLOR);
        } else if (carte == "beach") {
            this.flatnaturebtn.setbgColor(BLUE_COLOR);
            this.beachbtn.setbgColor(GREEN_COLOR);
            this.oceanbtn.setbgColor(BLUE_COLOR);

        } else if (carte == "ocean") {
            this.flatnaturebtn.setbgColor(BLUE_COLOR);
            this.beachbtn.setbgColor(BLUE_COLOR);
            this.oceanbtn.setbgColor(GREEN_COLOR);
        }
    }

    /**
     * @desc Gèrer la page de sélection de l'emplacement du chien
    */
    choisirEmplacementMedor() {

        //Test pour vérifier si une carte de jeu a été choisie
        if (tableauInfos.map != "") {

            //Vider les éléments tel que le tableau des objets poussés sur le canvas ainsi que le canvas lui même
            this.clear();
            this.elements = [];

            //Changement du texte du tutorial pour expliquer la tâche à effectuer à l'utilisateur
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText('start'));

            //Création d'une nouvelle carte avec la valeur choisie précédement
            this.map = new CarteNeutre(this.refGame, tableauInfos.map);

            //Ajout d'un événement en cas de clic sur les boutons Valider et Retour
            returnbtn.setOnClick(this.choisirCarteDeJeu.bind(this));
            nextbtn.setOnClick(this.choisirEmplacementNiche.bind(this));

            //Ajouter les objets créés précédement dans le tableau elements qui vont être poussés sur le canvas
            this.elements.push(returnbtn, nextbtn, this.map);

            //initialisation de l'image du chien puis affichage de celle-ci sur le canvas
            dogBrown = new Shape(300, 450, 50, 50, this.refGame.global.resources.getImage('dog_brown'), '')

            //Modification de la valeur de la variable XYPlacements en fonction de la carte choisie
            if (tableauInfos.map === "flat_nature") {

                this.XYPlacements = FLATNATURE_XYPLACEMENTS;

            } else if (tableauInfos.map === "beach") {

                this.XYPlacements = BEACH_XYPLACEMENTS;

            } else if (tableauInfos.map === "ocean") {

                this.XYPlacements = OCEAN_XYPLACEMENTS;

            }

            //création des emplacements sous forme de boutons qui seront sur la carte de jeu 
            this.firstPlacement = new PlacementBtn(this.XYPlacements[0][0], this.XYPlacements[0][1], 0, this.refGame);
            this.secondPlacement = new PlacementBtn(this.XYPlacements[1][0], this.XYPlacements[1][1], 1, this.refGame);
            this.thirdPlacement = new PlacementBtn(this.XYPlacements[2][0], this.XYPlacements[2][1], 2, this.refGame);
            this.fourthPlacement = new PlacementBtn(this.XYPlacements[3][0], this.XYPlacements[3][1], 3, this.refGame);
            this.fifthPlacement = new PlacementBtn(this.XYPlacements[4][0], this.XYPlacements[4][1], 4, this.refGame);
            this.sixthPlacement = new PlacementBtn(this.XYPlacements[5][0], this.XYPlacements[5][1], 5, this.refGame);


            //ajout des emplacements dasn le tableau qui sera poussé sur le canvas
            this.elements.push(this.firstPlacement, this.secondPlacement, this.thirdPlacement, this.fourthPlacement, this.fifthPlacement, this.sixthPlacement);

            //ajout d'un evenement à chaque emplacement pour qu'il affiche le chien au bon emplacmeent
            this.firstPlacement.setOnClick(this.modifierChien.bind(this, "0"));
            this.secondPlacement.setOnClick(this.modifierChien.bind(this, "1"));
            this.thirdPlacement.setOnClick(this.modifierChien.bind(this, "2"));
            this.fourthPlacement.setOnClick(this.modifierChien.bind(this, "3"));
            this.fifthPlacement.setOnClick(this.modifierChien.bind(this, "4"));
            this.sixthPlacement.setOnClick(this.modifierChien.bind(this, "5"));

            //Test pour savoir si le chien possède déjà un emplacement
            if (tableauInfos.dog == "") {
                //Si non alors on va afficher le chien à sa position initiale qui est en dessous de la carte
                this.elements.push(dogBrown);
            } else {
                //Si oui alors on va lancer la méthode modifierChien pour afficher le chien sur son emplacement
                this.modifierChien(tableauInfos.dog);
            }

            //Ajout des objets du tableau "elements" sur le canvas
            this.init();

        } else {

            //affiche une alerte si l'utilisateur n'a pas choisi de carte de jeu
            Swal.fire(
                'Erreur !',
                'Veuillez choisir une carte de jeu !',
                'error'
            )

            //appel de la fonction choisirCarteDeJeu pour revenir à l'écran de choix de la carte de jeu
            this.choisirCarteDeJeu();
        }
    }

    /**
     * @desc Modifier l'emplacement de Médor sur la carte de jeu
    */
    modifierChien(num) {

        //Vérification si la niche se trouve sur le nouvel emplacement voulu pour Médor
        if (tableauInfos.niche == num) {
            //Si oui alors on effacera la valeur de la niche
            tableauInfos.niche = "";
        }

        //Vérification si un biscuit se trouve sur le nouvel emplacement voulu pour Médor
        tableauInfos.biscuits.forEach(biscuit => {
            if (biscuit == num) {
                //Si oui alors on effacera cette emplacement de biscuit
                tableauInfos.biscuits.splice(tableauInfos.biscuits.indexOf(num), 1);
            }
        });

        //ajout du numéro de l'emplacement du chien dans le tableau des informations de l'exercice
        tableauInfos.dog = num;

        //test pour savoir si l'utilisateur a déjà choisi un emplacement pour le chien
        if (tableauInfos.dog != "") {
            //si oui, on rend le chien d'exemple invisible
            dogBrown.setVisible(false);
        }

        //suppression de l'image du chien sur tous les emplacements
        this.firstPlacement.removeImage();
        this.secondPlacement.removeImage();
        this.thirdPlacement.removeImage();
        this.fourthPlacement.removeImage();
        this.fifthPlacement.removeImage();
        this.sixthPlacement.removeImage();

        //ajout de l'image du chien sur l'emplacement choisi
        switch (num) {
            case "0":
                this.firstPlacement.setImage(['dog_brown']);
                break;
            case "1":
                this.secondPlacement.setImage(['dog_brown']);
                break;
            case "2":
                this.thirdPlacement.setImage(['dog_brown']);
                break;
            case "3":
                this.fourthPlacement.setImage(['dog_brown']);
                break;
            case "4":
                this.fifthPlacement.setImage(['dog_brown']);
                break;
            case "5":
                this.sixthPlacement.setImage(['dog_brown']);
                break;
        }
    }

    /**
     * @desc Gèrer la page de sélection de l'emplacement la niche sur la carte de jeu
    */
    choisirEmplacementNiche() {

        //Vérification si l'utilisateur a déjà choisi un emplacement pour le chien
        if (tableauInfos.dog != "") {

            //Vidage des éléments du canvas
            this.clear();
            //Vidage du tableau des éléments du canvas
            this.elements = [];
            //Changement des textes du tutoriel
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText('end'));
            //Création de la carte de jeu
            this.map = new CarteNeutre(this.refGame, tableauInfos.map);
            //Ajout des événements sur les boutons de passage des pages
            returnbtn.setOnClick(this.choisirEmplacementMedor.bind(this));
            nextbtn.setOnClick(this.choisirEmplacementBiscuits.bind(this));
            //Ajout des objets dans le tableau des éléments du canvas
            this.elements.push(returnbtn, nextbtn, this.map);

            //initiation de l'image de la niche
            dogHouse = new Shape(300, 450, 50, 50, this.refGame.global.resources.getImage('dog_house'), '')

            //Modification de la valeur de la variable XYPlacements en fonction de la carte choisie
            if (tableauInfos.map === "flat_nature") {

                this.XYPlacements = FLATNATURE_XYPLACEMENTS;

            } else if (tableauInfos.map === "beach") {

                this.XYPlacements = BEACH_XYPLACEMENTS;

            } else if (tableauInfos.map === "ocean") {

                this.XYPlacements = OCEAN_XYPLACEMENTS;

            }

            //création des emplacements sous forme de boutons qui seront sur la carte de jeu 
            this.firstPlacement = new PlacementBtn(this.XYPlacements[0][0], this.XYPlacements[0][1], 0, this.refGame);
            this.secondPlacement = new PlacementBtn(this.XYPlacements[1][0], this.XYPlacements[1][1], 1, this.refGame);
            this.thirdPlacement = new PlacementBtn(this.XYPlacements[2][0], this.XYPlacements[2][1], 2, this.refGame);
            this.fourthPlacement = new PlacementBtn(this.XYPlacements[3][0], this.XYPlacements[3][1], 3, this.refGame);
            this.fifthPlacement = new PlacementBtn(this.XYPlacements[4][0], this.XYPlacements[4][1], 4, this.refGame);
            this.sixthPlacement = new PlacementBtn(this.XYPlacements[5][0], this.XYPlacements[5][1], 5, this.refGame);


            //ajout des emplacements dasn le tableau qui sera poussé sur le canvas
            this.elements.push(this.firstPlacement, this.secondPlacement, this.thirdPlacement, this.fourthPlacement, this.fifthPlacement, this.sixthPlacement);

            //ajout d'un evenement à chaque emplacement pour qu'il affiche la niche au bon emplacement
            this.firstPlacement.setOnClick(this.modifierNiche.bind(this, "0"));
            this.secondPlacement.setOnClick(this.modifierNiche.bind(this, "1"));
            this.thirdPlacement.setOnClick(this.modifierNiche.bind(this, "2"));
            this.fourthPlacement.setOnClick(this.modifierNiche.bind(this, "3"));
            this.fifthPlacement.setOnClick(this.modifierNiche.bind(this, "4"));
            this.sixthPlacement.setOnClick(this.modifierNiche.bind(this, "5"));

            //Test pour savoir si l'utilisateur n'a pas encore choisi d'emplacement pour la niche
            if (tableauInfos.niche == "") {
                //Si oui on ajoute la niche dans le tableau des éléments du canvas
                this.elements.push(dogHouse);
                //On affiche le chien à son emplacement
                this.modifierChien(tableauInfos.dog);
            } else {
                //Si non on affiche la niche à son emplacement déjà choisi
                this.modifierNiche(tableauInfos.niche);
            }

            //appel de la fonction init pour afficher les éléments sur le canvas
            this.init();

        } else {

            //affiche une alerte si l'utilisateur n'a pas choisi d'emplacement pour le chien
            Swal.fire(
                'Erreur !',
                'Veuillez choisir un emplacement pour le chien !',
                'error'
            )

            //appel de la fonction choisirEmplacementMedor pour revenir à l'écran de choix de l'emplacement du chien
            this.choisirEmplacementMedor();
        }


    }

    /**
     * @desc Modifier l'emplacement de la niche sur la carte de jeu
    */
    modifierNiche(num) {

        //Test pour vérifier si l'utilisateur a choisi un emplacement différent pour la niche que celui du chien
        if (tableauInfos.dog != num) {
            //Si oui alors on ajout le numéro de l'emplacement de la niche dans le tableau des informations de l'exercice
            tableauInfos.niche = num;

            //Test pour vérifier si un biscuit était déjà placé sur le nouvel emplacement de la niche
            tableauInfos.biscuits.forEach(biscuit => {
                if (biscuit == num) {
                    //Si oui alors on supprime l'emplacement du biscuit
                    tableauInfos.biscuits.splice(tableauInfos.biscuits.indexOf(num), 1);
                }
            });

            //test pour savoir si l'utilisateur a déjà choisi un emplacement pour la niche
            if (tableauInfos.niche != "") {
                //si oui, on rend la niche d'exemple invisible
                dogHouse.setVisible(false);
            }

            //suppression de l'image de la niche sur tous les emplacements
            this.firstPlacement.removeImage();
            this.secondPlacement.removeImage();
            this.thirdPlacement.removeImage();
            this.fourthPlacement.removeImage();
            this.fifthPlacement.removeImage();
            this.sixthPlacement.removeImage();

            //ajout de l'image de la niche sur l'emplacement choisi
            switch (num) {
                case "0":
                    this.firstPlacement.setImage(['dog_house']);
                    break;
                case "1":
                    this.secondPlacement.setImage(['dog_house']);
                    break;
                case "2":
                    this.thirdPlacement.setImage(['dog_house']);
                    break;
                case "3":
                    this.fourthPlacement.setImage(['dog_house']);
                    break;
                case "4":
                    this.fifthPlacement.setImage(['dog_house']);
                    break;
                case "5":
                    this.sixthPlacement.setImage(['dog_house']);
                    break;
            }

            //ajout de l'image du chien sur l'emplacement choisi
            switch (tableauInfos.dog) {
                case "0":
                    this.firstPlacement.setImage(['dog_brown']);
                    break;
                case "1":
                    this.secondPlacement.setImage(['dog_brown']);
                    break;
                case "2":
                    this.thirdPlacement.setImage(['dog_brown']);
                    break;
                case "3":
                    this.fourthPlacement.setImage(['dog_brown']);
                    break;
                case "4":
                    this.fifthPlacement.setImage(['dog_brown']);
                    break;
                case "5":
                    this.sixthPlacement.setImage(['dog_brown']);
                    break;
            }
        } else {
            //Si non on affiche une alerte
            Swal.fire(
                'Erreur !',
                'Veuillez choisir un emplacement different pour la niche !',
                'error'
            )
        }
    }

   /**
     * @desc Gèrer la page de sélection des emplacements des biscuits
    */
    choisirEmplacementBiscuits() {
        //Si la variable "tableauInfos.niche" n'est pas une chaîne vide, on charge la page de choix des biscuits
        if (tableauInfos.niche != "") {
            //On vide le canvas de tous les éléments
            this.clear();
            //On vide le tableau des éléments qui sont poussés sur le canvas
            this.elements = [];
            //On change le texte du tutoriel
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText('biscuit'));
            //On crée la nouvelle carte de jeu
            this.map = new CarteNeutre(this.refGame, tableauInfos.map);
            //On ajoute un événement au clic sur chaque bouton
            returnbtn.setOnClick(this.choisirEmplacementNiche.bind(this));
            nextbtn.setOnClick(this.choisirGuide.bind(this));
            //On ajoute les éléments dans le tableau elements
            this.elements.push(returnbtn, nextbtn, this.map);

            //creation de l'image du biscuit puis affichage de celle-ci sur le canvas
            doggyBiscuit = new Shape(300, 450, 50, 50, this.refGame.global.resources.getImage('doggy_biscuit'), '')
            this.elements.push(doggyBiscuit);

            //Modification de la valeur de XYPlacements en fonction de la carte choisie
            if (tableauInfos.map === "flat_nature") {

                this.XYPlacements = FLATNATURE_XYPLACEMENTS;

            } else if (tableauInfos.map === "beach") {

                this.XYPlacements = BEACH_XYPLACEMENTS;

            } else if (tableauInfos.map === "ocean") {

                this.XYPlacements = OCEAN_XYPLACEMENTS;

            }

            //création des emplacements sous forme de boutons qui seront sur la carte de jeu 
            this.firstPlacement = new PlacementBtn(this.XYPlacements[0][0], this.XYPlacements[0][1], 0, this.refGame);
            this.secondPlacement = new PlacementBtn(this.XYPlacements[1][0], this.XYPlacements[1][1], 1, this.refGame);
            this.thirdPlacement = new PlacementBtn(this.XYPlacements[2][0], this.XYPlacements[2][1], 2, this.refGame);
            this.fourthPlacement = new PlacementBtn(this.XYPlacements[3][0], this.XYPlacements[3][1], 3, this.refGame);
            this.fifthPlacement = new PlacementBtn(this.XYPlacements[4][0], this.XYPlacements[4][1], 4, this.refGame);
            this.sixthPlacement = new PlacementBtn(this.XYPlacements[5][0], this.XYPlacements[5][1], 5, this.refGame);


            //ajout des emplacements dasn le tableau qui sera poussé sur le canvas
            this.elements.push(this.firstPlacement, this.secondPlacement, this.thirdPlacement, this.fourthPlacement, this.fifthPlacement, this.sixthPlacement);

            //ajout d'un evenement à chaque emplacement pour qu'il affiche la niche au bon emplacement
            this.firstPlacement.setOnClick(this.ajouterBiscuit.bind(this, "0"));
            this.secondPlacement.setOnClick(this.ajouterBiscuit.bind(this, "1"));
            this.thirdPlacement.setOnClick(this.ajouterBiscuit.bind(this, "2"));
            this.fourthPlacement.setOnClick(this.ajouterBiscuit.bind(this, "3"));
            this.fifthPlacement.setOnClick(this.ajouterBiscuit.bind(this, "4"));
            this.sixthPlacement.setOnClick(this.ajouterBiscuit.bind(this, "5"));

            //Test si le tableau des biscuits est vide ou non
            if (tableauInfos.biscuits.length == 0) {
                doggyBiscuit.setVisible(true);
                this.modifierNiche(tableauInfos.niche);
            } else {
                doggyBiscuit.setVisible(false);
                this.modifierBiscuits(tableauInfos.biscuits);
            }

            this.init();

        } else {

            //affiche une alerte si l'utilisateur n'a pas choisi d'emplacement pour la niche
            Swal.fire(
                'Erreur !',
                'Veuillez choisir un emplacement pour la niche !',
                'error'
            )

            //appel de la fonction choisirEmplacementNiche pour revenir à l'écran de choix de l'emplacement de la niche
            this.choisirEmplacementNiche();
        }


    }

    /**
     * @desc Ajoute un biscuit au tableau des biscuits
     * @param {string} num - numéro de l'emplacement du biscuit
     */
    ajouterBiscuit(num) {
        //Test si l'emplacement choisi est déjà intégré dans le tableau des biscuits
        if (tableauInfos.biscuits.find(element => element === num)) {
            //Si oui, alors on le supprime du tableau pour ne plus l'afficher
            tableauInfos.biscuits.splice(tableauInfos.biscuits.indexOf(num), 1);

        //Test si l'emplacement choisi est déjà utilisé par la niche ou le chien
        } else if (num != tableauInfos.niche && num != tableauInfos.dog) {
            //Ajout du biscuit dans le tableau elements
            tableauInfos.biscuits.push(num);
        } else {
            //affiche une alerte si l'utilisateur a choisi un emplacement déjà utilisé
            Swal.fire(
                'Erreur !',
                'Veuillez choisir un emplacement different pour le biscuit !',
                'error'
            )
        }

        //appel de la méthode pour afficher le nouveau biscuit
        this.modifierBiscuits(tableauInfos.biscuits);
    }

    /**
     * @desc Modifie les emplacements des biscuits
     * @param {string} biscuits - tableau des biscuits
     */
    modifierBiscuits(biscuits) {

        //test pour savoir si l'utilisateur a déjà choisi un emplacement pour les biscuits
        if (biscuits.length > 0) {
            //si oui, on rend le biscuit d'exemple invisible
            doggyBiscuit.setVisible(false);
        } else {
            //si non, on rend le biscuit d'exemple visible
            doggyBiscuit.setVisible(true);
        }

        //suppression des images sur tous les emplacements
        this.firstPlacement.removeImage();
        this.secondPlacement.removeImage();
        this.thirdPlacement.removeImage();
        this.fourthPlacement.removeImage();
        this.fifthPlacement.removeImage();
        this.sixthPlacement.removeImage();

        //parcours du tableau des biscuits
        biscuits.forEach(biscuit => {
            //Test si la case n'est pas vide
            if (biscuit != "") {

                //ajout de l'image du biscuit sur l'emplacement choisi
                switch (biscuit) {
                    case "0":
                        this.firstPlacement.setImage(['doggy_biscuit']);
                        break;
                    case "1":
                        this.secondPlacement.setImage(['doggy_biscuit']);
                        break;
                    case "2":
                        this.thirdPlacement.setImage(['doggy_biscuit']);
                        break;
                    case "3":
                        this.fourthPlacement.setImage(['doggy_biscuit']);
                        break;
                    case "4":
                        this.fifthPlacement.setImage(['doggy_biscuit']);
                        break;
                    case "5":
                        this.sixthPlacement.setImage(['doggy_biscuit']);
                        break;
                }
            }

        });


        //ajout de l'image de la niche sur l'emplacement choisi
        switch (tableauInfos.niche) {
            case "0":
                this.firstPlacement.setImage(['dog_house']);
                break;
            case "1":
                this.secondPlacement.setImage(['dog_house']);
                break;
            case "2":
                this.thirdPlacement.setImage(['dog_house']);
                break;
            case "3":
                this.fourthPlacement.setImage(['dog_house']);
                break;
            case "4":
                this.fifthPlacement.setImage(['dog_house']);
                break;
            case "5":
                this.sixthPlacement.setImage(['dog_house']);
                break;
        }

        //ajout de l'image du chien sur l'emplacement choisi
        switch (tableauInfos.dog) {
            case "0":
                this.firstPlacement.setImage(['dog_brown']);
                break;
            case "1":
                this.secondPlacement.setImage(['dog_brown']);
                break;
            case "2":
                this.thirdPlacement.setImage(['dog_brown']);
                break;
            case "3":
                this.fourthPlacement.setImage(['dog_brown']);
                break;
            case "4":
                this.fifthPlacement.setImage(['dog_brown']);
                break;
            case "5":
                this.sixthPlacement.setImage(['dog_brown']);
                break;
        }

    }

    /**
     * @desc Gèrer la page de choix du guide
     */
    choisirGuide() {
        this.clear();
        this.elements = [];
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('ChoixGuide'));
        this.map = new CarteNeutre(this.refGame, tableauInfos.map);
        nextbtn.setOnClick(this.choisirSolution.bind(this));
        returnbtn.setOnClick(this.choisirEmplacementBiscuits.bind(this));
        this.elements.push(returnbtn, nextbtn, this.map);

        //creation des boutons pour choisir la carte de jeu et ajout de ceux-ci dans le tableau qui sera poussé sur le canvas
        this.tokens4 = new Button(100, 540, this.refGame.global.resources.getOtherText('4Tokens'), BLUE_COLOR, WITHE_COLOR, true);
        this.elements.push(this.tokens4);
        this.tokens5 = new Button(300, 540, this.refGame.global.resources.getOtherText('5Tokens'), BLUE_COLOR, WITHE_COLOR, true);
        this.elements.push(this.tokens5);
        this.tokens6 = new Button(500, 540, this.refGame.global.resources.getOtherText('6Tokens'), BLUE_COLOR, WITHE_COLOR, true);
        this.elements.push(this.tokens6);

        if (tableauInfos.map === "flat_nature") {

            this.XYPlacements = FLATNATURE_XYPLACEMENTS;

        } else if (tableauInfos.map === "beach") {

            this.XYPlacements = BEACH_XYPLACEMENTS;

        } else if (tableauInfos.map === "ocean") {

            this.XYPlacements = OCEAN_XYPLACEMENTS;

        }

        //création des emplacements sous forme de boutons qui seront sur la carte de jeu 
        this.firstPlacement = new Placement(this.XYPlacements[0][0], this.XYPlacements[0][1], 0, this.refGame);
        this.secondPlacement = new Placement(this.XYPlacements[1][0], this.XYPlacements[1][1], 1, this.refGame);
        this.thirdPlacement = new Placement(this.XYPlacements[2][0], this.XYPlacements[2][1], 2, this.refGame);
        this.fourthPlacement = new Placement(this.XYPlacements[3][0], this.XYPlacements[3][1], 3, this.refGame);
        this.fifthPlacement = new Placement(this.XYPlacements[4][0], this.XYPlacements[4][1], 4, this.refGame);
        this.sixthPlacement = new Placement(this.XYPlacements[5][0], this.XYPlacements[5][1], 5, this.refGame);


        //ajout des emplacements dasn le tableau qui sera poussé sur le canvas
        this.elements.push(this.firstPlacement, this.secondPlacement, this.thirdPlacement, this.fourthPlacement, this.fifthPlacement, this.sixthPlacement);

        //ajout d'un evenement à chaque bouton pour qu'il affiche la carte de jeu correspondante
        this.tokens4.setOnClick(this.afficherGuide.bind(this, "4Tokens"));
        this.tokens5.setOnClick(this.afficherGuide.bind(this, "5Tokens"));
        this.tokens6.setOnClick(this.afficherGuide.bind(this, "6Tokens"));

        //test pour savoir si l'utilisateur a déjà choisi un guide
        if (tableauInfos.guide != "") {
            //si oui, on affiche le guide correspondant
            this.afficherGuide(tableauInfos.guide);
        } else {
            //si non on va simplement afficher toutes les infos sur la carte du jeu
            this.afficherTous();
        }

        //ajout les éléments du tableau sur le canvas
        this.init();

    }

    /**
     * @desc Affiche le guide correspondante au bouton cliqué
     * @param {string} guide - nom du guide à afficher
     */
    afficherGuide(guide) {

        this.clear();
        this.elements = [];
        this.map = new CarteNeutre(this.refGame, tableauInfos.map);
        this.elements.push(this.map, nextbtn, returnbtn, this.tokens4, this.tokens5, this.tokens6);
        //ajout des emplacements dans le tableau qui sera poussé sur le canvas
        this.elements.push(this.firstPlacement, this.secondPlacement, this.thirdPlacement, this.fourthPlacement, this.fifthPlacement, this.sixthPlacement);

        // //test si l'utilisateur a choisi un guide différent. Si oui alors on efface les données de la solution dans le tableau d'informations de l'exercice
        // if (tableauInfos.guide != guide) {
        //     // tableauInfos.solution = "";
        // }

        //création du guide
        this.guide = new GuideTokenBasic(this.refGame, guide);

        //ajout du guide dans le tableau d'informations de l'exercice pour former le JSON plus tard
        tableauInfos.guide = guide;

        //afficher le guide sur la canvas
        this.elements.push(this.guide)
        this.init();

        //test pour changer la couleur du bouton en fonction de s'il est sélectionné ou non
        if (guide == "4Tokens") {
            this.tokens4.setbgColor(GREEN_COLOR);
            this.tokens5.setbgColor(BLUE_COLOR);
            this.tokens6.setbgColor(BLUE_COLOR);
        } else if (guide == "5Tokens") {
            this.tokens4.setbgColor(BLUE_COLOR);
            this.tokens5.setbgColor(GREEN_COLOR);
            this.tokens6.setbgColor(BLUE_COLOR);

        } else if (guide == "6Tokens") {
            this.tokens4.setbgColor(BLUE_COLOR);
            this.tokens5.setbgColor(BLUE_COLOR);
            this.tokens6.setbgColor(GREEN_COLOR);
        }

        //afficher les elements sur la carte du jeu
        this.afficherTous();
    }

    /**
     * @desc Affiche toutes les elements (chien, niche, biscuits) sur la carte du jeu
     */
    afficherTous() {

        //ajout de l'image de la niche sur l'emplacement choisi
        switch (tableauInfos.niche) {
            case "0":
                this.firstPlacement.setImage(['dog_house']);
                break;
            case "1":
                this.secondPlacement.setImage(['dog_house']);
                break;
            case "2":
                this.thirdPlacement.setImage(['dog_house']);
                break;
            case "3":
                this.fourthPlacement.setImage(['dog_house']);
                break;
            case "4":
                this.fifthPlacement.setImage(['dog_house']);
                break;
            case "5":
                this.sixthPlacement.setImage(['dog_house']);
                break;
        }

        //ajout de l'image du chien sur l'emplacement choisi
        switch (tableauInfos.dog) {
            case "0":
                this.firstPlacement.setImage(['dog_brown']);
                break;
            case "1":
                this.secondPlacement.setImage(['dog_brown']);
                break;
            case "2":
                this.thirdPlacement.setImage(['dog_brown']);
                break;
            case "3":
                this.fourthPlacement.setImage(['dog_brown']);
                break;
            case "4":
                this.fifthPlacement.setImage(['dog_brown']);
                break;
            case "5":
                this.sixthPlacement.setImage(['dog_brown']);
                break;
        }

        //Pour chaque biscuit on va effectuer un test pour éviter de recherche une mauvaise valeur dans le switch
        tableauInfos.biscuits.forEach(biscuit => {
            if (biscuit != "") {
                //ajout de l'image du biscuit sur l'emplacement choisi
                switch (biscuit) {
                    case "0":
                        this.firstPlacement.setImage(['doggy_biscuit']);
                        break;
                    case "1":
                        this.secondPlacement.setImage(['doggy_biscuit']);
                        break;
                    case "2":
                        this.thirdPlacement.setImage(['doggy_biscuit']);
                        break;
                    case "3":
                        this.fourthPlacement.setImage(['doggy_biscuit']);
                        break;
                    case "4":
                        this.fifthPlacement.setImage(['doggy_biscuit']);
                        break;
                    case "5":
                        this.sixthPlacement.setImage(['doggy_biscuit']);
                        break;
                }
            }
        });

    }

    /**
     * @desc Gèrer la page de réalisation de la solution de l'exercice 
     */
    choisirSolution() {

        //Vérifiez si un guide a bien été choisi
        if (tableauInfos.guide != "") {

            this.clear();
            this.elements = [];
            //changement du texte du tutoriel 
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('ChoixSolution'));
            this.map = new CarteNeutre(this.refGame, tableauInfos.map);

            //ajout d'un evenement au bouton retour pour revenir à l'écran d'accueil et ajout d'un evenement au bouton valider pour passer à l'étape suivante
            nextbtn.setOnClick(this.createJSON.bind(this));
            returnbtn.setOnClick(this.choisirGuide.bind(this));

            //ajout des boutons dans le tableau qui sera poussé sur le canvas
            this.elements.push(returnbtn, nextbtn, this.map);

            if (tableauInfos.map === "flat_nature") {

                this.XYPlacements = FLATNATURE_XYPLACEMENTS;

            } else if (tableauInfos.map === "beach") {

                this.XYPlacements = BEACH_XYPLACEMENTS;

            } else if (tableauInfos.map === "ocean") {

                this.XYPlacements = OCEAN_XYPLACEMENTS;

            }

            //création des emplacements sous forme de boutons qui seront sur la carte de jeu 
            this.firstPlacement = new Placement(this.XYPlacements[0][0], this.XYPlacements[0][1], 0, this.refGame);
            this.secondPlacement = new Placement(this.XYPlacements[1][0], this.XYPlacements[1][1], 1, this.refGame);
            this.thirdPlacement = new Placement(this.XYPlacements[2][0], this.XYPlacements[2][1], 2, this.refGame);
            this.fourthPlacement = new Placement(this.XYPlacements[3][0], this.XYPlacements[3][1], 3, this.refGame);
            this.fifthPlacement = new Placement(this.XYPlacements[4][0], this.XYPlacements[4][1], 4, this.refGame);
            this.sixthPlacement = new Placement(this.XYPlacements[5][0], this.XYPlacements[5][1], 5, this.refGame);


            //ajout des emplacements dasn le tableau qui sera poussé sur le canvas
            this.elements.push(this.firstPlacement, this.secondPlacement, this.thirdPlacement, this.fourthPlacement, this.fifthPlacement, this.sixthPlacement);

            //ajout du guide dans le tableau qui sera poussé sur le canvas
            this.guide = new GuideTokenBasic(this.refGame, tableauInfos.guide);
            this.elements.push(this.guide);

            //rajout de tous les objets précédement créés sur le canvas (chien, niche, biscuits)
            this.afficherTous();

            //appel pour afficher directement des tokens par défaut sur le guide et ainsi de suite créer les événements sur les tokens pour changer la valeur dès qu'on click dessus
            switch (tableauInfos.guide) {
                //Selon la guide choisi
                case "4Tokens":
                    //Initialisation du tableau de solution avec la bonne taille
                    tableauInfos.solution = ["", "", "", ""];
                    //Pour chaque elements on va appeler la fonction changerContenuTokens pour changer le token
                    for (let i = 0; i < XYGuides4Tokens.length; i++) {
                        this.changerContenuTokens(XYGuides4Tokens[i][0], XYGuides4Tokens[i][1], XYGuides4Tokens[i][2], tableauInfos.solution[i], i);
                    };
                    break;
                case "5Tokens":
                    tableauInfos.solution = ["", "", "", "", ""];
                    for (let i = 0; i < XYGuides5Tokens.length; i++) {
                        this.changerContenuTokens(XYGuides5Tokens[i][0], XYGuides5Tokens[i][1], XYGuides5Tokens[i][2], tableauInfos.solution[i], i);
                    };
                    break;
                case "6Tokens":
                    tableauInfos.solution = ["", "", "", "", "", ""];
                    for (let i = 0; i < XYGuides6Tokens.length; i++) {
                        this.changerContenuTokens(XYGuides6Tokens[i][0], XYGuides6Tokens[i][1], XYGuides6Tokens[i][2], tableauInfos.solution[i], i);
                    };
                    break;
            }

            //ajout les éléments du tableau sur le canvas
            this.init();

        } else {

            //affiche une alerte si l'utilisateur n'a pas choisi de guide
            Swal.fire(
                'Erreur !',
                'Veuillez choisir un guide dabord !',
                'error'
            )

            //retour à l'écran de choix de l'emplacement du guide
            this.choisirGuide();
        }

    }

    /**
     * @desc Créer des nouveaux tokens par dessus les anciens selon un cycle
     * @param {string} placementX - Position du X
     * @param {string} placementY - Position du Y
     * @param {string} type -Type de token à créer (rond ou pentagone)
     * @param {string} currentToken - Le token qui est affiché en ce moment
     * @param {string} index - L'index de la position du token dans le tableau de solution
     */
    changerContenuTokens(placementX, placementY, type, currentToken, index) {
        //Initialisation d'un tableau pour les différentes images à récupérer selon la couleur du token
        let tokensIMG = {
            "blue": this.refGame.global.resources.getImage('dog_brown_sit'),
            "red": this.refGame.global.resources.getImage('dog_brown_jump'),
            "green": this.refGame.global.resources.getImage('dog_brown_bend')
        };
        let shape = null;
        let width = 35;

        //Si le guide est 4Tokens, on doit changer la taille des tokens
        if (tableauInfos.guide == "4Tokens") {
            width = 40;
        }

        //Test pour savoir quel est le type du token à créer
        if (type == "2X" || type == "3X") {
            //Changement de taille pour les tokens pentagonaux
            width = 25;
            //Selon le token courant, on va créer un nouveau token avec la valeur suivante
            switch (currentToken) {
                //Si aucun token courant n'existe alors on va créer un token 2X
                case "":
                    //Création du token
                    shape = new ShapeCreate(placementX, placementY, width, width, null, "2X");
                    //On change la valeur du token dans le tableau d'informations qui sera utilisé pour faire le JSON
                    tableauInfos.solution[index] = "2X";
                    //On ajout un événement au token pour changer sa valeur au moment du clic
                    shape.setOnClick(this.changerContenuTokens.bind(this, placementX, placementY, type, tableauInfos.solution[index], index));
                    //On ajoute le token dans le tableau d'éléments qui sera poussé sur le canvas
                    this.elements.push(shape);
                    break;
                //Si le token courant est 2X alors on va créer un token 3X
                case "2X":
                    shape = new ShapeCreate(placementX, placementY, width, width, null, "3X");
                    tableauInfos.solution[index] = "3X";
                    shape.setOnClick(this.changerContenuTokens.bind(this, placementX, placementY, type, tableauInfos.solution[index], index));
                    this.elements.push(shape);
                    break;
                //Si le token courant est 3X alors on va créer un token 2X
                case "3X":
                    shape = new ShapeCreate(placementX, placementY, width, width, null, "2X");
                    tableauInfos.solution[index] = "2X";
                    shape.setOnClick(this.changerContenuTokens.bind(this, placementX, placementY, type, tableauInfos.solution[index], index));
                    this.elements.push(shape);
                    break;
            }

        //Si le token n'est pas de type pendatgonal alors on va créer des tokens ronds
        } else {
            //test pour connaître le futur token qui sera créer selon le token courant afin de créer un cycle
            switch (currentToken) {
                //si aucun token courant alors créer un token vert par défaut
                case "":
                    shape = new ShapeCreate(placementX, placementY, width, width, tokensIMG["green"], "green");
                    tableauInfos.solution[index] = "green";
                    shape.setOnClick(this.changerContenuTokens.bind(this, placementX, placementY, type, tableauInfos.solution[index], index));
                    this.elements.push(shape);
                    break;
                //si le token courant est vert alors on va créer un token bleu par-dessus lui
                case "green":
                    shape = new ShapeCreate(placementX, placementY, width, width, tokensIMG["blue"], "blue");
                    tableauInfos.solution[index] = "blue";
                    shape.setOnClick(this.changerContenuTokens.bind(this, placementX, placementY, type, tableauInfos.solution[index], index));
                    this.elements.push(shape);
                    break;
                //si le token courant est bleu alors on va créer un token rouge par-dessus lui
                case "blue":
                    shape = new ShapeCreate(placementX, placementY, width, width, tokensIMG["red"], "red");
                    tableauInfos.solution[index] = "red";
                    shape.setOnClick(this.changerContenuTokens.bind(this, placementX, placementY, type, tableauInfos.solution[index], index));
                    this.elements.push(shape);
                    break;
                //si le token courant est rouge alors on va créer un token vert par-dessus lui
                case "red":
                    shape = new ShapeCreate(placementX, placementY, width, width, tokensIMG["green"], "green");
                    tableauInfos.solution[index] = "green";
                    shape.setOnClick(this.changerContenuTokens.bind(this, placementX, placementY, type, tableauInfos.solution[index], index));
                    this.elements.push(shape);
                    break;
            }

        }

        //On fait un init à la fin pour afficher le nouveau token sur le canvas
        this.init();
    }

    /**
     * @desc Fonction qui va créer le JSON de l'exercice
     */
    createJSON() {

        //Création des variables
        let JSONNewExercice = {};
        let image;

        //Création de la 1ère structure du JSON, le nom de l'exercice sera en dur car le page choix du titre n'a pas été créée
        JSONNewExercice = '{"name":{';
        //On ajoute à chaque fois le bout du JSON suivant grâce à des +=
        JSONNewExercice += '"fr":"exerciceMedor",';
        JSONNewExercice += '"de":"",';
        JSONNewExercice += '"en":"",';
        JSONNewExercice += '"it":""';
        JSONNewExercice += '},';

        //Test pour changer la valeur de la carte de jeu si elle est "flat_nature" car le nom de l'image dans la base de données est différent
        if (tableauInfos.map == "flat_nature") {
            tableauInfos.map = "flatNature";
        }

        //Création de la 2ème structure du JSON, le nom de la carte qui sera le même que celui de l'image
        JSONNewExercice += '"map":"' + tableauInfos.map + '",';
        JSONNewExercice += '"placements":[';

        //Pour chaque emplacement on va vérifier s'il faut afficher une image
        //Emplacement 0
        //Initialisation de la variable image à null au début
        image = null;
        //Début de la structure
        JSONNewExercice += '{';
        JSONNewExercice += '"num":"0",';

        //Test pour savoir si l'image du chien contient cet emplacement
        if (tableauInfos.dog == "0") {
            //Si oui alors on ajoute l'image du chien à l'emplacement
            image = '"image":["dog_brown"]';
        
        //Test pour savoir si l'image de la niche contient cet emplacement
        } else if (tableauInfos.niche == "0") {
            //Si oui alors on ajoute l'image de la niche à l'emplacement
            image = '"image":["dog_house"]';
        }
        //Test pour savoir si 1 des biscuits contient cet emplacement
        tableauInfos.biscuits.forEach(biscuit => {
            if (biscuit == "0") {
                //Si oui alors on ajoute l'image du biscuit à l'emplacement
                image = '"image":["doggy_biscuit"]';
            }
        });

        //Test pour savoir si l'image est toujours à null
        if (image == null) {
            //Si oui alors il n'y aura pas d'image à cet emplacement
            JSONNewExercice += '"image":[]';
        } else {
            //Si non alors on ajoute la valeur de l'image dans le JSON
            JSONNewExercice += image;
        }
        JSONNewExercice += '},';

        //Emplacement 1
        image = null;
        JSONNewExercice += '{';
        JSONNewExercice += '"num":"1",';

        if (tableauInfos.dog == "1") {
            image = '"image":["dog_brown"]';
        } else if (tableauInfos.niche == "1") {
            image = '"image":["dog_house"]';
        }
        tableauInfos.biscuits.forEach(biscuit => {
            if (biscuit == "1") {
                image = '"image":["doggy_biscuit"]';
            }
        });
        if (image == null) {
            JSONNewExercice += '"image":[]';
        } else {
            JSONNewExercice += image;
        }
        JSONNewExercice += '},';

        //Emplacement 2
        image = null;
        JSONNewExercice += '{';
        JSONNewExercice += '"num":"2",';

        if (tableauInfos.dog == "2") {
            image = '"image":["dog_brown"]';
        } else if (tableauInfos.niche == "2") {
            image = '"image":["dog_house"]';
        }
        tableauInfos.biscuits.forEach(biscuit => {
            if (biscuit == "2") {
                image = '"image":["doggy_biscuit"]';
            }
        });
        if (image == null) {
            JSONNewExercice += '"image":[]';
        } else {
            JSONNewExercice += image;
        }
        JSONNewExercice += '},';

        //Emplacement 3
        image = null;
        JSONNewExercice += '{';
        JSONNewExercice += '"num":"3",';

        if (tableauInfos.dog == "3") {
            image = '"image":["dog_brown"]';
        } else if (tableauInfos.niche == "3") {
            image = '"image":["dog_house"]';
        }
        tableauInfos.biscuits.forEach(biscuit => {
            if (biscuit == "3") {
                image = '"image":["doggy_biscuit"]';
            }
        });
        if (image == null) {
            JSONNewExercice += '"image":[]';
        } else {
            JSONNewExercice += image;
        }
        JSONNewExercice += '},';

        //Emplacement 4
        image = null;
        JSONNewExercice += '{';
        JSONNewExercice += '"num":"4",';

        if (tableauInfos.dog == "4") {
            image = '"image":["dog_brown"]';
        } else if (tableauInfos.niche == "4") {
            image = '"image":["dog_house"]';
        }
        tableauInfos.biscuits.forEach(biscuit => {
            if (biscuit == "4") {
                image = '"image":["doggy_biscuit"]';
            }
        });
        if (image == null) {
            JSONNewExercice += '"image":[]';
        } else {
            JSONNewExercice += image;
        }
        JSONNewExercice += '},';

        //Emplacement 5
        image = null;
        JSONNewExercice += '{';
        JSONNewExercice += '"num":"5",';

        if (tableauInfos.dog == "5") {
            image = '"image":["dog_brown"]';
        } else if (tableauInfos.niche == "5") {
            image = '"image":["dog_house"]';
        }
        tableauInfos.biscuits.forEach(biscuit => {
            if (biscuit == "5") {
                image = '"image":["doggy_biscuit"]';
            }
        });
        if (image == null) {
            JSONNewExercice += '"image":[]';
        } else {
            JSONNewExercice += image;
        }
        JSONNewExercice += '}';
        JSONNewExercice += '],';

        //Création du tableau des tokens dans le JSON
        JSONNewExercice += '"tokens":[';

        //Pour chaque token on ajoute sa valeur dans le JSON
        for (let i = 0; i < tableauInfos.solution.length - 1; i++) {
            JSONNewExercice += '"' + tableauInfos.solution[i] + '",';
        }

        //On ajoute la dernière valeur du tableau sans la virgule à la fin
        JSONNewExercice += '"' + tableauInfos.solution[tableauInfos.solution.length - 1] + '"';
        JSONNewExercice += '],';

        //Création du tableau de la solution dans le JSON
        JSONNewExercice += '"guides":[';

        //Si le guide est 5tokens alors on modifie le nom du guide pour le faire correspondre au nom du fichier
        if (tableauInfos.guide == "5Tokens") {
            tableauInfos.guide = "5TokensLoop";
        }
        JSONNewExercice += '"' + tableauInfos.guide + '",';

        //Test pour créer le bon nombre de token selon le guide utilisé
        if (tableauInfos.guide == "4Tokens") {
            //Pour le guide 4 on va créer uniquement 4 tokens
            for (let i = 0; i < tableauInfos.solution.length - 1; i++) {
                JSONNewExercice += '{';
                //On ajoute les coordonnées du token dans le JSON
                JSONNewExercice += '"x":"' + XYGuides4Tokens[i][0] + '",';
                JSONNewExercice += '"y":"' + XYGuides4Tokens[i][1] + '",';
                //On ajoute la valeur du token dans le JSON
                JSONNewExercice += '"value":"' + tableauInfos.solution[i] + '"';
                JSONNewExercice += '},';
            };

            //On créé le dernier token sans la virgule à la fin
            JSONNewExercice += '{';
            JSONNewExercice += '"x":"' + XYGuides4Tokens[tableauInfos.solution.length - 1][0] + '",';
            JSONNewExercice += '"y":"' + XYGuides4Tokens[tableauInfos.solution.length - 1][1] + '",';
            JSONNewExercice += '"value":"' + tableauInfos.solution[tableauInfos.solution.length - 1] + '"';
            JSONNewExercice += '}';

        } else if (tableauInfos.guide == "5TokensLoop") {
            //Pour le guide 5 on va créer 5 tokens
            for (let i = 0; i < tableauInfos.solution.length - 1; i++) {
                JSONNewExercice += '{';
                JSONNewExercice += '"x":"' + XYGuides5Tokens[i][0] + '",';
                JSONNewExercice += '"y":"' + XYGuides5Tokens[i][1] + '",';
                JSONNewExercice += '"value":"' + tableauInfos.solution[i] + '"';
                JSONNewExercice += '},';
            };

            JSONNewExercice += '{';
            JSONNewExercice += '"x":"' + XYGuides5Tokens[tableauInfos.solution.length - 1][0] + '",';
            JSONNewExercice += '"y":"' + XYGuides5Tokens[tableauInfos.solution.length - 1][1] + '",';
            JSONNewExercice += '"value":"' + tableauInfos.solution[tableauInfos.solution.length - 1] + '"';
            JSONNewExercice += '}';

        } else if (tableauInfos.guide == "6Tokens") {
            //Pour le guide 6 on va créer 6 tokens
            for (let i = 0; i < tableauInfos.solution.length - 1; i++) {
                JSONNewExercice += '{';
                JSONNewExercice += '"x":"' + XYGuides6Tokens[i][0] + '",';
                JSONNewExercice += '"y":"' + XYGuides6Tokens[i][1] + '",';
                JSONNewExercice += '"value":"' + tableauInfos.solution[i] + '"';
                JSONNewExercice += '},';
            };

            JSONNewExercice += '{';
            JSONNewExercice += '"x":"' + XYGuides6Tokens[tableauInfos.solution.length - 1][0] + '",';
            JSONNewExercice += '"y":"' + XYGuides6Tokens[tableauInfos.solution.length - 1][1] + '",';
            JSONNewExercice += '"value":"' + tableauInfos.solution[tableauInfos.solution.length - 1] + '"';
            JSONNewExercice += '}';
        }

        //Voici la fin du JSON
        JSONNewExercice += ']';
        JSONNewExercice += '}';

        //On envoie le JSON au serveur pour qu'il soit enregistré dans la base de données
        this.refGame.global.resources.saveExercice(JSONNewExercice, function (result) {
            Swal.fire(
                result.message,
                undefined,
                result.type
            );
            if (result) {
                // On réinitialise toute la page
                this.clear();
                this.elements = [];
                this.show();
            }
        }.bind(this));


    }


}
