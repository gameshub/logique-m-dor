class GuideTokenBasic extends Asset {

    constructor(refGame, guide) {
        super();
        this.refGame = refGame;
        this.container = new PIXI.Container();
        this.container.width = 200;
        this.container.height = 600;

        if (guide == "4Tokens") {

            //other images
            this.dogBrown = new Shape(50, 400, 50, 50, this.refGame.global.resources.getImage('dog_brown'), '');
            this.dogHouse = new Shape(550, 400, 50, 50, this.refGame.global.resources.getImage('dog_house'), '');

            //add others images
            this.init(this.dogBrown, this.dogHouse);

            //startBtn
            let startBtn = new PIXI.Graphics();
            startBtn.beginFill(0x4bacc6, 1);
            startBtn.lineStyle(2, 0x357d91, 1);
            startBtn.moveTo(35, 385)
            startBtn.lineTo(65, 400)
            startBtn.lineTo(35, 415)
            startBtn.lineTo(35, 385);
            startBtn.endFill();

            //add startBtn
            this.container.addChild(startBtn);

            this.guides = [];
            this.tokens = [];

            //XY des Guides
            this.XYGuides = [[165, 400, ""], [255, 400, ""], [345, 400, ""], [435, 400, ""]];

            //XY des lignes entre guides
            this.XYArrowUnderGuides = [[100, 400, 550, 400, true]];

            //Ajout des guides
            this.XYGuides.forEach(xy => {
                let guide = new Guide(xy[0], xy[1], 40, xy[2]);
                this.guides.push(guide)
            });

            //add arrow under guides
            this.XYArrowUnderGuides.forEach(xy => {
                this.init(new Arrow(xy[0], xy[1], xy[2], xy[3], 0x000000, xy[4]));
            });

            //add guides
            this.guides.forEach(guide => {
                this.init(guide)
            });
            this.numAttemps = 0;

            //set des jetons des guides à null
            for (let i = 0; i < this.guides.length; i++) {
                this.guides[i].setShape(null);
            }

        } else if (guide == "5Tokens") {

            //autres images
            this.dogBrown = new Shape(50, 405, 50, 50, this.refGame.global.resources.getImage('dog_brown'), '');
            this.dogHouse = new Shape(550, 440, 50, 50, this.refGame.global.resources.getImage('dog_house'), '');

            //ajout des images
            this.init(this.dogBrown, this.dogHouse);

            //startBtn
            let startBtn = new PIXI.Graphics();
            startBtn.beginFill(0x4bacc6, 1);
            startBtn.lineStyle(2, 0x357d91, 1);
            startBtn.moveTo(35, 390)
            startBtn.lineTo(65, 405)
            startBtn.lineTo(35, 420)
            startBtn.lineTo(35, 390);
            startBtn.endFill();

            //ajout startBtn
            this.container.addChild(startBtn);

            this.guides = [];
            this.tokens = [];

            //XY des Guides
            this.XYGuides = [[155, 405, "2X"], [250, 405, ""], [250, 480, ""], [345, 450, ""], [440, 450, "3X"]];

            //XY des petites images erreur et check des conditions
            this.XYImages = [[135, 450, 15, "error_sign"], [195, 380, 20, "check_sign"], [420, 405, 15, "error_sign"], [485, 430, 20, "check_sign"]],

                //XY des lignes entre les guides
                this.XYArrowUnderGuides = [
                    [100, 405, 155, 405, false, 6, null],
                    [155, 405, 255, 405, true, 6, null],
                    [155, 405, 155, 480, false, 6, null],
                    [152, 480, 255, 480, true, 6, null],
                    [255, 405, 345, 440, false, 6, null],
                    [345, 450, 445, 450, false, 6, null],
                    [250, 480, 345, 450, false, 6, null],
                    [345, 450, 550, 450, true, 6, null],
                    [440, 450, 440, 354.5, false, 6, [12.5, 9]],
                    [107, 357, 442, 357, false, 6, [12.5, 9]],
                    [110, 357, 110, 440, true, 6, [12.5, 9]]
                ];

            //création des guides
            this.XYGuides.forEach(xy => {
                let guide = new Guide(xy[0], xy[1], 35, xy[2]);
                this.guides.push(guide)
            });

            //ajout des lignes entre les guides
            this.XYArrowUnderGuides.forEach(xy => {
                this.init(new Arrow(xy[0], xy[1], xy[2], xy[3], 0x000000, xy[4], null, xy[5], xy[6]));
            });

            //ajout des guides
            this.guides.forEach(guide => {
                this.init(guide)
            });
            this.numAttemps = 0;

            //ajout des images d'erreurs et check
            this.XYImages.forEach(image => {
                let shape = new Shape(image[0], image[1], image[2], image[2], this.refGame.global.resources.getImage(image[3]), "");
                this.init(shape);
            });

            //Set les jetons dans les guides à null (aucun jeton dans les guides)
            for (let i = 0; i < this.guides.length; i++) {
                this.guides[i].setShape(null);
            }

        } else if (guide == "6Tokens") {

            //autres images
            this.dogBrown = new Shape(50, 395, 50, 50, this.refGame.global.resources.getImage('dog_brown'), '');
            this.dogHouse = new Shape(550, 395, 50, 50, this.refGame.global.resources.getImage('dog_house'), '');

            //ajout autres images
            this.init(this.dogBrown, this.dogHouse);

            //startBtn
            let startBtn = new PIXI.Graphics();
            startBtn.beginFill(0x4bacc6, 1);
            startBtn.lineStyle(2, 0x357d91, 1);
            startBtn.moveTo(35, 380)
            startBtn.lineTo(65, 395)
            startBtn.lineTo(35, 410)
            startBtn.lineTo(35, 380);
            startBtn.endFill();

            //ajout startBtn
            this.container.addChild(startBtn);

            this.guides = [];
            this.tokens = [];

            //XY des guides
            this.XYGuides = [[145, 395, ""], [225, 395, ""], [260, 470, ""], [340, 470, ""], [370, 395, ""], [450, 395, ""]];

            //XY des lignes entre les guides
            this.XYArrowUnderGuides = [[100, 395, 225, 395, false], [225, 395, 260, 470, false], [260, 470, 340, 470, false], [340, 470, 370, 395, false], [370, 395, 450, 395, false], [450, 395, 550, 395, true]];

            //Création des guides
            this.XYGuides.forEach(xy => {
                let guide = new Guide(xy[0], xy[1], 35, xy[2]);
                this.guides.push(guide)
            });

            //ajoout des lignes entre les guides
            this.XYArrowUnderGuides.forEach(xy => {
                this.init(new Arrow(xy[0], xy[1], xy[2], xy[3], 0x000000, xy[4]));
            });

            //ajout des guides
            this.guides.forEach(guide => {
                this.init(guide)
            });
            this.numAttemps = 0;

            //set des jetons des guides à null
            for (let i = 0; i < this.guides.length; i++) {
                this.guides[i].setShape(null);
            }
        }
    }

    init(...assets) {
        assets.forEach(asset => {
            asset.getPixiChildren().forEach(child => {
                this.container.addChild(child);
            });
        });
    }

    getPixiChildren() {
        return [this.container];
    }

    getGuides() {
        return this.guides;
    }

    //callback du listener
    onShapeMove(shape) {
        //aimant pour les guides 
        //si le jeton est près d'un guide il est mis dans le guide
        for (let i = 0; i < this.guides.length; i++) {
            let cell = this.guides[i];
            let minX = cell.x - cell.width / 1.7;
            let maxX = cell.x + cell.width / 1.7;
            let minY = cell.y - cell.width / 1.7;
            let maxY = cell.y + cell.width / 1.7;

            //enlève le jeton s'il est déplacé en dehors du guide
            if (cell.getShape() === shape && !(minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY())) {
                this.guides[i].setShape(null);
            }

            //ajout du jeton dans les guides
            if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY() && (this.guides[i].getShape() === shape || !this.guides[i].getShape())) {
                shape.setX(cell.x);
                shape.setY(cell.y);
                this.guides[i].setShape(shape);
            }
        }
    }

    //callback du listener
    onShapeEndMove(shape) {
        //aimant pour les guides 
        //si le jeton est près d'un guide il est mis dans le guide
        for (let i = 0; i < this.guides.length; i++) {
            let cell = this.guides[i];
            let minX = cell.x - cell.width / 1.7;
            let maxX = cell.x + cell.width / 1.7;
            let minY = cell.y - cell.width / 1.7;
            let maxY = cell.y + cell.width / 1.7;
            //s'il y a déjà un jeton il va mettre ce jeton dans son emplacement de base et mets le nouveau dans le guide
            if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY()) {
                shape.setX(cell.x);
                shape.setY(cell.y);
                if (this.guides[i].getShape() != null) {
                    if (this.guides[i].getShape() !== shape) {
                        this.guides[i].getShape().setX(this.guides[i].getShape().getBaseX());
                        this.guides[i].getShape().setY(this.guides[i].getShape().getBaseY());
                    }
                }
                this.guides[i].setShape(shape);
            }
        }
    }

    endGame(numAttemps) {
        if (this.onEndGame) {
            this.onEndGame(numAttemps);
        }
    }
}