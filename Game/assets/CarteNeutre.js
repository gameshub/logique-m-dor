const FLATNATURE_XYPLACEMENTS = [[75, 75], [300, 75], [525, 75], [75, 275], [300, 275], [525, 275]];
const BEACH_XYPLACEMENTS = [[75, 180], [225, 95], [225, 250], [375, 180], [525, 65], [525, 280]];
const OCEAN_XYPLACEMENTS = [[225, 65], [75, 180], [225, 180], [375, 180], [525, 180], [375, 300]];



class CarteNeutre extends Asset {

    constructor(refGame, carte) {

        super(refGame, "create");
        this.refGame = refGame;
        //
        this.background = new PIXI.Container();
        this.background.width = 600;
        this.background.height = 600;
        let img_background = refGame.global.resources.getImage(carte);
        let sprite_background = PIXI.Sprite.fromImage(img_background.image.src);
        sprite_background.anchor.x = 0.5;
        sprite_background.y = 0;
        sprite_background.x = 300;
        let largeur = 350 / img_background.getHeight() * img_background.getWidth();
        sprite_background.height = 350;
        sprite_background.width = largeur;
        this.background.addChild(sprite_background);

        //colors
        this.blue = 0x4f81bd;
        this.green = 0x9bbb59;
        this.red = 0xc0504d;
        this.black = 0x000000;

        if (carte === "flat_nature") {

            this.XYPlacements = FLATNATURE_XYPLACEMENTS;

            //Arrows between Placements
            this.arrow_first_second_red = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[1][0], this.XYPlacements[1][1], this.red);
            this.arrow_first_fifth_blue = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[4][0], this.XYPlacements[4][1], this.blue);
            this.arrow_second_third_blue = new Arrow(this.XYPlacements[1][0], this.XYPlacements[1][1], this.XYPlacements[2][0], this.XYPlacements[2][1], this.blue);
            this.arrow_second_fouth_green = new Arrow(this.XYPlacements[1][0], this.XYPlacements[1][1], this.XYPlacements[3][0], this.XYPlacements[3][1], this.green);
            this.arrow_fourth_fifth_red = new Arrow(this.XYPlacements[3][0], this.XYPlacements[3][1], this.XYPlacements[4][0], this.XYPlacements[4][1], this.red);
            this.arrow_fifth_sixth_green = new Arrow(this.XYPlacements[4][0], this.XYPlacements[4][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.green);
            this.arrow_first_third_green = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[2][0], this.XYPlacements[2][1], this.green, false, [100, 0, 500, 0]);
            this.arrow_fourth_sixth_blue = new Arrow(this.XYPlacements[3][0], this.XYPlacements[3][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.blue, false, [100, 350, 500, 350]);

            //add arrows between Placements
            this.init(this.arrow_first_second_red, this.arrow_first_fifth_blue, this.arrow_second_third_blue, this.arrow_second_fouth_green, this.arrow_fourth_fifth_red, this.arrow_fifth_sixth_green, this.arrow_first_third_green, this.arrow_fourth_sixth_blue);

        } else if (carte === "ocean") {

            this.XYPlacements = OCEAN_XYPLACEMENTS;

            //lignes entre les placements
            this.arrow_first_second_blue = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[1][0], this.XYPlacements[1][1], this.blue);
            this.arrow_first_fifth_green = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[4][0], this.XYPlacements[4][1], this.green);
            this.arrow_first_third_red = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[2][0], this.XYPlacements[2][1], this.red);
            this.arrow_second_third_green = new Arrow(this.XYPlacements[1][0], this.XYPlacements[1][1], this.XYPlacements[2][0], this.XYPlacements[2][1], this.green);
            this.arrow_second_sixth_red = new Arrow(this.XYPlacements[1][0], this.XYPlacements[1][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.red);
            this.arrow_third_fourth_blue = new Arrow(this.XYPlacements[2][0], this.XYPlacements[2][1], this.XYPlacements[3][0], this.XYPlacements[3][1], this.blue);
            this.arrow_fourth_sixth_green = new Arrow(this.XYPlacements[3][0], this.XYPlacements[3][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.green);
            this.arrow_fourth_fifth_red = new Arrow(this.XYPlacements[3][0], this.XYPlacements[3][1], this.XYPlacements[4][0], this.XYPlacements[4][1], this.red);
            this.arrow_fifth_sixth_blue = new Arrow(this.XYPlacements[4][0], this.XYPlacements[4][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.blue);

            //ajout lignes entres placements
            this.init(this.arrow_first_second_blue, this.arrow_first_fifth_green, this.arrow_first_third_red, this.arrow_second_third_green, this.arrow_second_sixth_red, this.arrow_third_fourth_blue, this.arrow_fourth_sixth_green, this.arrow_fourth_fifth_red, this.arrow_fifth_sixth_blue);

        } else if (carte === "beach") {

            this.XYPlacements = BEACH_XYPLACEMENTS;

            //Arrows between Placements
            this.arrow_first_second_blue = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[1][0], this.XYPlacements[1][1], this.green,);
            this.arrow_first_fifth_blue = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[4][0], this.XYPlacements[4][1], this.blue, false, [225, -100, 430, 45]);
            this.arrow_first_sixth_red = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.red, false, [225, 435, 430, 300]);
            this.arrow_second_third_red = new Arrow(this.XYPlacements[1][0], this.XYPlacements[1][1], this.XYPlacements[2][0], this.XYPlacements[2][1], this.red, true, null, 8, [12.5, 9]);
            this.arrow_second_sixth_red = new Arrow(this.XYPlacements[1][0], this.XYPlacements[1][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.red);
            this.arrow_third_fourth_green = new Arrow(this.XYPlacements[2][0], this.XYPlacements[2][1], this.XYPlacements[3][0], this.XYPlacements[3][1], this.green);
            this.arrow_third_sixth_blue = new Arrow(this.XYPlacements[2][0], this.XYPlacements[2][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.blue);
            this.arrow_fourth_fifth_red = new Arrow(this.XYPlacements[3][0], this.XYPlacements[3][1], this.XYPlacements[4][0], this.XYPlacements[4][1], this.red);
            this.arrow_fifth_sixth_green = new Arrow(this.XYPlacements[4][0], this.XYPlacements[4][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.green);

            //add arrows between Placements
            this.init(this.arrow_first_second_blue, this.arrow_first_fifth_blue, this.arrow_first_sixth_red, this.arrow_second_third_red, this.arrow_second_sixth_red, this.arrow_third_fourth_green, this.arrow_third_sixth_blue, this.arrow_fourth_fifth_red, this.arrow_fifth_sixth_green);

        }

        //Placements 
        this.firstPlacement = new Placement(this.XYPlacements[0][0], this.XYPlacements[0][1], 0, this.refGame,);
        this.secondPlacement = new Placement(this.XYPlacements[1][0], this.XYPlacements[1][1], 1, this.refGame);
        this.thirdPlacement = new Placement(this.XYPlacements[2][0], this.XYPlacements[2][1], 2, this.refGame,);
        this.fourthPlacement = new Placement(this.XYPlacements[3][0], this.XYPlacements[3][1], 3, this.refGame);
        this.fifthPlacement = new Placement(this.XYPlacements[4][0], this.XYPlacements[4][1], 4, this.refGame,);
        this.sixthPlacement = new Placement(this.XYPlacements[5][0], this.XYPlacements[5][1], 5, this.refGame);

        //add placements
        this.init(this.firstPlacement, this.secondPlacement, this.thirdPlacement, this.fourthPlacement, this.fifthPlacement, this.sixthPlacement);
    }

    init(...assets) {
        assets.forEach(asset => {
            asset.getPixiChildren().forEach(child => {
                this.background.addChild(child);
            });
        });
    }

    getPixiChildren() {
        return [this.background];
    }
}