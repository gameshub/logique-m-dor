class FlatNature extends Asset {

    constructor(refGame, level) {
        super();
        this.refGame = refGame;
        this.level = level;
        this.background = new PIXI.Container();
        this.background.width = 600;
        this.background.height = 600;
        let img_background = refGame.global.resources.getImage("flat_nature");
        let sprite_background = PIXI.Sprite.fromImage(img_background.image.src);
        sprite_background.anchor.x = 0.5;
        sprite_background.y = 0;
        sprite_background.x = 300;
        let largeur = 350 / img_background.getHeight() * img_background.getWidth();
        sprite_background.height = 350;
        sprite_background.width = largeur;
        this.background.addChild(sprite_background);

        //colors
        this.blue = 0x4f81bd;
        this.green = 0x9bbb59;
        this.red = 0xc0504d;
        this.black = 0x000000;

        this.XYPlacements = [[75, 75], [300, 75], [525, 75], [75, 275], [300, 275], [525, 275]];
        
        //Arrows between Placements
        this.arrow_first_second_red = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[1][0], this.XYPlacements[1][1], this.red);
        this.arrow_first_fifth_blue = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[4][0], this.XYPlacements[4][1], this.blue);
        this.arrow_second_third_blue = new Arrow(this.XYPlacements[1][0], this.XYPlacements[1][1], this.XYPlacements[2][0], this.XYPlacements[2][1], this.blue);
        this.arrow_second_fouth_green = new Arrow(this.XYPlacements[1][0], this.XYPlacements[1][1], this.XYPlacements[3][0], this.XYPlacements[3][1], this.green);
        this.arrow_fourth_fifth_red = new Arrow(this.XYPlacements[3][0], this.XYPlacements[3][1], this.XYPlacements[4][0], this.XYPlacements[4][1], this.red);
        this.arrow_fifth_sixth_green = new Arrow(this.XYPlacements[4][0], this.XYPlacements[4][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.green);
        this.arrow_first_third_green = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[2][0], this.XYPlacements[2][1], this.green, false, [100, 0, 500, 0]);
        this.arrow_fourth_sixth_blue = new Arrow(this.XYPlacements[3][0], this.XYPlacements[3][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.blue, false, [100, 350, 500, 350]);

        //add arrows between Placements
        this.init(this.arrow_first_second_red, this.arrow_first_fifth_blue, this.arrow_second_third_blue, this.arrow_second_fouth_green, this.arrow_fourth_fifth_red, this.arrow_fifth_sixth_green, this.arrow_first_third_green, this.arrow_fourth_sixth_blue);

        //placements
        let i = 0;
        this.XYPlacements.forEach(XY => {
            let img = [];
            level["placements"].forEach(plac => {
                if(plac["num"] == i && plac["image"] != null && plac["image"].length > 0){
                    img = plac["image"];
                }
            });
            this.init(new Placement(XY[0], XY[1], i, this.refGame, img));
            i++;
        });
    }

    init(...assets) {
        assets.forEach(asset => {
            asset.getPixiChildren().forEach(child => {
                this.background.addChild(child);
            });
        });
    }

    getPixiChildren() {
        return [this.background];
    }
}