class Guide extends Asset {
    constructor(x, y, width, value, shape = null) {
        super();
        this.x = x;
        this.y = y;
        this.width = width;
        this.value = value;
        this.shape = shape;
        this.container = new PIXI.Container();
        this.display();
    }

    display() {
        //Supprimer le contenu et setup de notre objet graphique
        this.container.removeChildren();
        this.container.x = 0;
        this.container.y = 0;
        this.container.width = 600;
        this.container.height = 600;
        
        //Ajouter notre forme sur l'élément graphique
        this.graphics = new PIXI.Graphics();
        this.graphics.beginFill(0xffffff);
        this.graphics.lineStyle(2, 0x000000, 1);
        if(this.value == "3X" || this.value == "2X"){
            this.graphics.drawPolygon(this.x - 32, this.y - 7, this.x, this.y - 35, this.x + 32, this.y - 7, this.x + 20, this.y + 28, this.x - 20, this.y + 28);
        }else{
            this.graphics.drawCircle(this.x, this.y, this.width);
        }
        this.graphics.endFill();
        this.graphics.interactive = true;
        this.container.addChild(this.graphics);
    }

    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        //Mise à jour de l'affichage
        this.display();
    }

    setX(x) {
        this.x = x;
        //Mise à jour de l'affichage
        this.display();
    }

    getWidth(){
        return this.width
    }

    setWidth(width){
        this.width = width;
    }

    getValue(){
        return this.value;
    }

    setValue(value){
        this.value = value;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

    getShape(){
        return this.shape;
    }

    setShape(shape){
        this.shape = shape;
    }
}