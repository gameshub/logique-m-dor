class Beach extends Asset {

    constructor(refGame, level) {
        super();
        this.refGame = refGame;
        this.level = level;
        this.background = new PIXI.Container();
        this.background.width = 600;
        this.background.height = 600;
        let img_background = refGame.global.resources.getImage("beach");
        let sprite_background = PIXI.Sprite.fromImage(img_background.image.src);
        sprite_background.anchor.x = 0.5;
        sprite_background.y = 0;
        sprite_background.x = 300;
        let largeur = 350 / img_background.getHeight() * img_background.getWidth(); 
        sprite_background.height = 350;
        sprite_background.width = largeur;
        this.background.addChild(sprite_background);

        //colors
        this.blue = 0x4f81bd;
        this.green = 0x9bbb59;
        this.red = 0xc0504d;
        this.black = 0x000000;

        this.XYPlacements = [[75, 180], [225, 95], [225, 250], [375, 180], [525, 65], [525, 280]];

        //Arrows between Placements
        this.arrow_first_second_blue = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[1][0], this.XYPlacements[1][1], this.green,);
        this.arrow_first_fifth_blue = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[4][0], this.XYPlacements[4][1], this.blue, false, [225,-100,430,45]);
        this.arrow_first_sixth_red = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.red, false, [225,435,430,300]);
        this.arrow_second_third_red = new Arrow(this.XYPlacements[1][0], this.XYPlacements[1][1], this.XYPlacements[2][0], this.XYPlacements[2][1], this.red, true, null, 8, [12.5,9]);
        this.arrow_second_sixth_red = new Arrow(this.XYPlacements[1][0], this.XYPlacements[1][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.red);
        this.arrow_third_fourth_green = new Arrow(this.XYPlacements[2][0], this.XYPlacements[2][1], this.XYPlacements[3][0], this.XYPlacements[3][1], this.green);
        this.arrow_third_sixth_blue = new Arrow(this.XYPlacements[2][0], this.XYPlacements[2][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.blue);
        this.arrow_fourth_fifth_red = new Arrow(this.XYPlacements[3][0], this.XYPlacements[3][1], this.XYPlacements[4][0], this.XYPlacements[4][1], this.red);
        this.arrow_fifth_sixth_green = new Arrow(this.XYPlacements[4][0], this.XYPlacements[4][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.green);
      
        //add arrows between Placements
        this.init(this.arrow_first_second_blue, this.arrow_first_fifth_blue, this.arrow_first_sixth_red ,this.arrow_second_third_red, this.arrow_second_sixth_red, this.arrow_third_fourth_green, this.arrow_third_sixth_blue, this.arrow_fourth_fifth_red, this.arrow_fifth_sixth_green);

        //placements        
        let i = 0;
        this.XYPlacements.forEach(XY => {
            let img = [];
            level["placements"].forEach(plac => {
                if(plac["num"] == i && plac["image"] != null && plac["image"].length > 0){
                    img = plac["image"];
                }
            });
            this.init(new Placement(XY[0], XY[1], i, this.refGame, img)); 
            i++;
        });
    }

    init(...assets) {
        assets.forEach(asset => {
            asset.getPixiChildren().forEach(child => {
                this.background.addChild(child);
            });
        });
    }

    getPixiChildren() {
        return [this.background];
    }
}