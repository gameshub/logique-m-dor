class Guide4Tokens extends Asset {

    constructor(refGame, guidesFromDB, onEndGame) {
        super();
        this.refGame = refGame;
        this.onEndGame = onEndGame;
        this.container = new PIXI.Container();
        this.container.width = 200;
        this.container.height = 600;

        //other images
        this.dogBrown = new Shape(50, 400, 50, 50, this.refGame.global.resources.getImage('dog_brown'), '');
        this.dogHouse = new Shape(550, 400, 50, 50, this.refGame.global.resources.getImage('dog_house'), '');

        //add others images
        this.init(this.dogBrown, this.dogHouse);

        //startBtn
        let startBtn = new PIXI.Graphics();
        startBtn.beginFill(0x4bacc6, 1);
        startBtn.lineStyle(2, 0x357d91, 1);
        startBtn.moveTo(35, 385)       
        startBtn.lineTo(65, 400)
        startBtn.lineTo(35, 415)
        startBtn.lineTo(35, 385);
        startBtn.endFill();
        startBtn.interactive = true;
        startBtn.on('pointerdown', this.onClick.bind(this));

        //add startBtn
        this.container.addChild(startBtn);

        this.guides = [];
        this.tokens = [];

        //XY des Guides
        this.XYGuides = [[165, 400, ""],[255, 400, ""],[345, 400, ""],[435, 400, ""]];

        //XY des lignes entre guides
        this.XYArrowUnderGuides = [[100, 400, 550, 400, true]];

        //Ajout des guides
        this.XYGuides.forEach(xy => {
            let guide = new Guide(xy[0], xy[1], 40, xy[2]);
            this.guides.push(guide)
        });

        //ajout de la valeur des guides
        for (let i = 1; i < guidesFromDB["guides"].length; i++) {
            for (let j = 0; j < this.guides.length; j++) {
                if(this.guides[j].getX() == guidesFromDB["guides"][i]["x"] && this.guides[j].getY() == guidesFromDB["guides"][i]["y"]){
                    this.guides[j].setValue(guidesFromDB["guides"][i]["value"]);
                }
            }
        }
    
        //add arrow under guides
        this.XYArrowUnderGuides.forEach(xy => {
            this.init(new Arrow(xy[0], xy[1], xy[2], xy[3], 0x000000, xy[4]));
        });

        //add guides
        this.guides.forEach(guide => {
            this.init(guide)
        });
        this.numAttemps = 0;

        //tokens
        let tokensIMG = {
            "blue": this.refGame.global.resources.getImage('dog_brown_sit'),
            "red": this.refGame.global.resources.getImage('dog_brown_jump'),
            "green": this.refGame.global.resources.getImage('dog_brown_bend')
        };


        //ajout des tokens
        guidesFromDB["tokens"].forEach(token => {
            let shape = new Shape(0, 0, 40, 40, tokensIMG[token], token, true, this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));
            this.tokens.push(shape)
            this.init(shape);
        });

        //set des jetons des guides à null
        for (let i = 0; i < this.guides.length; i++) {
            this.guides[i].setShape(null);
        }

        //Position des jetons aléatoire
        let xTokens = [165, 255, 345, 435];
        for (let i = 0; i < this.tokens.length; i++) {
            let random = Math.floor(Math.random() * xTokens.length);
            this.tokens[i].setBaseX(xTokens[random]);
            this.tokens[i].setBaseY(555);
            xTokens.splice(random, 1);
        }
    }

    init(...assets) {
        assets.forEach(asset => {
            asset.getPixiChildren().forEach(child => {
                this.container.addChild(child);
            });
        });
    }

    onClick() {
        let juste = 0;
        let allFull = true;
        //regarde le jetons dans chaque guide pour voir s'il est dans le bon guide
        for (let i = 0; i < this.guides.length; i++) {
            let cell = this.guides[i];
            if (cell.getShape() != null) {
                if (cell.getValue() === cell.getShape().getValeur()) {
                    juste++;
                }
            } else {
                allFull = false;
            }
        }
        //si c'est tout juste fini le jeu
        if (juste === this.guides.length) {
            this.numAttemps++;
            this.endGame(this.numAttemps);
            this.numAttemps = 0;
        } else if (allFull) {
            this.numAttemps++;
            this.refGame.global.util.showAlert(
                //Type d'alerte
                'error',
                //Texte
                this.refGame.global.resources.getOtherText('blocked'),
                //Titre
                this.refGame.global.resources.getOtherText('error'),
                //Footer
                undefined,
                //Callback
                undefined
            );
        } else {
            this.refGame.global.util.showAlert(
                //Type d'alerte
                'error',
                //Texte
                this.refGame.global.resources.getOtherText('notFull'),
                //Titre
                this.refGame.global.resources.getOtherText('error'),
                //Footer
                undefined,
                //Callback
                undefined
            );
        }
    }

    getPixiChildren() {
        return [this.container];
    }

    getGuides(){
        return this.guides;
    }

    //callback du listener
    onShapeMove(shape) {
        //aimant pour les guides 
        //si le jeton est près d'un guide il est mis dans le guide
        for (let i = 0; i < this.guides.length; i++) {
            let cell = this.guides[i];
            let minX = cell.x - cell.width / 1.7;
            let maxX = cell.x + cell.width / 1.7;
            let minY = cell.y - cell.width / 1.7;
            let maxY = cell.y + cell.width / 1.7;

            //enlève le jeton s'il est déplacé en dehors du guide
            if (cell.getShape() === shape && !(minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY())) {
                this.guides[i].setShape(null);
            }

            //ajout du jeton dans les guides
            if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY() && (this.guides[i].getShape() === shape || !this.guides[i].getShape())) {
                shape.setX(cell.x);
                shape.setY(cell.y);
                this.guides[i].setShape(shape);
            }
        }
    }
    
    //callback du listener
    onShapeEndMove(shape) {
        //aimant pour les guides 
        //si le jeton est près d'un guide il est mis dans le guide
        for (let i = 0; i < this.guides.length; i++) {
            let cell = this.guides[i];
            let minX = cell.x - cell.width / 1.7;
            let maxX = cell.x + cell.width / 1.7;
            let minY = cell.y - cell.width / 1.7;
            let maxY = cell.y + cell.width / 1.7;
            //s'il y a déjà un jeton il va mettre ce jeton dans son emplacement de base et mets le nouveau dans le guide
            if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY()) {
                shape.setX(cell.x);
                shape.setY(cell.y);
                if (this.guides[i].getShape() != null) {
                    if (this.guides[i].getShape() !== shape) {
                        this.guides[i].getShape().setX(this.guides[i].getShape().getBaseX());
                        this.guides[i].getShape().setY(this.guides[i].getShape().getBaseY());
                    }
                }
                this.guides[i].setShape(shape);
            }
        }
    }

    endGame(numAttemps) {
        if (this.onEndGame) {
            this.onEndGame(numAttemps);
        }
    }
}