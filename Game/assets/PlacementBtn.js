class PlacementBtn extends Asset {
    constructor(x, y, number, refGame, images = null, width = null) {
        super();
        this.x = x;
        this.y = y;
        this.number = number;
        this.images = images;
        this.refGame = refGame;
        this.width = width;
        this.placement = new PIXI.Graphics();
        this.container = new PIXI.Container();
        this.yPoints = [60, 10];
        this.display();

        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bouton */
        this.onClick = function () {
            console.log('Replace this action with button.setOnClick')
        };
    }

    display() {
        //Supprimer le contenu et setup de notre objet graphique
        this.container.removeChildren();
        this.container.x = 0;
        this.container.y = 0;
        this.container.width = 600;
        this.container.height = 600;

        //Ajouter notre forme sur l'élément graphique
        let text = new PIXI.Text(this.number, { fontFamily: "\"Comic Sans MS\", cursive, sans-serif", fontVariant: "small-caps", fontWeight: 900, fontSize: 30, fill: 0x000000, align: 'center' });
        text.x = this.x;
        text.y = this.y;
        text.anchor.x = 0.5;
        text.anchor.y = 0.5;


        this.placement.beginFill(0xffffff)
            .lineStyle(2, 0xef994e, 1)
            .drawCircle(this.x, this.y, 40)
            .endFill()
        this.placement.interactive = true;
        //ajout de l'evenement click
        this.placement.buttonMode = true;
        this.placement.on('pointerdown', function () {
            this.onClick()
        }.bind(this));
        this.placement.addChild(text);

        this.setImage(this.images);
        this.container.addChild(this.placement);
    }



    /**
         * Défini la fonction de callback à appeler après un click sur le bouton
         * @param {function} onClick La fonction de callback
         */
    setOnClick(onClick) {
        this.onClick = onClick;
    }

    getPixiChildren() {
        return [this.container];
    }

    getX() {
        return this.x;
    }

    setX(x) {
        this.x = x;
    }

    getY() {
        return this.y;
    }

    setY(y) {
        this.y = y;
    }

    getImage() {
        return this.images;
    }

    //Méthode pour lier une image en haut à droite de l'objet créé
    setImage(images) {
        let i = 0;
        this.images = images;
        if (this.images != null) {
            this.images.forEach(image => {
                let ima = this.refGame.global.resources.getImage(image);
                this.shape = PIXI.Sprite.fromImage(ima.image.src);
                //Définir sa position et l'ancre sur l'image, ici au centre de l'image
                this.shape.x = this.x + 15;
                this.shape.y = this.y - this.yPoints[i];
                this.shape.width = 50;
                this.shape.height = 50;
                if (image == "doggy_biscuit") {
                    this.shape.x = this.x;
                    this.shape.width = 60;
                    this.shape.height = 60;
                }
                if (this.width != null) {
                    this.shape.width = this.width;
                    this.shape.height = this.width;
                }
                i++;

                //Test pour savoir si l'image doit être placé en position 1 afin de l'effacer plus tard si besoin
                if (image == "dog_brown" || image == "dog_house" || image == "doggy_biscuit") {
                    //Si oui alors on ajoute l'image à la position 1
                    this.placement.addChildAt(this.shape, 1);
                } else {
                    //Si non la position va être généré aléatoirement selon la plus petite position libre
                    this.placement.addChild(this.shape);
                }
            });
        }
    }

    //Méthode pour effacer l'image qui est lier à l'objet
    removeImage() {

        //Test pour savoir si une image est bien lier
        if (this.placement.children.length > 1) {
            //Si oui alors on efface l'image à la position 1
            this.placement.removeChildAt(1);
        }
    }

    getNumber() {
        return this.number;
    }

    setNumber(number) {
        this.number = number;
    }

    getPixiChildren() {
        return [this.container];
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

}