class Shape extends Asset {
    constructor(x, y, width, height, image, valeur, toMove = false, onMove = null, endMove = null) {
        super();
        this.x = x;
        this.y = y;
        this.baseX = x;
        this.baseY = y;
        this.width = width;
        this.height = height;
        this.image = image;
        this.valeur = valeur;
        this.toMove = toMove;
        this.onMove = onMove;
        this.endMove = endMove;
        this.stopMove = false;
        this.visible = true;

        this.container = new PIXI.Container();
        this.isFirstClick = true;
        this.data = undefined;
        this.shape = undefined;
        this.display();
    }

    display() {
        //Supprimer le contenu et setup de notre objet graphique
        this.container.removeChildren();
        this.container.x = 0;
        this.container.y = 0;
        this.container.width = 600;
        this.container.height = 600;

        this.shape = null;
        //Si la valeur est 2X ou 3X et que l'image est null du text sera mis dans la forme en blanc gras taille 30
        if(this.image == null && (this.valeur == "2X" || this.valeur == "3X")){
            this.shape = new PIXI.Text(this.valeur,{fontFamily: "\"Comic Sans MS\", cursive, sans-serif", fontVariant: "small-caps", fontWeight: 900, fontSize: 30, fill : 0xffffff, align : 'center'});
        }else{
            //Créer un "Sprite" avec une image en base64.
            //Le type d'image est "IMG" donc pour récupérer l'image, il faut faire
            //this.image.image.src
            this.shape = PIXI.Sprite.fromImage(this.image.image.src);  
        }
    
        //Définir sa position et l'ancre sur l'image, ici au centre de l'image
        this.shape.anchor.x = 0.5;
        this.shape.anchor.y = 0.5;
        this.shape.x = this.x;
        this.shape.y = this.y;
        this.shape.width = this.width + 10;
        this.shape.height = this.height + 10;

        //couleurs
        this.red = 0xc0504d;
        this.redContour = 0x8c3836;
        this.blue = 0x4f81bd;
        this.blueContour = 0x385e8b;
        this.green = 0x9bbb59;
        this.greenContour = 0x71893f;
        this.purple = 0x8064a2;
        this.purpleContour = 0x5b4675;
        
        //Ajouter notre forme sur l'élément graphique
        if(this.toMove){
            this.graphics = new PIXI.Graphics();
            switch(this.valeur) {
                case "red":
                    this.graphics.beginFill(this.red);
                    this.graphics.lineStyle(2, this.redContour, 2);
                    break;
                case "blue":
                    this.graphics.beginFill(this.blue);
                    this.graphics.lineStyle(2, this.blueContour, 2);
                    break;
                case "green":
                    this.graphics.beginFill(this.green);
                    this.graphics.lineStyle(2, this.greenContour, 2);
                    break;
                case "2X":
                    this.graphics.beginFill(this.purple);
                    this.graphics.lineStyle(2, this.purpleContour, 2);
                    break;
                case "3X":
                    this.graphics.beginFill(this.purple);
                    this.graphics.lineStyle(2, this.purpleContour, 2);
                    break;
                } 
            //Si c'est 2X ou 3X un pentagone sera dessiné au lieu du cercle.
            if(this.valeur == "3X" || this.valeur == "2X"){
                this.graphics.drawPolygon(this.x - 32, this.y - 7, this.x, this.y - 35, this.x + 32, this.y - 7, this.x + 20, this.y + 28, this.x - 20, this.y + 28);
                this.graphics.endFill();
            }else{
                this.graphics.drawCircle(this.x, this.y, this.width);
                this.graphics.endFill();
            }

            //Ajouter les listeners
            this.graphics.interactive = true;
            this.graphics.on('mousedown', this.onDragStart.bind(this))
                        .on('touchstart', this.onDragStart.bind(this))
                        .on('mouseup', this.onDragEnd.bind(this))
                        .on('mouseupoutside', this.onDragEnd.bind(this))
                        .on('touchend', this.onDragEnd.bind(this))
                        .on('touchendoutside', this.onDragEnd.bind(this))
                        .on('mousemove', this.onDragMove.bind(this))
                        .on('touchmove', this.onDragMove.bind(this));
            this.graphics.addChild(this.shape);
            this.container.addChild(this.graphics);
        }else{
            this.container.addChild(this.shape);
        }
    }

    onDragStart(event) {
        this.data = event.data;
    }


    onDragEnd() {
        this.data = null;
        if (this.endMove) {
            this.endMove(this);
        }
    }

    onDragMove() {
        if (this.data) {
            //Regarde si la forme peut bouger
            if(this.toMove == true){
                if(!this.stopMove){
                    //Position actuelle
                    let newPosition = this.data.getLocalPosition(this.shape.parent);

                    //Empêcher la forme de sortir du canvas et de devenir invisible
                    newPosition.x = newPosition.x < 45 ? 45 : newPosition.x;
                    newPosition.y = newPosition.y < 400 ? 400 : newPosition.y;
                    newPosition.x = newPosition.x > 555 ? 555 : newPosition.x;
                    newPosition.y = newPosition.y > 555 ? 555 : newPosition.y;

                    //Mise à jour la position de la forme
                    this.y = newPosition.y;
                    this.x = newPosition.x;
                    this.display();

                    //Appel au callback s'il y en a un
                    if (this.onMove) {
                        this.onMove(this);
                    }
                }
            }
        }
        
    }

    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    getBaseX(){
        return this.baseX;
    }

    getBaseY(){
        return this.baseY;
    }

    setBaseX(baseX){
        this.baseX = baseX;
        this.setX(baseX);
    }

    setBaseY(baseY){
        this.baseY = baseY;
        this.setY(baseY);
    }

    setY(y) {
        this.y = y;
        //Mise à jour de l'affichage
        this.display();
    }

    setX(x) {
        this.x = x;
        //Mise à jour de l'affichage
        this.display();
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

    getValeur(){
        return this.valeur;
    }

    setValeur(valeur){
        this.valeur = valeur;
    }

    getName(){
        return this.name;
    }

    setName(name){
        this.name = name;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

    setStopMove(stopMove){
        this.stopMove = stopMove;
    }
}