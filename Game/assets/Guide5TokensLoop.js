class Guide5TokensLoop extends Asset {

    constructor(refGame, guidesFromDB, onEndGame) {
        super();
        this.refGame = refGame;
        this.onEndGame = onEndGame;
        this.container = new PIXI.Container();
        this.container.width = 200;
        this.container.height = 600;

        //autres images
        this.dogBrown = new Shape(50, 405, 50, 50, this.refGame.global.resources.getImage('dog_brown'), '');
        this.dogHouse = new Shape(550, 440, 50, 50, this.refGame.global.resources.getImage('dog_house'), '');

        //ajout des images
        this.init(this.dogBrown, this.dogHouse);

        //startBtn
        let startBtn = new PIXI.Graphics();
        startBtn.beginFill(0x4bacc6, 1);
        startBtn.lineStyle(2, 0x357d91, 1);
        startBtn.moveTo(35, 390)
        startBtn.lineTo(65, 405)
        startBtn.lineTo(35, 420)
        startBtn.lineTo(35, 390);
        startBtn.endFill();
        startBtn.interactive = true;
        startBtn.on('pointerdown', this.onClick.bind(this));

        //ajout startBtn
        this.container.addChild(startBtn);

        this.guides = [];
        this.tokens = [];

        //XY des Guides
        this.XYGuides = [[155, 405, "2X"], [250, 405, ""], [250, 480, ""], [345, 450, ""], [440, 450, "3X"]];

        //XY des petites images erreur et check des conditions
        this.XYImages = [[135, 450, 15, "error_sign"], [195, 380, 20, "check_sign"], [420, 405, 15, "error_sign"], [485, 430, 20, "check_sign"]],

            //XY des lignes entre les guides
            this.XYArrowUnderGuides = [
                [100, 405, 155, 405, false, 6, null],
                [155, 405, 255, 405, true, 6, null],
                [155, 405, 155, 480, false, 6, null],
                [152, 480, 255, 480, true, 6, null],
                [255, 405, 345, 440, false, 6, null],
                [345, 450, 445, 450, false, 6, null],
                [250, 480, 345, 450, false, 6, null],
                [345, 450, 550, 450, true, 6, null],
                [440, 450, 440, 354.5, false, 6, [12.5, 9]],
                [107, 357, 442, 357, false, 6, [12.5, 9]],
                [110, 357, 110, 440, true, 6, [12.5, 9]]
            ];

        //création des guides
        this.XYGuides.forEach(xy => {
            let guide = new Guide(xy[0], xy[1], 35, xy[2]);
            this.guides.push(guide)
        });

        //ajout des valeurs du guides
        for (let i = 1; i < guidesFromDB["guides"].length; i++) {
            for (let j = 0; j < this.guides.length; j++) {
                if (this.guides[j].getX() == guidesFromDB["guides"][i]["x"] && this.guides[j].getY() == guidesFromDB["guides"][i]["y"]) {
                    this.guides[j].setValue(guidesFromDB["guides"][i]["value"]);
                }
            }
        }

        //ajout des lignes entre les guides
        this.XYArrowUnderGuides.forEach(xy => {
            this.init(new Arrow(xy[0], xy[1], xy[2], xy[3], 0x000000, xy[4], null, xy[5], xy[6]));
        });

        //ajout des guides
        this.guides.forEach(guide => {
            this.init(guide)
        });
        this.numAttemps = 0;

        //ajout des images d'erreurs et check
        this.XYImages.forEach(image => {
            let shape = new Shape(image[0], image[1], image[2], image[2], this.refGame.global.resources.getImage(image[3]), "");
            this.init(shape);
        });

        //tokens
        let tokensIMG = {
            "blue": this.refGame.global.resources.getImage('dog_brown_sit'),
            "red": this.refGame.global.resources.getImage('dog_brown_jump'),
            "green": this.refGame.global.resources.getImage('dog_brown_bend')
        };

        //Ajout des tokens
        guidesFromDB["tokens"].forEach(token => {
            let width = 35;
            if (token == "2X" || token == "3X") {
                width = 25;
            }
            let shape = new Shape(0, 0, width, width, tokensIMG[token], token, true, this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));
            this.tokens.push(shape)
            this.init(shape);
        });

        //Set les jetons dans les guides à null (aucun jeton dans les guides)
        for (let i = 0; i < this.guides.length; i++) {
            this.guides[i].setShape(null);
        }

        //Position des jetons aléatoire
        let xTokens = [120, 210, 300, 390, 480];
        for (let i = 0; i < this.tokens.length; i++) {
            let random = Math.floor(Math.random() * xTokens.length);
            this.tokens[i].setBaseX(xTokens[random]);
            this.tokens[i].setBaseY(555);
            xTokens.splice(random, 1);
        }
    }

    init(...assets) {
        assets.forEach(asset => {
            asset.getPixiChildren().forEach(child => {
                this.container.addChild(child);
            });
        });
    }

    onClick() {
        let juste = 0;
        let allFull = true;
        //regarde le jetons dans chaque guide pour voir s'il est dans le bon guide
        for (let i = 0; i < this.guides.length; i++) {
            let cell = this.guides[i];
            if (cell.getShape() != null) {
                if (cell.getValue() === cell.getShape().getValeur()) {
                    juste++;
                }
            } else {
                allFull = false;
            }
        }
        //si c'est tout juste fini le jeu
        if (juste === this.guides.length) {
            this.numAttemps++;
            this.endGame(this.numAttemps);
            this.numAttemps = 0;
        } else if (allFull) {
            this.numAttemps++;
            this.refGame.global.util.showAlert(
                //Type d'alerte
                'error',
                //Texte
                this.refGame.global.resources.getOtherText('blocked'),
                //Titre
                this.refGame.global.resources.getOtherText('error'),
                //Footer
                undefined,
                //Callback
                undefined
            );
        } else {
            this.refGame.global.util.showAlert(
                //Type d'alerte
                'error',
                //Texte
                this.refGame.global.resources.getOtherText('notFull'),
                //Titre
                this.refGame.global.resources.getOtherText('error'),
                //Footer
                undefined,
                //Callback
                undefined
            );
        }
    }

    getPixiChildren() {
        return [this.container];
    }

    getGuides() {
        return this.guides;
    }

    //callback du listener
    onShapeMove(shape) {
        //aimant pour les guides 
        //si le jeton est près d'un guide il est mis dans le guide
        for (let i = 0; i < this.guides.length; i++) {
            let cell = this.guides[i];
            let minX = cell.x - cell.width / 1.7;
            let maxX = cell.x + cell.width / 1.7;
            let minY = cell.y - cell.width / 1.7;
            let maxY = cell.y + cell.width / 1.7;

            //enlève le jeton s'il est déplacé en dehors du guide
            if (cell.getShape() === shape && !(minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY())) {
                this.guides[i].setShape(null);
            }

            //ajout du jeton dans les guides
            if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY() && (this.guides[i].getShape() === shape || !this.guides[i].getShape())) {
                let name = "";
                let value = "";
                if (shape.getValeur() == "2X" || shape.getValeur() == "3X") {
                    name = shape.getValeur();
                }
                if (this.guides[i].getValue() == "2X" || this.guides[i].getValue() == "3X") {
                    value = this.guides[i].getValue();
                }
                if ((name == "" && value == "") || (name != "" && value != "")) {
                    shape.setX(cell.x);
                    shape.setY(cell.y);
                    this.guides[i].setShape(shape);
                }
            }
        }
    }

    //callback du listener
    onShapeEndMove(shape) {
        //aimant pour les guides 
        //si le jeton est près d'un guide il est mis dans le guide
        for (let i = 0; i < this.guides.length; i++) {
            let cell = this.guides[i];
            let minX = cell.x - cell.width / 1.7;
            let maxX = cell.x + cell.width / 1.7;
            let minY = cell.y - cell.width / 1.7;
            let maxY = cell.y + cell.width / 1.7;
            if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY()) {
                let name = "";
                let value = "";
                if (shape.getValeur() == "2X" || shape.getValeur() == "3X") {
                    name = shape.getValeur();
                }
                if (this.guides[i].getValue() == "2X" || this.guides[i].getValue() == "3X") {
                    value = this.guides[i].getValue();
                }
                //s'il y a déjà un jeton il va mettre ce jeton dans son emplacement de base et mets le nouveau dans le guide
                if ((name == "" && value == "") || (name != "" && value != "")) {
                    shape.setX(cell.x);
                    shape.setY(cell.y);
                    if (this.guides[i].getShape() != null) {
                        if (this.guides[i].getShape() !== shape) {
                            this.guides[i].getShape().setX(this.guides[i].getShape().getBaseX());
                            this.guides[i].getShape().setY(this.guides[i].getShape().getBaseY());
                        }
                    }
                    this.guides[i].setShape(shape);
                } else {
                    shape.setX(shape.getBaseX());
                    shape.setY(shape.getBaseY());
                }
            }
        }
    }

    endGame(numAttemps) {
        if (this.onEndGame) {
            this.onEndGame(numAttemps);
        }
    }
}