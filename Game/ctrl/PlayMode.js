
class PlayMode extends Mode {

    constructor(refGame) {
        super('play');
        this.refGame = refGame;
        this.setInterfaces({
            play: new Play(refGame)
        });
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init() {
    }

    /**
     * Affiche la page de garde de la création
     */
    show() {
        this.interfaces.play.show();
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.interfaces.play.refreshLang(lang);
    }


    onFontChange(isOpendyslexic) {
        this.interfaces.play.refreshFont(isOpendyslexic)
    }
}
