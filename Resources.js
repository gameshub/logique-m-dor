define(["require", "exports", "./proto/IMG", "./Util", "./Statistics", "./proto/Log"], function (require, exports, IMG, Util, Statistics, Log) {
    "use strict";
    var util = Util.getInstance();
    var Resources = (function () {
        function Resources() {
        }
        Resources.addImage = function (name, base64, width, height, frames) {
            Resources.IMAGES[name] = new IMG(base64, width, height, frames);
        };
        Resources.addText = function (name, text) {
            Resources.TEXTS[name] = text;
        };
        Resources.addInput = function (id) {
            Resources.INPUTS.push(id);
        };
        Resources.getImage = function (name) {
            if (Resources.IMAGES[name]) {
                return Resources.IMAGES[name];
            }
            else {
                new Log('error', "Image introuvable -> " + name).show();
                return Resources.IMAGES['default_sprite_error_framework'];
            }
        };
        Resources.getOtherText = function (name, options) {
            var text = Resources.DEFAULT_TEXT;
            if (Resources.language) {
                if (Resources.TEXTS[Resources.language][name]) {
                    text = Resources.TEXTS[Resources.language][name];
                    if (options) {
                        var templateString = generateTemplateString(text);
                        text = templateString(options);
                    }
                }
                else {
                    new Log('error', "Texte introuvable -> " + name).show();
                }
            }
            else {
                new Log('error', "Langue non d\u00E9finie (Resources.changeLanguage pour la d\u00E9finir)").show();
            }
            return text;
        };
        Resources.getTutorialText = function (name, options) {
            var text = Resources.DEFAULT_TEXT;
            if (Resources.language) {
                if (Resources.TEXTS[Resources.language]['tutorial'][name]) {
                    Resources.currentTutorialText = name;
                    text = Resources.TEXTS[Resources.language]['tutorial'][name];
                    if (options) {
                        var templateString = generateTemplateString(text);
                        text = templateString(options);
                    }
                }
                else {
                    new Log('error', "Texte introuvable -> " + name).show();
                }
            }
            else {
                new Log('error', "Langue non d\u00E9finie (Resources.changeLanguage pour la d\u00E9finir)").show();
            }
            return text;
        };
        Resources.changeLanguage = function (e, lang) {
            if (lang && Resources.TEXTS[lang]) {
                Resources.language = lang;
            }
            else if (e.target.id && Resources.TEXTS[e.target.id]) {
                Resources.language = e.target.id;
            }
            else {
                new Log('error', "Langue introuvable -> " + (lang ? lang : e.target.id)).show();
            }
            $('#languageCode').text(Resources.language);
            if (Resources.currentTutorialText)
                util.setTutorialText(Resources.getTutorialText(Resources.currentTutorialText));
            Resources.INPUTS.forEach(function (b) { return $("#" + b).val(Resources.getOtherText(b)); });
            if (Resources.onLanguageChange)
                Resources.onLanguageChange(Resources.language);
        };
        Resources.callOnLanguageChange = function (callback) {
            Resources.onLanguageChange = callback;
        };
        Resources.changeDegre = function (e, deg) {
            if (deg)
                Resources.degre = deg;
            else
                Resources.degre = e.target.id;
            Statistics.setDegre(Resources.degre);
            util.onGameDataChanged();
        };
        Resources.getDegre = function () {
            return Resources.degre;
        };
        Resources.getLanguage = function () {
            return Resources.language;
        };
        Resources.getExercice = function () {
            var retour;
            $.ajax({
                type: 'POST',
                async: false,
                dataType: 'json',
                url: '/getExercice',
                data: {
                    degre: Resources.degre
                },
                success: function (result) {
                    retour = result;
                }
            });
            return retour;
        };
        Resources.saveExercice = function (exerice, callback, state=-1) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/addExercice',
                data: {
                    exercice:exerice,
					degre:Resources.getDegre(),
                    forcedState:state
                },
                success: function (result) {
                    callback(result);
                }
            });
        };
		Resources.getExercicesEleves = function () {
            var retour;
            $.ajax({
                type: 'POST',
                async: false,
                dataType: 'json',
                url: '/getExercicesEleves',
                data: {
                    degre: Resources.degre
                },
                success: function (result) {
                    retour = result;
                }
            });
            return retour;
        };

		Resources.signaler = function(message, password, exId, callback){
            $.ajax({
                type: 'POST',
                async: false,
                dataType: 'json',
                url: '/signaler',
                data: {
                    ex:exId,
                    msg:message,
                    password:password
                },
                success: callback,
                error: callback
            });
        };

		Resources.changeFont = function(e){
            if (Resources.onFontChange)
                Resources.onFontChange(e.target.checked);
        };
        Resources.callOnFontChange = function (callback) {
            Resources.onFontChange = callback;
        };
        Resources.IMAGES = {
            'default_sprite_error_framework': new IMG('iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAIAAACQkWg2AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAABKSURBVDhP3Y+xEcBADIM8XcbOap+GU4HeC4SjE41m4zzvVeZGXWRu1EXmRl1kbtRF5kZdZG7UReZGXfz3uQ11kblRF5kbdZFZzHyGpgGQGV5drAAAAABJRU5ErkJggg==', 16, 16, 1)
        };
        Resources.DEFAULT_TEXT = '404 - Text Not Found';
        Resources.TEXTS = {};
        Resources.INPUTS = [];

        Resources.setScenario = function (scenario) {
            Resources.SCENARIO = scenario;
        };
        Resources.getScenario = function () {
            return Resources.SCENARIO;
        };
        Resources.setEvaluation = function (scenario) {
            Resources.EVALUATION = scenario;
        };
        Resources.getEvaluation = function () {
            return Resources.EVALUATION;
        };
        return Resources;
    }());
    var generateTemplateString = (function () {
        var cache = {};
        function generateTemplate(template) {
            var fn = cache[template];
            if (!fn) {
                var sanitized = template
                    .replace(/\$\{([\s]*[^;\s\{]+[\s]*)\}/g, function (_, match) {
                    return "${map." + match.trim() + "}";
                })
                    .replace(/(\$\{(?!map\.)[^}]+\})/g, '');
                fn = Function('map', "return `" + sanitized + "`");
            }
            return fn;
        }
        return generateTemplate;
    })();
    $('.onclick_language').on('click', Resources.changeLanguage);
    $('.onclick_degre').on('click', Resources.changeDegre);
    $('.onclick_opendyslexic').on('click', Resources.changeFont);
    return Resources;
});
