/**
 * @interface
 * @classdesc Interface Asset, défini un asset, soit un élément du canvas comme un bouton ou une grille
 * @author Vincent Audergon
 * @version 1.0
 */
class Asset {

    /**
     * Retourne la liste des éléments PIXI d'un asset
     * @return {Object[]} les éléments PIXI
     */
    getPixiChildren(){}

    /**
     * Retourne la position y du composant
     * @return {number} posY
     */
    getY(){}
    /**
     * Retourne la position x du composant
     * @return {number} posX
     */
    getX(){}
    setY(y){}
    setX(x){}
    getWidth(){}
    getHeight(){}
    setVisible(visible){}

}
class Arrow extends Asset {
    constructor(startX, startY, toX, toY, color, arrow = false, cpXY = null, width = 8, dashed = null) {
        super();
        this.startX = startX;
        this.startY = startY;
        this.toX = toX;
        this.toY = toY;
        this.arrow = arrow;
        this.color = color;
        this.cpXY = cpXY;
        this.dashed = dashed;
        this.width = width;
        this.container = new PIXI.Container();
        this.display();
    }

    display() {
        //Supprimer le contenu et setup de notre objet graphique
        this.container.removeChildren();
        this.container.x = 0;
        this.container.y = 0;
        this.container.width = 600;
        this.container.height = 600;

        let distX = this.toX - this.startX;
        let distY = this.toY - this.startY;
        let normal = [-(distY), distX]

        let hypo = Math.sqrt(normal[0] ** 2 + normal[1] ** 2);
        normal[0] /= hypo;
        normal[1] /= hypo;

        //permet de déplacer le vecteur normal plus vers l'arrière de la ligne
        let tangent = [ -normal[1] * 57, normal[0] * 57]
        normal[0] *= 14;
        normal[1] *= 14;

        let propX = this.startX + (distX / hypo * (hypo-39));
        let propY = this.startY + (distY / hypo * (hypo-39));
        
        let line = new PIXI.Graphics();

        if(this.cpXY == null){
            let endX = this.toX;
            let endY = this.toY;
            if(this.arrow){
                endX = this.startX + (distX / hypo * (hypo - 47));
                endY = this.startY + (distY / hypo * (hypo - 47));
            }
            line.lineStyle(this.width, this.color, 1);
            line.moveTo(this.startX, this.startY);

            //si c'est traitillé il appelle la méthode pour dessinner les traits
            if(this.dashed == null){
                line.lineTo(endX, endY);
            }else{
                this.drawDashLine(line, this.startX, this.startY, endX, endY, this.dashed[0] ,this.dashed[1]);
            }
                                              
        }else{
            line
                .lineStyle(this.width, this.color, 1)
                .moveTo(this.startX, this.startY)
                .bezierCurveTo(this.cpXY[0], this.cpXY[1], this.cpXY[2], this.cpXY[3], this.toX, this.toY);

            //Flèche au bout de la flèche ronde
            if(this.arrow){
                normal = [ -(this.toY - this.cpXY[3]), this.toX - this.cpXY[2]]
                hypo = Math.sqrt(normal[0] ** 2 + normal[1] ** 2);
                normal[0] /= hypo;
                normal[1] /= hypo;    
                tangent = [ -normal[1] * 20, normal[0] * 20]
                normal[0] *= 14;
                normal[1] *= 14;
                propX = this.toX;
                propY = this.toY;
            }
        }
        
        //enlever this.cpXY == null si on veut ajouter flèche au bout des courbes
        if(this.arrow && this.cpXY == null){
            line
                //triangle bout flèche
                .beginFill(this.color, 1)
                .lineStyle(0, this.color, 1)
                .moveTo(this.toX - normal[0] + tangent[0], this.toY - normal[1] + tangent[1])
                .lineTo(this.toX + normal[0] + tangent[0], this.toY + normal[1] + tangent[1])
                .lineTo(this.toX + normal[0] + tangent[0], this.toY + normal[1] + tangent[1])
                .lineTo(propX , propY)
                .endFill();
        }
        this.container.addChild(line);
    }

    getPixiChildren() {
        return [this.container];
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

    getFromX(){
        return this.startX;
    }

    setFromX(fromX){
        this.startX = fromX;
    }

    getFromY(){
        return this.startY;
    }

    setFromY(fromY){
        this.startY = fromY;
    }

    getFromY(){
        return this.startY;
    }

    setFromY(fromY){
        this.startY = fromY;
    }

    drawDashLine(line, x, y, toX, toY, tailleTrais, tailleEspace) {
        let distX = toX - x;
        let distY = toY - y;
        let hypo = Math.sqrt(distX ** 2 + distY ** 2);

        let pointHypo = tailleTrais;

        let startX = x;
        let startY = y;

        while (pointHypo <= hypo) {
            startX = x + (distX / hypo * (pointHypo));
            startY = y + (distY / hypo * (pointHypo)); 
            
            line.lineTo(startX, startY);

            pointHypo += tailleEspace;

            startX = x + (distX / hypo * (pointHypo));
            startY = y + (distY / hypo * (pointHypo)); 

            line.moveTo(startX, startY);

            pointHypo += tailleTrais;

            //permet de dessiner un trait plus petit si besoin pour bien correspondre à la traille voulue
            if((pointHypo - hypo) > 0 && (pointHypo - tailleTrais) < hypo){
                pointHypo = hypo;
            }
        }
      };
}
class Beach extends Asset {

    constructor(refGame, level) {
        super();
        this.refGame = refGame;
        this.level = level;
        this.background = new PIXI.Container();
        this.background.width = 600;
        this.background.height = 600;
        let img_background = refGame.global.resources.getImage("beach");
        let sprite_background = PIXI.Sprite.fromImage(img_background.image.src);
        sprite_background.anchor.x = 0.5;
        sprite_background.y = 0;
        sprite_background.x = 300;
        let largeur = 350 / img_background.getHeight() * img_background.getWidth(); 
        sprite_background.height = 350;
        sprite_background.width = largeur;
        this.background.addChild(sprite_background);

        //colors
        this.blue = 0x4f81bd;
        this.green = 0x9bbb59;
        this.red = 0xc0504d;
        this.black = 0x000000;

        this.XYPlacements = [[75, 180], [225, 95], [225, 250], [375, 180], [525, 65], [525, 280]];

        //Arrows between Placements
        this.arrow_first_second_blue = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[1][0], this.XYPlacements[1][1], this.green,);
        this.arrow_first_fifth_blue = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[4][0], this.XYPlacements[4][1], this.blue, false, [225,-100,430,45]);
        this.arrow_first_sixth_red = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.red, false, [225,435,430,300]);
        this.arrow_second_third_red = new Arrow(this.XYPlacements[1][0], this.XYPlacements[1][1], this.XYPlacements[2][0], this.XYPlacements[2][1], this.red, true, null, 8, [12.5,9]);
        this.arrow_second_sixth_red = new Arrow(this.XYPlacements[1][0], this.XYPlacements[1][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.red);
        this.arrow_third_fourth_green = new Arrow(this.XYPlacements[2][0], this.XYPlacements[2][1], this.XYPlacements[3][0], this.XYPlacements[3][1], this.green);
        this.arrow_third_sixth_blue = new Arrow(this.XYPlacements[2][0], this.XYPlacements[2][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.blue);
        this.arrow_fourth_fifth_red = new Arrow(this.XYPlacements[3][0], this.XYPlacements[3][1], this.XYPlacements[4][0], this.XYPlacements[4][1], this.red);
        this.arrow_fifth_sixth_green = new Arrow(this.XYPlacements[4][0], this.XYPlacements[4][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.green);
      
        //add arrows between Placements
        this.init(this.arrow_first_second_blue, this.arrow_first_fifth_blue, this.arrow_first_sixth_red ,this.arrow_second_third_red, this.arrow_second_sixth_red, this.arrow_third_fourth_green, this.arrow_third_sixth_blue, this.arrow_fourth_fifth_red, this.arrow_fifth_sixth_green);

        //placements        
        let i = 0;
        this.XYPlacements.forEach(XY => {
            let img = [];
            level["placements"].forEach(plac => {
                if(plac["num"] == i && plac["image"] != null && plac["image"].length > 0){
                    img = plac["image"];
                }
            });
            this.init(new Placement(XY[0], XY[1], i, this.refGame, img)); 
            i++;
        });
    }

    init(...assets) {
        assets.forEach(asset => {
            asset.getPixiChildren().forEach(child => {
                this.background.addChild(child);
            });
        });
    }

    getPixiChildren() {
        return [this.background];
    }
}
/**
 * @classdesc Asset bouton
 * @author Vincent Audergon
 * @version 1.0
 */
class Button extends Asset {

    /**
     * Constructeur de l'asset bouton
     * @param {double} x Coordonnée X
     * @param {double} y Coordonnée Y
     * @param {string} label Le texte du bouton
     * @param {int} bgColor La couleur du bouton
     * @param {int} fgColor La couleur du texte
     */
    constructor(x, y, label, bgColor, fgColor, autofit = false, width = 150) {
        super();
        /** @type {double} la coordonée x */
        this.x = x;
        /** @type {double} la coordonée y */
        this.y = y;
        /** @type {string} le texte du bouton */
        this.text = label;
        /** @type {int} la couleur de fond du bouton */
        this.bgColor = bgColor;
        /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
        this.graphics = new PIXI.Graphics();
        /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
        this.lbl = new PIXI.Text(this.text, {fontFamily: 'Arial', fontSize: 18, fill: fgColor, align: 'center'})

        this.height = 0;
        this.autofit = autofit;
        this.width = width;
        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bouton */
        this.onClick = function () {
            console.log('Replace this action with button.setOnClick')
        };
        this.init();
    }

    /**
     * Initialise les éléments qui composent le bouton
     */
    init() {
        this.setText(this.text);
        this.graphics.interactive = true;
        this.graphics.buttonMode = true;
        this.graphics.on('pointerdown', function () {
            this.onClick()
        }.bind(this));
    }


    /**
     * Défini le texte à afficher sur le bouton
     * @param {string} text Le texte à afficher
     */
    setText(text) {
        this.lbl.text = text;
        this.update();
    }

    update(){
        let buttonWidth = this.getWidth();
        let buttonHeight = this.lbl.height * 1.5;
        this.height = buttonHeight;
        this.graphics.clear();
        this.graphics.beginFill(this.bgColor);
        this.graphics.drawRect(this.x - buttonWidth / 2, this.y - buttonHeight / 2, buttonWidth, buttonHeight);
        this.graphics.endFill();
        this.lbl.anchor.set(0.5);
        this.lbl.x = this.x;
        this.lbl.y = this.y;
    }

    /**
     * Défini la fonction de callback à appeler après un click sur le bouton
     * @param {function} onClick La fonction de callback
     */
    setOnClick(onClick) {
        this.onClick = onClick;
    }

    /**
     * Retourne les éléments PIXI du bouton
     * @return {Object[]} les éléments PIXI qui composent le bouton
     */
    getPixiChildren() {
        return [this.graphics, this.lbl];
    }


    updateFont(font) {
        this.lbl.style.fontFamily = font;
    }

    getHeight() {
        return this.height;
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        for (let element of this.getPixiChildren()) {
            element.visible = visible
        }
    }

    setY(y) {
        this.y = y;
        this.update();
    }

    setX(x) {
        this.x = x;
        this.update();
    }


    getWidth() {
        return this.autofit ? (this.width < this.lbl.width + 20 ? this.lbl.width + 20 : this.width) : this.width;
    }

    setbgColor(color) {
        this.bgColor = color;
        this.update();
    }

    setfgColor(color) {
        this.fgColor = color;
        this.update();
    }

    setdisabled(disabled){
       this.disabled = disabled;
    }

    getdisabled(){
        return this.disabled;
    }
}
const FLATNATURE_XYPLACEMENTS = [[75, 75], [300, 75], [525, 75], [75, 275], [300, 275], [525, 275]];
const BEACH_XYPLACEMENTS = [[75, 180], [225, 95], [225, 250], [375, 180], [525, 65], [525, 280]];
const OCEAN_XYPLACEMENTS = [[225, 65], [75, 180], [225, 180], [375, 180], [525, 180], [375, 300]];



class CarteNeutre extends Asset {

    constructor(refGame, carte) {

        super(refGame, "create");
        this.refGame = refGame;
        //
        this.background = new PIXI.Container();
        this.background.width = 600;
        this.background.height = 600;
        let img_background = refGame.global.resources.getImage(carte);
        let sprite_background = PIXI.Sprite.fromImage(img_background.image.src);
        sprite_background.anchor.x = 0.5;
        sprite_background.y = 0;
        sprite_background.x = 300;
        let largeur = 350 / img_background.getHeight() * img_background.getWidth();
        sprite_background.height = 350;
        sprite_background.width = largeur;
        this.background.addChild(sprite_background);

        //colors
        this.blue = 0x4f81bd;
        this.green = 0x9bbb59;
        this.red = 0xc0504d;
        this.black = 0x000000;

        if (carte === "flat_nature") {

            this.XYPlacements = FLATNATURE_XYPLACEMENTS;

            //Arrows between Placements
            this.arrow_first_second_red = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[1][0], this.XYPlacements[1][1], this.red);
            this.arrow_first_fifth_blue = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[4][0], this.XYPlacements[4][1], this.blue);
            this.arrow_second_third_blue = new Arrow(this.XYPlacements[1][0], this.XYPlacements[1][1], this.XYPlacements[2][0], this.XYPlacements[2][1], this.blue);
            this.arrow_second_fouth_green = new Arrow(this.XYPlacements[1][0], this.XYPlacements[1][1], this.XYPlacements[3][0], this.XYPlacements[3][1], this.green);
            this.arrow_fourth_fifth_red = new Arrow(this.XYPlacements[3][0], this.XYPlacements[3][1], this.XYPlacements[4][0], this.XYPlacements[4][1], this.red);
            this.arrow_fifth_sixth_green = new Arrow(this.XYPlacements[4][0], this.XYPlacements[4][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.green);
            this.arrow_first_third_green = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[2][0], this.XYPlacements[2][1], this.green, false, [100, 0, 500, 0]);
            this.arrow_fourth_sixth_blue = new Arrow(this.XYPlacements[3][0], this.XYPlacements[3][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.blue, false, [100, 350, 500, 350]);

            //add arrows between Placements
            this.init(this.arrow_first_second_red, this.arrow_first_fifth_blue, this.arrow_second_third_blue, this.arrow_second_fouth_green, this.arrow_fourth_fifth_red, this.arrow_fifth_sixth_green, this.arrow_first_third_green, this.arrow_fourth_sixth_blue);

        } else if (carte === "ocean") {

            this.XYPlacements = OCEAN_XYPLACEMENTS;

            //lignes entre les placements
            this.arrow_first_second_blue = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[1][0], this.XYPlacements[1][1], this.blue);
            this.arrow_first_fifth_green = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[4][0], this.XYPlacements[4][1], this.green);
            this.arrow_first_third_red = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[2][0], this.XYPlacements[2][1], this.red);
            this.arrow_second_third_green = new Arrow(this.XYPlacements[1][0], this.XYPlacements[1][1], this.XYPlacements[2][0], this.XYPlacements[2][1], this.green);
            this.arrow_second_sixth_red = new Arrow(this.XYPlacements[1][0], this.XYPlacements[1][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.red);
            this.arrow_third_fourth_blue = new Arrow(this.XYPlacements[2][0], this.XYPlacements[2][1], this.XYPlacements[3][0], this.XYPlacements[3][1], this.blue);
            this.arrow_fourth_sixth_green = new Arrow(this.XYPlacements[3][0], this.XYPlacements[3][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.green);
            this.arrow_fourth_fifth_red = new Arrow(this.XYPlacements[3][0], this.XYPlacements[3][1], this.XYPlacements[4][0], this.XYPlacements[4][1], this.red);
            this.arrow_fifth_sixth_blue = new Arrow(this.XYPlacements[4][0], this.XYPlacements[4][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.blue);

            //ajout lignes entres placements
            this.init(this.arrow_first_second_blue, this.arrow_first_fifth_green, this.arrow_first_third_red, this.arrow_second_third_green, this.arrow_second_sixth_red, this.arrow_third_fourth_blue, this.arrow_fourth_sixth_green, this.arrow_fourth_fifth_red, this.arrow_fifth_sixth_blue);

        } else if (carte === "beach") {

            this.XYPlacements = BEACH_XYPLACEMENTS;

            //Arrows between Placements
            this.arrow_first_second_blue = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[1][0], this.XYPlacements[1][1], this.green,);
            this.arrow_first_fifth_blue = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[4][0], this.XYPlacements[4][1], this.blue, false, [225, -100, 430, 45]);
            this.arrow_first_sixth_red = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.red, false, [225, 435, 430, 300]);
            this.arrow_second_third_red = new Arrow(this.XYPlacements[1][0], this.XYPlacements[1][1], this.XYPlacements[2][0], this.XYPlacements[2][1], this.red, true, null, 8, [12.5, 9]);
            this.arrow_second_sixth_red = new Arrow(this.XYPlacements[1][0], this.XYPlacements[1][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.red);
            this.arrow_third_fourth_green = new Arrow(this.XYPlacements[2][0], this.XYPlacements[2][1], this.XYPlacements[3][0], this.XYPlacements[3][1], this.green);
            this.arrow_third_sixth_blue = new Arrow(this.XYPlacements[2][0], this.XYPlacements[2][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.blue);
            this.arrow_fourth_fifth_red = new Arrow(this.XYPlacements[3][0], this.XYPlacements[3][1], this.XYPlacements[4][0], this.XYPlacements[4][1], this.red);
            this.arrow_fifth_sixth_green = new Arrow(this.XYPlacements[4][0], this.XYPlacements[4][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.green);

            //add arrows between Placements
            this.init(this.arrow_first_second_blue, this.arrow_first_fifth_blue, this.arrow_first_sixth_red, this.arrow_second_third_red, this.arrow_second_sixth_red, this.arrow_third_fourth_green, this.arrow_third_sixth_blue, this.arrow_fourth_fifth_red, this.arrow_fifth_sixth_green);

        }

        //Placements 
        this.firstPlacement = new Placement(this.XYPlacements[0][0], this.XYPlacements[0][1], 0, this.refGame,);
        this.secondPlacement = new Placement(this.XYPlacements[1][0], this.XYPlacements[1][1], 1, this.refGame);
        this.thirdPlacement = new Placement(this.XYPlacements[2][0], this.XYPlacements[2][1], 2, this.refGame,);
        this.fourthPlacement = new Placement(this.XYPlacements[3][0], this.XYPlacements[3][1], 3, this.refGame);
        this.fifthPlacement = new Placement(this.XYPlacements[4][0], this.XYPlacements[4][1], 4, this.refGame,);
        this.sixthPlacement = new Placement(this.XYPlacements[5][0], this.XYPlacements[5][1], 5, this.refGame);

        //add placements
        this.init(this.firstPlacement, this.secondPlacement, this.thirdPlacement, this.fourthPlacement, this.fifthPlacement, this.sixthPlacement);
    }

    init(...assets) {
        assets.forEach(asset => {
            asset.getPixiChildren().forEach(child => {
                this.background.addChild(child);
            });
        });
    }

    getPixiChildren() {
        return [this.background];
    }
}
class CheckBox extends Asset {

    /**
     * Créé le composant graphic
     */
    constructor(x = 0, y = 0, text = '', sizeFactor=1) {
        super();
        this.y = y;
        this.x = x;
        this.text = text;
        this.sizeFactor = sizeFactor;

        this.selected = false;

        this.elements = {
            square: new PIXI.Graphics(),
            line1: new PIXI.Graphics(),
            line2: new PIXI.Graphics(),
            text: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 18, fill: 0x000000, align: 'left', breakWords:true, wordWrap:true})
        };

        this.elements.square.interactive = true;
        this.elements.square.buttonMode = true;
        this.elements.square.on('pointerdown', function () {
            this.select();
        }.bind(this));

        this.draw();
    }


    /**
     * Modifie la position de la checkbox.
     * @param x {number} Position X
     * @param y {number} Position Y
     */
    setPosition(x, y) {
        this.x = x;
        this.y = y;
        this.draw();
    }

    setText(value) {
        this.text = value;
        this.draw();
    }

    /**
     * Affiche ou cache le composant.
     * @param visible {boolean} Est visible ?
     */
    setVisible(visible) {
        if (!visible)
            for (let element of this.getPixiChildren())
                element.visible = false;
        else {
            this.elements.text.visible = true;
            this.elements.square.visible = true;
            this.select(false);
        }
    }

    draw() {
        let line1StartX = this.x + 3*this.sizeFactor;
        let line1StartY = this.y + 20*this.sizeFactor;
        let line1EndX = this.x + 13*this.sizeFactor;
        let line1EndY = this.y + 30*this.sizeFactor;

        let line2StartX = line1EndX-5*this.sizeFactor/4;
        let line2StartY = line1EndY+5*this.sizeFactor/4;
        let line2EndX = this.x + 35*this.sizeFactor;
        let line2EndY = this.y + 8*this.sizeFactor;


        this.elements.square.clear();
        this.elements.square.lineStyle(1, 0x000000, 1);
        this.elements.square.beginFill(0xe0e0e0, 0.25);
        this.elements.square.drawRoundedRect(this.x, this.y, 38*this.sizeFactor, 38*this.sizeFactor, 5);
        this.elements.square.endFill();

        this.elements.line1.clear();
        this.elements.line1.lineStyle(5*this.sizeFactor, 0x000000).moveTo(line1StartX, line1StartY).lineTo(line1EndX, line1EndY);

        this.elements.line2.clear();
        this.elements.line2.lineStyle(5*this.sizeFactor, 0x000000).moveTo(line2StartX, line2StartY).lineTo(line2EndX, line2EndY);


        this.elements.text.text = this.text;
        this.elements.text.style.wordWrapWidth =600-Math.floor( this.x + 5 + 38*this.sizeFactor +25);
        this.elements.text.anchor.set(0.0);
        this.elements.text.x = this.x + 38*this.sizeFactor+5;
        this.elements.text.y = this.y + (38*this.sizeFactor-this.elements.text.height)/2;

    }

    getPixiChildren() {
        let objects = [];
        for (let element of Object.keys(this.elements)) {
            objects.push(this.elements[element]);
        }
        return objects;
    }

    select(toggle = true) {
        if (toggle)
            this.selected = !this.selected;
        this.elements.line1.visible = this.selected;
        this.elements.line2.visible = this.selected;
    }

    isChecked() {
        return this.selected;
    }


    updateFont(font){
        this.elements.text.style.fontFamily = font;
    }


    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }


    setY(y) {
        this.y = y;
        this.setPosition(this.x, this.y);
    }

    setX(x) {
        this.x = x;
        this.setPosition(this.x, this.y);
    }
}
/**
 * @classdesc Asset grille de dessin, retourne une séries de points lorsque l'utilisateur a dessiné un polygone
 * @author Vincent Audergon
 * @version 2.0
 */
class DrawingGrid extends Asset {

    /**
     * Constructeur de la grille de dessin
     * @param {int} col Le nombre de colonnes qui composent la grille
     * @param {int} lines Le nombre de lignes qui composent la grille
     * @param {double} width La largeur / hauteur entre chaque noeuds de la grille
     * @param {Pixi.Stage} stage Le stage Pixi
     * @param {function} onShapeCreated La fonction de callback appelée lorsqu'une forme a été dessinée
     */
    constructor(col, lines, width, stage, onShapeCreated) {
        super();
        /** @type {int} le nombre de colonnes */
        this.col = col;
        /** @type {int} le nombre de lignes */
        this.lines = lines;
        /** @type {double} la largeur / hauteur d'une cellule */
        this.width = width;
        /** @type {PIXI.Stage} le stage PIXI sur lequel dessiner les éléments de la grille */
        this.stage = stage;
        /** @type {Node[]} la liste des noeuds qui composent la grille */
        this.nodes = [];
        /** @type {Point[]} la liste des points de la forme en cours de dessin */
        this.points = [];
        /** @type {PIXI.Graphics} la liste des lignes dessinées */
        this.strLines = [];
        /** @type {Point} l'origine de la forme en cours de dessin */
        this.origin = new Point(0, 0);
        /** @type {Node} le dernier noeud rencontré lors du dessin */
        this.lastnode = undefined;
        /** @type {PIXI.Graphics} le trait qui suit le curseur lors d'un dessin */
        this.line = undefined;
        /** @type {function} fonction de callback appelée lorsqu'une forme est dessinée */
        this.onShapeCreated = onShapeCreated;
        /** @type {boolean} si un dessin est en cours */
        this.drawing = false;
        this.init();
    }

    /**
     * Initialise la grille de dessin
     */
    init() {
        this.stage.interactive = true;
        this.stage.on('pointermove', this.onPointerMove.bind(this));
        this.stage.on('pointerup', this.onReleased.bind(this));
        for (let c = 0; c < this.col; c++) {
            for (let l = 0; l < this.lines; l++) {
                this.nodes.push(new Node(c * this.width, l * this.width, this.width / 4.5, this));
            }
        }
    }

    /**
     * Réinitialise la grille de dessin
     */
    reset() {
        for (let line of this.strLines) {
            this.stage.removeChild(line);
        }
        this.strLines = [];
        this.points = [];
        this.origin = new Point(0, 0);
        this.drawing = false;
    }

    /**
     * Créer le prochain point du polygone en cours de dessin
     * @param {Node} node le {@link Node} qui défini le nouveau point
     * @return {Point} le nouveau {@link Point}
     */
    createNextPoint(node) {
        let p = new Point(node.x / this.width - this.origin.x, node.y / this.width - this.origin.y);
        p.name = String.fromCharCode(65 + this.points.length);
        return p;
    }

    /**
     * Vérifie qu'un point ne soit pas deja existant dans la grille
     * @param {Point} p le point à controller
     * @return {boolean} si le point existe déjà
     */
    containsPoint(p) {
        for (let point of this.points) {
            if (point.x === p.x && point.y === p.y) return true;
        }
        return false;
    }

    /**
     * Créer le trait affiché sur la grille lorsque l'utilisateur déssine
     * @param {Node} node le noeud de départ
     */
    initLine(node) {
        this.line = new PIXI.Graphics();
        this.stage.addChild(this.line);
        this.line.lineStyle(6, 0xFF0000, 1);
        this.line.moveTo(node.center().x, node.center().y);
    }

    /**
     * Dessine une ligne le dernier noeud rencontré et le noeud donné en argument
     * @param {Node} node le noeud jusqu'au quel faire le trait
     */
    drawStrLine(node) {
        this.stage.removeChild(this.line);
        this.initLine(node);
        let straightLine = new PIXI.Graphics();
        this.strLines.push(straightLine);
        this.stage.addChild(straightLine);
        straightLine.lineStyle(6, 0xFF, 1);
        straightLine.moveTo(this.lastnode.center().x, this.lastnode.center().y);
        straightLine.lineTo(node.center().x, node.center().y);
        this.lastnode = node;
        if (this.onLineDrawn) this.onLineDrawn();
    }

    /**
     * Retourne la liste des éléments PIXI de la grille de dessin
     * @return {Object[]} les éléments PIXI qui composent la grille
     */
    getPixiChildren() {
        let children = [];
        for (let n of this.nodes) {
            if (n.graphics) children.push(n.graphics);
            if(n.point) children.push(n.point);
        }
        return children;
    }

    /**
     * Méthode de callback appelée lorsqu'un click est effectué sur un noeud de la grille.
     * Défini le noeud comme étant le noeud de départ et crée le trait de dessin
     * @param {Event} e l'événement JavaScript
     * @param {Node} node le noeud sur lequel on a clické
     */
    onNodeClicked(e, node) {
        this.lastnode = node;
        this.initLine(node);
        this.drawing = true;
        this.points.push();
        this.origin = this.createNextPoint(node);
        let p = new Point(0, 0);
        p.name = this.origin.name;
        this.points.push(p);
    }

    /**
     * Méthode de callback appelée lorsque le click est relâché
     * @param {Event} e L'événement JavaScript
     */
    onReleased(e) {
        this.drawing = false;
        this.stage.removeChild(this.line);
        this.reset();
    }

    /**
     * Méthode de callback appelée lorsque le curseur bouge.
     * Déssine le trait à la position du curseur
     * @param {Event} e L'événement JavaScript
     */
    onPointerMove(e) {
        if (this.drawing) {
            let position = e.data.getLocalPosition(this.stage);
            this.line.lineTo(position.x, position.y);
            for (let n of this.nodes) {
                if (n.graphics.containsPoint(e.data.getLocalPosition(n.graphics.parent))) this.onNodeEncountered(e, n);
            }
        }
    }

    /**
     * Méthode de callback appelée lorsque que le curseur touche un noeud de la grille.
     * Vérifie si la forme est terminée ou si il faut dessiner un trait droit entre ce noeud et le noeud de départ.
     * @param {Event} e L'événement JavaScript
     * @param {Node} node Le noeud de la grille touché
     */
    onNodeEncountered(e, node) {
        if (this.drawing && this.lastnode !== node) { // Si le noeud rencontré n'est pas le dernier noeud
            let point = this.createNextPoint(node);
            this.drawStrLine(node);
            let len = this.points.length;
            // console.log("Diagonale : ");
            // if (len >= 2) {
            //     let val1 = this.points[len - 2].sub(this.points[len - 1]);
            //     let val2 =  this.points[len - 1].sub(point);
            //     console.log("############################################################");
            //     console.log("Point len - 2 : ");
            //     console.log(this.points[len - 2]);
            //     console.log("Point len - 1 : ");
            //     console.log(this.points[len - 1]);
            //     console.log("Point :");
            //     console.log(point);
            //     console.log("len-2 - len-1 :");
            //     console.log(val1);
            //     console.log("len-1 - len :");
            //     console.log(val2);
            //
            //     if ((this.points[len - 1].x === this.points[len - 2].x && this.points[len - 1].x === point.x) || //Al. horizontal
            //         (this.points[len - 1].y === this.points[len - 2].y && this.points[len - 1].y === point.y) || //Al. vertical
            //         (val1.equals(val2))) { //Al. diagonale
            //         //Si trois points sont allignés on ne crée pas un nouveau mais on déplace le dernier
            //         if (point.equals(this.points[0]) && len >= 3) {
            //             //Si c'est le dernier point, on efface le dernier
            //             this.points.pop();
            //             this.pointsCalcul.push(point);
            //         } else if (!this.containsPoint(point)) {
            //             //Si c'est pas le dernier et qu'il n'exite pas encore on déplace le dernier
            //             point.name = this.points[len - 1].name;
            //             this.points[len - 1] = point;
            //             this.pointsCalcul.push(point);
            //         }
            //     } else if (!this.containsPoint(point)) {
            //         this.points.push(point);
            //         this.pointsCalcul.push(point);
            //     }
            // }
            if (!this.containsPoint(point)) {
                this.points.push(point);
            }
            if (this.points.length >= 3 && point.equals(this.points[0])) { //Sinon, si il correspond à l'origine
                //Fin de la forme

                this.drawStrLine(node);
                this.onShapeCreated(new Shape(0, 0, this.points, this.width, 'unknown', {}, this.points[0]));
                this.reset();
            }
        }
    }

    /**
     * Défini la fonction de callback appelée lorsqu'une ligne est dessinée
     * @param {function} onLineDrawn La fonction de callback
     */
    setOnLineDrawn(onLineDrawn) {
        this.onLineDrawn = onLineDrawn;
    }

    /**
     * Défini la fonction de callback lorsqu'un polygone est créé sur le canvas
     * @param {function} onShapeCreated La fonction de callback
     */
    setOnShapeCreated(onShapeCreated) {
        this.onShapeCreated = onShapeCreated;
    }

}
class FlatNature extends Asset {

    constructor(refGame, level) {
        super();
        this.refGame = refGame;
        this.level = level;
        this.background = new PIXI.Container();
        this.background.width = 600;
        this.background.height = 600;
        let img_background = refGame.global.resources.getImage("flat_nature");
        let sprite_background = PIXI.Sprite.fromImage(img_background.image.src);
        sprite_background.anchor.x = 0.5;
        sprite_background.y = 0;
        sprite_background.x = 300;
        let largeur = 350 / img_background.getHeight() * img_background.getWidth();
        sprite_background.height = 350;
        sprite_background.width = largeur;
        this.background.addChild(sprite_background);

        //colors
        this.blue = 0x4f81bd;
        this.green = 0x9bbb59;
        this.red = 0xc0504d;
        this.black = 0x000000;

        this.XYPlacements = [[75, 75], [300, 75], [525, 75], [75, 275], [300, 275], [525, 275]];
        
        //Arrows between Placements
        this.arrow_first_second_red = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[1][0], this.XYPlacements[1][1], this.red);
        this.arrow_first_fifth_blue = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[4][0], this.XYPlacements[4][1], this.blue);
        this.arrow_second_third_blue = new Arrow(this.XYPlacements[1][0], this.XYPlacements[1][1], this.XYPlacements[2][0], this.XYPlacements[2][1], this.blue);
        this.arrow_second_fouth_green = new Arrow(this.XYPlacements[1][0], this.XYPlacements[1][1], this.XYPlacements[3][0], this.XYPlacements[3][1], this.green);
        this.arrow_fourth_fifth_red = new Arrow(this.XYPlacements[3][0], this.XYPlacements[3][1], this.XYPlacements[4][0], this.XYPlacements[4][1], this.red);
        this.arrow_fifth_sixth_green = new Arrow(this.XYPlacements[4][0], this.XYPlacements[4][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.green);
        this.arrow_first_third_green = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[2][0], this.XYPlacements[2][1], this.green, false, [100, 0, 500, 0]);
        this.arrow_fourth_sixth_blue = new Arrow(this.XYPlacements[3][0], this.XYPlacements[3][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.blue, false, [100, 350, 500, 350]);

        //add arrows between Placements
        this.init(this.arrow_first_second_red, this.arrow_first_fifth_blue, this.arrow_second_third_blue, this.arrow_second_fouth_green, this.arrow_fourth_fifth_red, this.arrow_fifth_sixth_green, this.arrow_first_third_green, this.arrow_fourth_sixth_blue);

        //placements
        let i = 0;
        this.XYPlacements.forEach(XY => {
            let img = [];
            level["placements"].forEach(plac => {
                if(plac["num"] == i && plac["image"] != null && plac["image"].length > 0){
                    img = plac["image"];
                }
            });
            this.init(new Placement(XY[0], XY[1], i, this.refGame, img));
            i++;
        });
    }

    init(...assets) {
        assets.forEach(asset => {
            asset.getPixiChildren().forEach(child => {
                this.background.addChild(child);
            });
        });
    }

    getPixiChildren() {
        return [this.background];
    }
}
class Guide extends Asset {
    constructor(x, y, width, value, shape = null) {
        super();
        this.x = x;
        this.y = y;
        this.width = width;
        this.value = value;
        this.shape = shape;
        this.container = new PIXI.Container();
        this.display();
    }

    display() {
        //Supprimer le contenu et setup de notre objet graphique
        this.container.removeChildren();
        this.container.x = 0;
        this.container.y = 0;
        this.container.width = 600;
        this.container.height = 600;
        
        //Ajouter notre forme sur l'élément graphique
        this.graphics = new PIXI.Graphics();
        this.graphics.beginFill(0xffffff);
        this.graphics.lineStyle(2, 0x000000, 1);
        if(this.value == "3X" || this.value == "2X"){
            this.graphics.drawPolygon(this.x - 32, this.y - 7, this.x, this.y - 35, this.x + 32, this.y - 7, this.x + 20, this.y + 28, this.x - 20, this.y + 28);
        }else{
            this.graphics.drawCircle(this.x, this.y, this.width);
        }
        this.graphics.endFill();
        this.graphics.interactive = true;
        this.container.addChild(this.graphics);
    }

    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        //Mise à jour de l'affichage
        this.display();
    }

    setX(x) {
        this.x = x;
        //Mise à jour de l'affichage
        this.display();
    }

    getWidth(){
        return this.width
    }

    setWidth(width){
        this.width = width;
    }

    getValue(){
        return this.value;
    }

    setValue(value){
        this.value = value;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

    getShape(){
        return this.shape;
    }

    setShape(shape){
        this.shape = shape;
    }
}
class Guide4Tokens extends Asset {

    constructor(refGame, guidesFromDB, onEndGame) {
        super();
        this.refGame = refGame;
        this.onEndGame = onEndGame;
        this.container = new PIXI.Container();
        this.container.width = 200;
        this.container.height = 600;

        //other images
        this.dogBrown = new Shape(50, 400, 50, 50, this.refGame.global.resources.getImage('dog_brown'), '');
        this.dogHouse = new Shape(550, 400, 50, 50, this.refGame.global.resources.getImage('dog_house'), '');

        //add others images
        this.init(this.dogBrown, this.dogHouse);

        //startBtn
        let startBtn = new PIXI.Graphics();
        startBtn.beginFill(0x4bacc6, 1);
        startBtn.lineStyle(2, 0x357d91, 1);
        startBtn.moveTo(35, 385)       
        startBtn.lineTo(65, 400)
        startBtn.lineTo(35, 415)
        startBtn.lineTo(35, 385);
        startBtn.endFill();
        startBtn.interactive = true;
        startBtn.on('pointerdown', this.onClick.bind(this));

        //add startBtn
        this.container.addChild(startBtn);

        this.guides = [];
        this.tokens = [];

        //XY des Guides
        this.XYGuides = [[165, 400, ""],[255, 400, ""],[345, 400, ""],[435, 400, ""]];

        //XY des lignes entre guides
        this.XYArrowUnderGuides = [[100, 400, 550, 400, true]];

        //Ajout des guides
        this.XYGuides.forEach(xy => {
            let guide = new Guide(xy[0], xy[1], 40, xy[2]);
            this.guides.push(guide)
        });

        //ajout de la valeur des guides
        for (let i = 1; i < guidesFromDB["guides"].length; i++) {
            for (let j = 0; j < this.guides.length; j++) {
                if(this.guides[j].getX() == guidesFromDB["guides"][i]["x"] && this.guides[j].getY() == guidesFromDB["guides"][i]["y"]){
                    this.guides[j].setValue(guidesFromDB["guides"][i]["value"]);
                }
            }
        }
    
        //add arrow under guides
        this.XYArrowUnderGuides.forEach(xy => {
            this.init(new Arrow(xy[0], xy[1], xy[2], xy[3], 0x000000, xy[4]));
        });

        //add guides
        this.guides.forEach(guide => {
            this.init(guide)
        });
        this.numAttemps = 0;

        //tokens
        let tokensIMG = {
            "blue": this.refGame.global.resources.getImage('dog_brown_sit'),
            "red": this.refGame.global.resources.getImage('dog_brown_jump'),
            "green": this.refGame.global.resources.getImage('dog_brown_bend')
        };


        //ajout des tokens
        guidesFromDB["tokens"].forEach(token => {
            let shape = new Shape(0, 0, 40, 40, tokensIMG[token], token, true, this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));
            this.tokens.push(shape)
            this.init(shape);
        });

        //set des jetons des guides à null
        for (let i = 0; i < this.guides.length; i++) {
            this.guides[i].setShape(null);
        }

        //Position des jetons aléatoire
        let xTokens = [165, 255, 345, 435];
        for (let i = 0; i < this.tokens.length; i++) {
            let random = Math.floor(Math.random() * xTokens.length);
            this.tokens[i].setBaseX(xTokens[random]);
            this.tokens[i].setBaseY(555);
            xTokens.splice(random, 1);
        }
    }

    init(...assets) {
        assets.forEach(asset => {
            asset.getPixiChildren().forEach(child => {
                this.container.addChild(child);
            });
        });
    }

    onClick() {
        let juste = 0;
        let allFull = true;
        //regarde le jetons dans chaque guide pour voir s'il est dans le bon guide
        for (let i = 0; i < this.guides.length; i++) {
            let cell = this.guides[i];
            if (cell.getShape() != null) {
                if (cell.getValue() === cell.getShape().getValeur()) {
                    juste++;
                }
            } else {
                allFull = false;
            }
        }
        //si c'est tout juste fini le jeu
        if (juste === this.guides.length) {
            this.numAttemps++;
            this.endGame(this.numAttemps);
            this.numAttemps = 0;
        } else if (allFull) {
            this.numAttemps++;
            this.refGame.global.util.showAlert(
                //Type d'alerte
                'error',
                //Texte
                this.refGame.global.resources.getOtherText('blocked'),
                //Titre
                this.refGame.global.resources.getOtherText('error'),
                //Footer
                undefined,
                //Callback
                undefined
            );
        } else {
            this.refGame.global.util.showAlert(
                //Type d'alerte
                'error',
                //Texte
                this.refGame.global.resources.getOtherText('notFull'),
                //Titre
                this.refGame.global.resources.getOtherText('error'),
                //Footer
                undefined,
                //Callback
                undefined
            );
        }
    }

    getPixiChildren() {
        return [this.container];
    }

    getGuides(){
        return this.guides;
    }

    //callback du listener
    onShapeMove(shape) {
        //aimant pour les guides 
        //si le jeton est près d'un guide il est mis dans le guide
        for (let i = 0; i < this.guides.length; i++) {
            let cell = this.guides[i];
            let minX = cell.x - cell.width / 1.7;
            let maxX = cell.x + cell.width / 1.7;
            let minY = cell.y - cell.width / 1.7;
            let maxY = cell.y + cell.width / 1.7;

            //enlève le jeton s'il est déplacé en dehors du guide
            if (cell.getShape() === shape && !(minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY())) {
                this.guides[i].setShape(null);
            }

            //ajout du jeton dans les guides
            if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY() && (this.guides[i].getShape() === shape || !this.guides[i].getShape())) {
                shape.setX(cell.x);
                shape.setY(cell.y);
                this.guides[i].setShape(shape);
            }
        }
    }
    
    //callback du listener
    onShapeEndMove(shape) {
        //aimant pour les guides 
        //si le jeton est près d'un guide il est mis dans le guide
        for (let i = 0; i < this.guides.length; i++) {
            let cell = this.guides[i];
            let minX = cell.x - cell.width / 1.7;
            let maxX = cell.x + cell.width / 1.7;
            let minY = cell.y - cell.width / 1.7;
            let maxY = cell.y + cell.width / 1.7;
            //s'il y a déjà un jeton il va mettre ce jeton dans son emplacement de base et mets le nouveau dans le guide
            if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY()) {
                shape.setX(cell.x);
                shape.setY(cell.y);
                if (this.guides[i].getShape() != null) {
                    if (this.guides[i].getShape() !== shape) {
                        this.guides[i].getShape().setX(this.guides[i].getShape().getBaseX());
                        this.guides[i].getShape().setY(this.guides[i].getShape().getBaseY());
                    }
                }
                this.guides[i].setShape(shape);
            }
        }
    }

    endGame(numAttemps) {
        if (this.onEndGame) {
            this.onEndGame(numAttemps);
        }
    }
}
class Guide5TokensLoop extends Asset {

    constructor(refGame, guidesFromDB, onEndGame) {
        super();
        this.refGame = refGame;
        this.onEndGame = onEndGame;
        this.container = new PIXI.Container();
        this.container.width = 200;
        this.container.height = 600;

        //autres images
        this.dogBrown = new Shape(50, 405, 50, 50, this.refGame.global.resources.getImage('dog_brown'), '');
        this.dogHouse = new Shape(550, 440, 50, 50, this.refGame.global.resources.getImage('dog_house'), '');

        //ajout des images
        this.init(this.dogBrown, this.dogHouse);

        //startBtn
        let startBtn = new PIXI.Graphics();
        startBtn.beginFill(0x4bacc6, 1);
        startBtn.lineStyle(2, 0x357d91, 1);
        startBtn.moveTo(35, 390)
        startBtn.lineTo(65, 405)
        startBtn.lineTo(35, 420)
        startBtn.lineTo(35, 390);
        startBtn.endFill();
        startBtn.interactive = true;
        startBtn.on('pointerdown', this.onClick.bind(this));

        //ajout startBtn
        this.container.addChild(startBtn);

        this.guides = [];
        this.tokens = [];

        //XY des Guides
        this.XYGuides = [[155, 405, "2X"], [250, 405, ""], [250, 480, ""], [345, 450, ""], [440, 450, "3X"]];

        //XY des petites images erreur et check des conditions
        this.XYImages = [[135, 450, 15, "error_sign"], [195, 380, 20, "check_sign"], [420, 405, 15, "error_sign"], [485, 430, 20, "check_sign"]],

            //XY des lignes entre les guides
            this.XYArrowUnderGuides = [
                [100, 405, 155, 405, false, 6, null],
                [155, 405, 255, 405, true, 6, null],
                [155, 405, 155, 480, false, 6, null],
                [152, 480, 255, 480, true, 6, null],
                [255, 405, 345, 440, false, 6, null],
                [345, 450, 445, 450, false, 6, null],
                [250, 480, 345, 450, false, 6, null],
                [345, 450, 550, 450, true, 6, null],
                [440, 450, 440, 354.5, false, 6, [12.5, 9]],
                [107, 357, 442, 357, false, 6, [12.5, 9]],
                [110, 357, 110, 440, true, 6, [12.5, 9]]
            ];

        //création des guides
        this.XYGuides.forEach(xy => {
            let guide = new Guide(xy[0], xy[1], 35, xy[2]);
            this.guides.push(guide)
        });

        //ajout des valeurs du guides
        for (let i = 1; i < guidesFromDB["guides"].length; i++) {
            for (let j = 0; j < this.guides.length; j++) {
                if (this.guides[j].getX() == guidesFromDB["guides"][i]["x"] && this.guides[j].getY() == guidesFromDB["guides"][i]["y"]) {
                    this.guides[j].setValue(guidesFromDB["guides"][i]["value"]);
                }
            }
        }

        //ajout des lignes entre les guides
        this.XYArrowUnderGuides.forEach(xy => {
            this.init(new Arrow(xy[0], xy[1], xy[2], xy[3], 0x000000, xy[4], null, xy[5], xy[6]));
        });

        //ajout des guides
        this.guides.forEach(guide => {
            this.init(guide)
        });
        this.numAttemps = 0;

        //ajout des images d'erreurs et check
        this.XYImages.forEach(image => {
            let shape = new Shape(image[0], image[1], image[2], image[2], this.refGame.global.resources.getImage(image[3]), "");
            this.init(shape);
        });

        //tokens
        let tokensIMG = {
            "blue": this.refGame.global.resources.getImage('dog_brown_sit'),
            "red": this.refGame.global.resources.getImage('dog_brown_jump'),
            "green": this.refGame.global.resources.getImage('dog_brown_bend')
        };

        //Ajout des tokens
        guidesFromDB["tokens"].forEach(token => {
            let width = 35;
            if (token == "2X" || token == "3X") {
                width = 25;
            }
            let shape = new Shape(0, 0, width, width, tokensIMG[token], token, true, this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));
            this.tokens.push(shape)
            this.init(shape);
        });

        //Set les jetons dans les guides à null (aucun jeton dans les guides)
        for (let i = 0; i < this.guides.length; i++) {
            this.guides[i].setShape(null);
        }

        //Position des jetons aléatoire
        let xTokens = [120, 210, 300, 390, 480];
        for (let i = 0; i < this.tokens.length; i++) {
            let random = Math.floor(Math.random() * xTokens.length);
            this.tokens[i].setBaseX(xTokens[random]);
            this.tokens[i].setBaseY(555);
            xTokens.splice(random, 1);
        }
    }

    init(...assets) {
        assets.forEach(asset => {
            asset.getPixiChildren().forEach(child => {
                this.container.addChild(child);
            });
        });
    }

    onClick() {
        let juste = 0;
        let allFull = true;
        //regarde le jetons dans chaque guide pour voir s'il est dans le bon guide
        for (let i = 0; i < this.guides.length; i++) {
            let cell = this.guides[i];
            if (cell.getShape() != null) {
                if (cell.getValue() === cell.getShape().getValeur()) {
                    juste++;
                }
            } else {
                allFull = false;
            }
        }
        //si c'est tout juste fini le jeu
        if (juste === this.guides.length) {
            this.numAttemps++;
            this.endGame(this.numAttemps);
            this.numAttemps = 0;
        } else if (allFull) {
            this.numAttemps++;
            this.refGame.global.util.showAlert(
                //Type d'alerte
                'error',
                //Texte
                this.refGame.global.resources.getOtherText('blocked'),
                //Titre
                this.refGame.global.resources.getOtherText('error'),
                //Footer
                undefined,
                //Callback
                undefined
            );
        } else {
            this.refGame.global.util.showAlert(
                //Type d'alerte
                'error',
                //Texte
                this.refGame.global.resources.getOtherText('notFull'),
                //Titre
                this.refGame.global.resources.getOtherText('error'),
                //Footer
                undefined,
                //Callback
                undefined
            );
        }
    }

    getPixiChildren() {
        return [this.container];
    }

    getGuides() {
        return this.guides;
    }

    //callback du listener
    onShapeMove(shape) {
        //aimant pour les guides 
        //si le jeton est près d'un guide il est mis dans le guide
        for (let i = 0; i < this.guides.length; i++) {
            let cell = this.guides[i];
            let minX = cell.x - cell.width / 1.7;
            let maxX = cell.x + cell.width / 1.7;
            let minY = cell.y - cell.width / 1.7;
            let maxY = cell.y + cell.width / 1.7;

            //enlève le jeton s'il est déplacé en dehors du guide
            if (cell.getShape() === shape && !(minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY())) {
                this.guides[i].setShape(null);
            }

            //ajout du jeton dans les guides
            if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY() && (this.guides[i].getShape() === shape || !this.guides[i].getShape())) {
                let name = "";
                let value = "";
                if (shape.getValeur() == "2X" || shape.getValeur() == "3X") {
                    name = shape.getValeur();
                }
                if (this.guides[i].getValue() == "2X" || this.guides[i].getValue() == "3X") {
                    value = this.guides[i].getValue();
                }
                if ((name == "" && value == "") || (name != "" && value != "")) {
                    shape.setX(cell.x);
                    shape.setY(cell.y);
                    this.guides[i].setShape(shape);
                }
            }
        }
    }

    //callback du listener
    onShapeEndMove(shape) {
        //aimant pour les guides 
        //si le jeton est près d'un guide il est mis dans le guide
        for (let i = 0; i < this.guides.length; i++) {
            let cell = this.guides[i];
            let minX = cell.x - cell.width / 1.7;
            let maxX = cell.x + cell.width / 1.7;
            let minY = cell.y - cell.width / 1.7;
            let maxY = cell.y + cell.width / 1.7;
            if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY()) {
                let name = "";
                let value = "";
                if (shape.getValeur() == "2X" || shape.getValeur() == "3X") {
                    name = shape.getValeur();
                }
                if (this.guides[i].getValue() == "2X" || this.guides[i].getValue() == "3X") {
                    value = this.guides[i].getValue();
                }
                //s'il y a déjà un jeton il va mettre ce jeton dans son emplacement de base et mets le nouveau dans le guide
                if ((name == "" && value == "") || (name != "" && value != "")) {
                    shape.setX(cell.x);
                    shape.setY(cell.y);
                    if (this.guides[i].getShape() != null) {
                        if (this.guides[i].getShape() !== shape) {
                            this.guides[i].getShape().setX(this.guides[i].getShape().getBaseX());
                            this.guides[i].getShape().setY(this.guides[i].getShape().getBaseY());
                        }
                    }
                    this.guides[i].setShape(shape);
                } else {
                    shape.setX(shape.getBaseX());
                    shape.setY(shape.getBaseY());
                }
            }
        }
    }

    endGame(numAttemps) {
        if (this.onEndGame) {
            this.onEndGame(numAttemps);
        }
    }
}
class Guide6Tokens extends Asset {

    constructor(refGame, guidesFromDB, onEndGame) {
        super();
        this.refGame = refGame;
        this.onEndGame = onEndGame;
        this.container = new PIXI.Container();
        this.container.width = 200;
        this.container.height = 600;

        //autres images
        this.dogBrown = new Shape(50, 395, 50, 50, this.refGame.global.resources.getImage('dog_brown'), '');
        this.dogHouse = new Shape(550, 395, 50, 50, this.refGame.global.resources.getImage('dog_house'), '');

        //ajout autres images
        this.init(this.dogBrown, this.dogHouse);

        //startBtn
        let startBtn = new PIXI.Graphics();
        startBtn.beginFill(0x4bacc6, 1);
        startBtn.lineStyle(2, 0x357d91, 1);
        startBtn.moveTo(35, 380)
        startBtn.lineTo(65, 395)
        startBtn.lineTo(35, 410)
        startBtn.lineTo(35, 380);
        startBtn.endFill();
        startBtn.interactive = true;
        startBtn.on('pointerdown', this.onClick.bind(this));

        //ajout startBtn
        this.container.addChild(startBtn);

        this.guides = [];
        this.tokens = [];

        //XY des guides
        this.XYGuides = [[145, 395, ""],[225, 395, ""],[260, 470, ""],[340, 470, ""],[370, 395, ""],[450, 395, ""]];
        
        //XY des lignes entre les guides
        this.XYArrowUnderGuides = [[100, 395, 225, 395, false], [225, 395, 260, 470, false], [260, 470, 340, 470, false], [340, 470, 370, 395, false],[370, 395, 450, 395, false], [450, 395, 550, 395, true]];

        //Création des guides
        this.XYGuides.forEach(xy => {
            let guide = new Guide(xy[0], xy[1], 35, xy[2]);
            this.guides.push(guide)
        });

        //ajout de la valeur des guides
        for (let i = 1; i < guidesFromDB["guides"].length; i++) {
            for (let j = 0; j < this.guides.length; j++) {
                if (this.guides[j].getX() == guidesFromDB["guides"][i]["x"] && this.guides[j].getY() == guidesFromDB["guides"][i]["y"]) {
                    this.guides[j].setValue(guidesFromDB["guides"][i]["value"]);
                }
            }
        }

        //ajoout des lignes entre les guides
        this.XYArrowUnderGuides.forEach(xy => {
            this.init(new Arrow(xy[0], xy[1], xy[2], xy[3], 0x000000, xy[4]));
        });

        //ajout des guides
        this.guides.forEach(guide => {
            this.init(guide)
        });
        this.numAttemps = 0;

        //jetons
        let tokensIMG = {
            "blue": this.refGame.global.resources.getImage('dog_brown_sit'),
            "red": this.refGame.global.resources.getImage('dog_brown_jump'),
            "green": this.refGame.global.resources.getImage('dog_brown_bend')
        };

        //ajout des jetons
        guidesFromDB["tokens"].forEach(token => {
            let shape = new Shape(0, 0, 35, 35, tokensIMG[token], token, true, this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));
            this.tokens.push(shape)
            this.init(shape);
        });

        //set des jetons des guides à null
        for (let i = 0; i < this.guides.length; i++) {
            this.guides[i].setShape(null);
        }

        //Position des jetons aléatoire
        let xTokens = [75, 165, 255, 345, 435, 525];
        for (let i = 0; i < this.tokens.length; i++) {
            let random = Math.floor(Math.random() * xTokens.length);
            this.tokens[i].setBaseX(xTokens[random]);
            this.tokens[i].setBaseY(555);
            xTokens.splice(random, 1);
        }
    }

    init(...assets) {
        assets.forEach(asset => {
            asset.getPixiChildren().forEach(child => {
                this.container.addChild(child);
            });
        });
    }

    onClick() {
        let juste = 0;
        let allFull = true;
        //regarde le jetons dans chaque guide pour voir s'il est dans le bon guide
        for (let i = 0; i < this.guides.length; i++) {
            let cell = this.guides[i];
            if (cell.getShape() != null) {
                if (cell.getValue() === cell.getShape().getValeur()) {
                    juste++;
                }
            } else {
                allFull = false;
            }
        }
        //si c'est tout juste fini le jeu
        if (juste === this.guides.length) {
            this.numAttemps++;
            this.endGame(this.numAttemps);
            this.numAttemps = 0;
        } else if (allFull) {
            this.numAttemps++;
            this.refGame.global.util.showAlert(
                //Type d'alerte
                'error',
                //Texte
                this.refGame.global.resources.getOtherText('blocked'),
                //Titre
                this.refGame.global.resources.getOtherText('error'),
                //Footer
                undefined,
                //Callback
                undefined
            );
        } else {
            this.refGame.global.util.showAlert(
                //Type d'alerte
                'error',
                //Texte
                this.refGame.global.resources.getOtherText('notFull'),
                //Titre
                this.refGame.global.resources.getOtherText('error'),
                //Footer
                undefined,
                //Callback
                undefined
            );
        }
    }

    getPixiChildren() {
        return [this.container];
    }

    getGuides() {
        return this.guides;
    }

    //callback du listener
    onShapeMove(shape) {
        //aimant pour les guides 
        //si le jeton est près d'un guide il est mis dans le guide
        for (let i = 0; i < this.guides.length; i++) {
            let cell = this.guides[i];
            let minX = cell.x - cell.width / 1.7;
            let maxX = cell.x + cell.width / 1.7;
            let minY = cell.y - cell.width / 1.7;
            let maxY = cell.y + cell.width / 1.7;

            //enlève le jeton s'il est déplacé en dehors du guide
            if (cell.getShape() === shape && !(minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY())) {
                this.guides[i].setShape(null);
            }

            //ajout du jeton dans les guides
            if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY() && (this.guides[i].getShape() === shape || !this.guides[i].getShape())) {
                shape.setX(cell.x);
                shape.setY(cell.y);
                this.guides[i].setShape(shape);
            }
        }
    }

    //callback du listener
    onShapeEndMove(shape) {
        //aimant pour les guides 
        //si le jeton est près d'un guide il est mis dans le guide
        for (let i = 0; i < this.guides.length; i++) {
            let cell = this.guides[i];
            let minX = cell.x - cell.width / 1.7;
            let maxX = cell.x + cell.width / 1.7;
            let minY = cell.y - cell.width / 1.7;
            let maxY = cell.y + cell.width / 1.7;
            //s'il y a déjà un jeton il va mettre ce jeton dans son emplacement de base et mets le nouveau dans le guide
            if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY()) {
                shape.setX(cell.x);
                shape.setY(cell.y);
                if (this.guides[i].getShape() != null) {
                    if (this.guides[i].getShape() !== shape) {
                        this.guides[i].getShape().setX(this.guides[i].getShape().getBaseX());
                        this.guides[i].getShape().setY(this.guides[i].getShape().getBaseY());
                    }
                }
                this.guides[i].setShape(shape);
            }
        }
    }

    endGame(numAttemps) {
        if (this.onEndGame) {
            this.onEndGame(numAttemps);
        }
    }
}
class GuideTokenBasic extends Asset {

    constructor(refGame, guide) {
        super();
        this.refGame = refGame;
        this.container = new PIXI.Container();
        this.container.width = 200;
        this.container.height = 600;

        if (guide == "4Tokens") {

            //other images
            this.dogBrown = new Shape(50, 400, 50, 50, this.refGame.global.resources.getImage('dog_brown'), '');
            this.dogHouse = new Shape(550, 400, 50, 50, this.refGame.global.resources.getImage('dog_house'), '');

            //add others images
            this.init(this.dogBrown, this.dogHouse);

            //startBtn
            let startBtn = new PIXI.Graphics();
            startBtn.beginFill(0x4bacc6, 1);
            startBtn.lineStyle(2, 0x357d91, 1);
            startBtn.moveTo(35, 385)
            startBtn.lineTo(65, 400)
            startBtn.lineTo(35, 415)
            startBtn.lineTo(35, 385);
            startBtn.endFill();

            //add startBtn
            this.container.addChild(startBtn);

            this.guides = [];
            this.tokens = [];

            //XY des Guides
            this.XYGuides = [[165, 400, ""], [255, 400, ""], [345, 400, ""], [435, 400, ""]];

            //XY des lignes entre guides
            this.XYArrowUnderGuides = [[100, 400, 550, 400, true]];

            //Ajout des guides
            this.XYGuides.forEach(xy => {
                let guide = new Guide(xy[0], xy[1], 40, xy[2]);
                this.guides.push(guide)
            });

            //add arrow under guides
            this.XYArrowUnderGuides.forEach(xy => {
                this.init(new Arrow(xy[0], xy[1], xy[2], xy[3], 0x000000, xy[4]));
            });

            //add guides
            this.guides.forEach(guide => {
                this.init(guide)
            });
            this.numAttemps = 0;

            //set des jetons des guides à null
            for (let i = 0; i < this.guides.length; i++) {
                this.guides[i].setShape(null);
            }

        } else if (guide == "5Tokens") {

            //autres images
            this.dogBrown = new Shape(50, 405, 50, 50, this.refGame.global.resources.getImage('dog_brown'), '');
            this.dogHouse = new Shape(550, 440, 50, 50, this.refGame.global.resources.getImage('dog_house'), '');

            //ajout des images
            this.init(this.dogBrown, this.dogHouse);

            //startBtn
            let startBtn = new PIXI.Graphics();
            startBtn.beginFill(0x4bacc6, 1);
            startBtn.lineStyle(2, 0x357d91, 1);
            startBtn.moveTo(35, 390)
            startBtn.lineTo(65, 405)
            startBtn.lineTo(35, 420)
            startBtn.lineTo(35, 390);
            startBtn.endFill();

            //ajout startBtn
            this.container.addChild(startBtn);

            this.guides = [];
            this.tokens = [];

            //XY des Guides
            this.XYGuides = [[155, 405, "2X"], [250, 405, ""], [250, 480, ""], [345, 450, ""], [440, 450, "3X"]];

            //XY des petites images erreur et check des conditions
            this.XYImages = [[135, 450, 15, "error_sign"], [195, 380, 20, "check_sign"], [420, 405, 15, "error_sign"], [485, 430, 20, "check_sign"]],

                //XY des lignes entre les guides
                this.XYArrowUnderGuides = [
                    [100, 405, 155, 405, false, 6, null],
                    [155, 405, 255, 405, true, 6, null],
                    [155, 405, 155, 480, false, 6, null],
                    [152, 480, 255, 480, true, 6, null],
                    [255, 405, 345, 440, false, 6, null],
                    [345, 450, 445, 450, false, 6, null],
                    [250, 480, 345, 450, false, 6, null],
                    [345, 450, 550, 450, true, 6, null],
                    [440, 450, 440, 354.5, false, 6, [12.5, 9]],
                    [107, 357, 442, 357, false, 6, [12.5, 9]],
                    [110, 357, 110, 440, true, 6, [12.5, 9]]
                ];

            //création des guides
            this.XYGuides.forEach(xy => {
                let guide = new Guide(xy[0], xy[1], 35, xy[2]);
                this.guides.push(guide)
            });

            //ajout des lignes entre les guides
            this.XYArrowUnderGuides.forEach(xy => {
                this.init(new Arrow(xy[0], xy[1], xy[2], xy[3], 0x000000, xy[4], null, xy[5], xy[6]));
            });

            //ajout des guides
            this.guides.forEach(guide => {
                this.init(guide)
            });
            this.numAttemps = 0;

            //ajout des images d'erreurs et check
            this.XYImages.forEach(image => {
                let shape = new Shape(image[0], image[1], image[2], image[2], this.refGame.global.resources.getImage(image[3]), "");
                this.init(shape);
            });

            //Set les jetons dans les guides à null (aucun jeton dans les guides)
            for (let i = 0; i < this.guides.length; i++) {
                this.guides[i].setShape(null);
            }

        } else if (guide == "6Tokens") {

            //autres images
            this.dogBrown = new Shape(50, 395, 50, 50, this.refGame.global.resources.getImage('dog_brown'), '');
            this.dogHouse = new Shape(550, 395, 50, 50, this.refGame.global.resources.getImage('dog_house'), '');

            //ajout autres images
            this.init(this.dogBrown, this.dogHouse);

            //startBtn
            let startBtn = new PIXI.Graphics();
            startBtn.beginFill(0x4bacc6, 1);
            startBtn.lineStyle(2, 0x357d91, 1);
            startBtn.moveTo(35, 380)
            startBtn.lineTo(65, 395)
            startBtn.lineTo(35, 410)
            startBtn.lineTo(35, 380);
            startBtn.endFill();

            //ajout startBtn
            this.container.addChild(startBtn);

            this.guides = [];
            this.tokens = [];

            //XY des guides
            this.XYGuides = [[145, 395, ""], [225, 395, ""], [260, 470, ""], [340, 470, ""], [370, 395, ""], [450, 395, ""]];

            //XY des lignes entre les guides
            this.XYArrowUnderGuides = [[100, 395, 225, 395, false], [225, 395, 260, 470, false], [260, 470, 340, 470, false], [340, 470, 370, 395, false], [370, 395, 450, 395, false], [450, 395, 550, 395, true]];

            //Création des guides
            this.XYGuides.forEach(xy => {
                let guide = new Guide(xy[0], xy[1], 35, xy[2]);
                this.guides.push(guide)
            });

            //ajoout des lignes entre les guides
            this.XYArrowUnderGuides.forEach(xy => {
                this.init(new Arrow(xy[0], xy[1], xy[2], xy[3], 0x000000, xy[4]));
            });

            //ajout des guides
            this.guides.forEach(guide => {
                this.init(guide)
            });
            this.numAttemps = 0;

            //set des jetons des guides à null
            for (let i = 0; i < this.guides.length; i++) {
                this.guides[i].setShape(null);
            }
        }
    }

    init(...assets) {
        assets.forEach(asset => {
            asset.getPixiChildren().forEach(child => {
                this.container.addChild(child);
            });
        });
    }

    getPixiChildren() {
        return [this.container];
    }

    getGuides() {
        return this.guides;
    }

    //callback du listener
    onShapeMove(shape) {
        //aimant pour les guides 
        //si le jeton est près d'un guide il est mis dans le guide
        for (let i = 0; i < this.guides.length; i++) {
            let cell = this.guides[i];
            let minX = cell.x - cell.width / 1.7;
            let maxX = cell.x + cell.width / 1.7;
            let minY = cell.y - cell.width / 1.7;
            let maxY = cell.y + cell.width / 1.7;

            //enlève le jeton s'il est déplacé en dehors du guide
            if (cell.getShape() === shape && !(minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY())) {
                this.guides[i].setShape(null);
            }

            //ajout du jeton dans les guides
            if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY() && (this.guides[i].getShape() === shape || !this.guides[i].getShape())) {
                shape.setX(cell.x);
                shape.setY(cell.y);
                this.guides[i].setShape(shape);
            }
        }
    }

    //callback du listener
    onShapeEndMove(shape) {
        //aimant pour les guides 
        //si le jeton est près d'un guide il est mis dans le guide
        for (let i = 0; i < this.guides.length; i++) {
            let cell = this.guides[i];
            let minX = cell.x - cell.width / 1.7;
            let maxX = cell.x + cell.width / 1.7;
            let minY = cell.y - cell.width / 1.7;
            let maxY = cell.y + cell.width / 1.7;
            //s'il y a déjà un jeton il va mettre ce jeton dans son emplacement de base et mets le nouveau dans le guide
            if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY()) {
                shape.setX(cell.x);
                shape.setY(cell.y);
                if (this.guides[i].getShape() != null) {
                    if (this.guides[i].getShape() !== shape) {
                        this.guides[i].getShape().setX(this.guides[i].getShape().getBaseX());
                        this.guides[i].getShape().setY(this.guides[i].getShape().getBaseY());
                    }
                }
                this.guides[i].setShape(shape);
            }
        }
    }

    endGame(numAttemps) {
        if (this.onEndGame) {
            this.onEndGame(numAttemps);
        }
    }
}
class Ocean extends Asset {

    constructor(refGame, level) {
        super();
        this.refGame = refGame;
        this.level = level;
        this.background = new PIXI.Container();
        this.background.width = 600;
        this.background.height = 600;
        let img_background = refGame.global.resources.getImage("ocean");
        let sprite_background = PIXI.Sprite.fromImage(img_background.image.src);
        sprite_background.anchor.x = 0.5;
        sprite_background.y = 0;
        sprite_background.x = 300;
        let largeur = 350 / img_background.getHeight() * img_background.getWidth(); 
        sprite_background.height = 350;
        sprite_background.width = largeur;
        this.background.addChild(sprite_background);

        //colors
        this.blue = 0x4f81bd;
        this.green = 0x9bbb59;
        this.red = 0xc0504d;
        this.black = 0x000000;

        //XY des placements
        this.XYPlacements = [[225, 65], [75, 180], [225, 180], [375, 180], [525, 180], [375, 300]];

        //lignes entre les placements
        this.arrow_first_second_blue = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[1][0], this.XYPlacements[1][1], this.blue);
        this.arrow_first_fifth_green = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[4][0], this.XYPlacements[4][1], this.green);
        this.arrow_first_third_red = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[2][0], this.XYPlacements[2][1], this.red);
        this.arrow_second_third_green = new Arrow(this.XYPlacements[1][0], this.XYPlacements[1][1], this.XYPlacements[2][0], this.XYPlacements[2][1], this.green);
        this.arrow_second_sixth_red = new Arrow(this.XYPlacements[1][0], this.XYPlacements[1][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.red);
        this.arrow_third_fourth_blue = new Arrow(this.XYPlacements[2][0], this.XYPlacements[2][1], this.XYPlacements[3][0], this.XYPlacements[3][1], this.blue);
        this.arrow_fourth_sixth_green = new Arrow(this.XYPlacements[3][0], this.XYPlacements[3][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.green);
        this.arrow_fourth_fifth_red = new Arrow(this.XYPlacements[3][0], this.XYPlacements[3][1], this.XYPlacements[4][0], this.XYPlacements[4][1], this.red);
        this.arrow_fifth_sixth_blue = new Arrow(this.XYPlacements[4][0], this.XYPlacements[4][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.blue);
      
        //ajout lignes entres placements
        this.init(this.arrow_first_second_blue, this.arrow_first_fifth_green, this.arrow_first_third_red, this.arrow_second_third_green, this.arrow_second_sixth_red, this.arrow_third_fourth_blue, this.arrow_fourth_sixth_green, this.arrow_fourth_fifth_red, this.arrow_fifth_sixth_blue);

        //ajout placements        
        let i = 0;
        this.XYPlacements.forEach(XY => {
            let img = [];
            level["placements"].forEach(plac => {
                if(plac["num"] == i && plac["image"] != null && plac["image"].length > 0){
                    img = plac["image"];
                }
            });
            this.init(new Placement(XY[0], XY[1], i, this.refGame, img));
            i++;
        });
    }

    init(...assets) {
        assets.forEach(asset => {
            asset.getPixiChildren().forEach(child => {
                this.background.addChild(child);
            });
        });
    }

    getPixiChildren() {
        return [this.background];
    }
}
class Placement extends Asset {
    constructor(x, y, number, refGame, images = null, width = null) {
        super();
        this.x = x;
        this.y = y;
        this.number = number;
        this.images = images;
        this.refGame = refGame;
        this.width = width;
        this.placement = new PIXI.Graphics();
        this.container = new PIXI.Container();
        this.yPoints = [60,10];
        this.display();
    }

    display() {
        //Supprimer le contenu et setup de notre objet graphique
        this.container.removeChildren();
        this.container.x = 0;
        this.container.y = 0;
        this.container.width = 600;
        this.container.height = 600;
        
        //Ajouter notre forme sur l'élément graphique
        let text = new PIXI.Text(this.number,{fontFamily: "\"Comic Sans MS\", cursive, sans-serif", fontVariant: "small-caps", fontWeight: 900, fontSize: 30, fill : 0x000000, align : 'center'});
        text.x = this.x;
        text.y = this.y;
        text.anchor.x = 0.5;
        text.anchor.y = 0.5;

        
        this.placement.beginFill(0xffffff)
            .lineStyle(2, 0xef994e, 1)
            .drawCircle(this.x, this.y, 40)
            .endFill()
        this.placement.interactive = true;
        this.placement.addChild(text);

        this.setImage(this.images);
        this.container.addChild(this.placement);
    }
    
    getPixiChildren() {
        return [this.container];
    }

    getX(){
        return this.x;
    }

    setX(x){
        this.x = x;
    }

    getY(){
        return this.y;
    }

    setY(y){
        this.y = y;
    }

    getImage(){
        return this.images;
    }

    setImage(images){
        let i = 0;
        this.images = images;
        if(this.images != null){
            this.images.forEach(image => {
                let ima = this.refGame.global.resources.getImage(image);
                this.shape = PIXI.Sprite.fromImage(ima.image.src);
                //Définir sa position et l'ancre sur l'image, ici au centre de l'image
                this.shape.x = this.x + 15;
                this.shape.y = this.y - this.yPoints[i];
                this.shape.width = 50;
                this.shape.height = 50;             
                if(image == "doggy_biscuit"){
                    this.shape.x = this.x;
                    this.shape.width = 60;
                    this.shape.height = 60;
                }
                if(this.width != null){
                    this.shape.width = this.width ;
                    this.shape.height = this.width ; 
                } 
                i++;
                if (image == "dog_brown" || image == "dog_house" || image == "doggy_biscuit") {
                    this.placement.addChildAt(this.shape, 1);
                } else {
                this.placement.addChild(this.shape);
                }
            });
        }
    }

    removeImage() {
        if (this.placement.children.length > 1) {
            this.placement.removeChildAt(1);
        }
    }

    getNumber(){
        return this.number;
    }

    setNumber(number){
        this.number = number;
    }

    getPixiChildren() {
        return [this.container];
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

}
class PlacementBtn extends Asset {
    constructor(x, y, number, refGame, images = null, width = null) {
        super();
        this.x = x;
        this.y = y;
        this.number = number;
        this.images = images;
        this.refGame = refGame;
        this.width = width;
        this.placement = new PIXI.Graphics();
        this.container = new PIXI.Container();
        this.yPoints = [60, 10];
        this.display();

        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bouton */
        this.onClick = function () {
            console.log('Replace this action with button.setOnClick')
        };
    }

    display() {
        //Supprimer le contenu et setup de notre objet graphique
        this.container.removeChildren();
        this.container.x = 0;
        this.container.y = 0;
        this.container.width = 600;
        this.container.height = 600;

        //Ajouter notre forme sur l'élément graphique
        let text = new PIXI.Text(this.number, { fontFamily: "\"Comic Sans MS\", cursive, sans-serif", fontVariant: "small-caps", fontWeight: 900, fontSize: 30, fill: 0x000000, align: 'center' });
        text.x = this.x;
        text.y = this.y;
        text.anchor.x = 0.5;
        text.anchor.y = 0.5;


        this.placement.beginFill(0xffffff)
            .lineStyle(2, 0xef994e, 1)
            .drawCircle(this.x, this.y, 40)
            .endFill()
        this.placement.interactive = true;
        //ajout de l'evenement click
        this.placement.buttonMode = true;
        this.placement.on('pointerdown', function () {
            this.onClick()
        }.bind(this));
        this.placement.addChild(text);

        this.setImage(this.images);
        this.container.addChild(this.placement);
    }



    /**
         * Défini la fonction de callback à appeler après un click sur le bouton
         * @param {function} onClick La fonction de callback
         */
    setOnClick(onClick) {
        this.onClick = onClick;
    }

    getPixiChildren() {
        return [this.container];
    }

    getX() {
        return this.x;
    }

    setX(x) {
        this.x = x;
    }

    getY() {
        return this.y;
    }

    setY(y) {
        this.y = y;
    }

    getImage() {
        return this.images;
    }

    setImage(images) {
        let i = 0;
        this.images = images;
        if (this.images != null) {
            this.images.forEach(image => {
                let ima = this.refGame.global.resources.getImage(image);
                this.shape = PIXI.Sprite.fromImage(ima.image.src);
                //Définir sa position et l'ancre sur l'image, ici au centre de l'image
                this.shape.x = this.x + 15;
                this.shape.y = this.y - this.yPoints[i];
                this.shape.width = 50;
                this.shape.height = 50;
                if (image == "doggy_biscuit") {
                    this.shape.x = this.x;
                    this.shape.width = 60;
                    this.shape.height = 60;
                }
                if (this.width != null) {
                    this.shape.width = this.width;
                    this.shape.height = this.width;
                }
                i++;

                if (image == "dog_brown" || image == "dog_house" || image == "doggy_biscuit") {
                    this.placement.addChildAt(this.shape, 1);
                }
            });
        }
    }

    removeImage() {
        if (this.placement.children.length > 1) {
            this.placement.removeChildAt(1);
        }
    }

    getNumber() {
        return this.number;
    }

    setNumber(number) {
        this.number = number;
    }

    getPixiChildren() {
        return [this.container];
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

}
class ScrollPane extends Asset {


    constructor(x, y, width, height) {
        super();
        this.x = x | 0;
        this.y = y | 0;
        this.width = width | 0;
        this.height = height | 0;
        this.index = 1;

        this.maxIndex = 0;

        this.touchScrollLimiter = 0;
        this.lastTouch = 0;
        this.data = {};
        this.dragging = false;

        this.elements = [];

        this.graphics = {
            elementContainer: new PIXI.Container(),
            scrollBar: new PIXI.Graphics(),
            scrollButton: new PIXI.Graphics()
        };

        this.graphics.scrollButton
            .on('mousedown', this.onDragStart.bind(this))
            .on('touchstart', this.onDragStart.bind(this))
            .on('mouseup', this.onDragEnd.bind(this))
            .on('mouseupoutside', this.onDragEnd.bind(this))
            .on('touchend', this.onDragEnd.bind(this))
            .on('touchendoutside', this.onDragEnd.bind(this))
            .on('mousemove', this.onDragMove.bind(this))
            .on('touchmove', this.onDragMove.bind(this));


        let canvas = document.getElementById('canvas');
        canvas.addEventListener('wheel', function (event) {
            this.index += (event.deltaY > 0 ? (this.height / this.elements.length) : -(this.height / this.elements.length));
            if (this.index > this.height - 31)
                this.index = (this.height - 31);
            else if (this.index <= 1)
                this.index = 1;
            this.scroll();
        }.bind(this));
        canvas.addEventListener('touchmove', function (event) {
            if (this.lastTouch === 0)
                this.lastTouch = event.touches[0].clientY;
            this.touchScrollLimiter += (this.lastTouch - event.touches[0].clientY);
            this.lastTouch = event.touches[0].clientY;
            if ((this.touchScrollLimiter > (this.maxIndex / this.elements.length))
                || (this.touchScrollLimiter < -(this.maxIndex / this.elements.length))) {
                console.log(this.touchScrollLimiter);
                this.index += this.touchScrollLimiter > 0 ? (this.height / this.elements.length) : -(this.height / this.elements.length);
                this.touchScrollLimiter = 0;
                if (this.index > this.height - 31)
                    this.index = (this.height - 31);
                else if (this.index <= 1)
                    this.index = 1;
                this.scroll();
            }
        }.bind(this));
        canvas.addEventListener('touchend', function (event) {
            this.lastTouch = 0;
        }.bind(this));
    }

    setPosition(x, y) {
        this.x = x;
        this.y = y;
        this.init();
    }

    addElements(...elements) {
        for (let element of elements)
            this.elements.push(element);
    }

    init() {

        console.log(this.elements);

        this.graphics.scrollBar.clear();
        this.graphics.scrollBar.beginFill(0xDDDDDD);
        this.graphics.scrollBar.drawRect(this.x + this.width - 18, this.y, 18, this.height);
        this.graphics.scrollBar.endFill();

        this.graphics.scrollButton.clear();
        this.graphics.scrollButton.beginFill(0x999999);
        this.graphics.scrollButton.drawRect(this.x + this.width - 17, this.y + 1, 16, 30);
        this.graphics.scrollButton.endFill();

        this.graphics.scrollButton.interactive = true;
        this.graphics.scrollButton.buttonMode = true;

        let y = this.y;
        for (let element of this.elements) {
            if (element instanceof Asset) {
                element.setY(y);
                element.setX((this.width-20) / 2);
                y += element.getHeight();
                element.setVisible(this.checkIsInContainer(element.getY(), element.getHeight() / 2));
                for (let pc of element.getPixiChildren())
                    this.graphics.elementContainer.addChild(pc);
            } else if (typeof element.y !== 'undefined' && typeof element.visible !== 'undefined') {
                element.y = y;
                y += (typeof element.height !== 'undefined' ? element.height : 25);
                element.visible = this.checkIsInContainer(element.y);
                this.graphics.elementContainer.addChild(element);
                element.x = this.width / 2;
            }
            y += 5;
        }
        this.maxIndex = y;

        this.graphics.scrollButton.visible = (this.height < this.maxIndex);
        this.graphics.scrollBar.visible = (this.height < this.maxIndex);

        this.scroll();
    }

    clear() {
        this.elements = [];
    }

    scroll() {
        if (this.elements.length === 0)
            return;

        this.graphics.scrollButton.position.y = this.index;

        let firstDisplayedIndex = Math.floor((this.elements.length - 1) * (this.index / this.height));

        let posY = 1 + this.y + this.elements[firstDisplayedIndex].getHeight()/2;

        for (let i = 0; i < this.elements.length; i++) {
            let element = this.elements[i];
            if (element instanceof Asset) {
                if (i < firstDisplayedIndex)
                    element.setVisible(false);
                else {
                    element.setY(posY);
                    element.setVisible(this.checkIsInContainer(element.getY()));
                    posY += element.getHeight() + 5;
                }
            } else if (typeof element.y !== 'undefined' && typeof element.visible !== 'undefined') {
                if (i < firstDisplayedIndex)
                    element.visible = false;
                else {
                    element.y = posY;
                    element.visible = this.checkIsInContainer(element.y);
                    posY += element.height + 5;
                }
            }
            if(element instanceof ToggleButton)
                element.updateView();
        }
    }


    checkIsInContainer(posY, compensation = 0) {
        return (posY < (this.y + this.height) && (posY > (this.y + compensation)));
    }

    getPixiChildren() {
        let elements = [];
        for (let e in this.graphics)
            if (this.graphics[e] instanceof Asset)
                for (let element of this.graphics[e].getPixiChildren())
                    elements.push(element);
            else
                elements.push(this.graphics[e]);
        return elements;
    }


    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {

    }

    onDragStart(event) {
        this.data = event.data;
        this.graphics.scrollButton.alpha = 0.5;
        this.dragging = true;
    }

    onDragEnd() {
        this.graphics.scrollButton.alpha = 1;
        this.dragging = false;
        this.data = null;
    }

    onDragMove() {
        if (this.dragging) {
            let newPosition = this.data.getLocalPosition(this.graphics.scrollButton.parent);
            this.index = newPosition.y - this.y;
            if (this.index > this.height - 31)
                this.index = (this.height - 31);
            else if (this.index <= 1)
                this.index = 1;
            this.scroll();
        }
    }
}
class Select extends Asset{


    constructor(height, width, x, y) {
        super();

        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;

        this.button = new Button(x,y,'SELECT',0xFFFFFF,0x000000, false,width);
        this.container = new PIXI.Container();

        this.currentIndex = 0;
        this.elements = [];

        this.button.setOnClick(function () {
            this.container.visible = !this.container.visible;
        }.bind(this));

        this.container.visible = true;
    }

    addElement(element){
        element.setSelect(this);
        this.elements.push(element);
        this.build();
    }


    onClick(data){
        console.log(data);
    }

    build(){
        this.container.removeChildren(0);
        this.container.width = this.width;
        this.container.height = this.height;
        this.container.x = this.x-this.width/2;
        this.container.y = this.y+30;


        let currentY = 0;
        for(let element of this.elements){
            element.build(this.width/2, currentY, this.width);
            currentY+=element.getHeight();
            for(let child of element.getPixiChildren()){
                this.container.addChild(child);
            }
        }


    }


    resize(height, width){
        this.height = height;
        this.width = width;
        this.build();
    }

    setPosition(x,y){
        this.x = x;
        this.y = y;
        this.build();
    }

    getPixiChildren() {
        let elements = [];
        for (let e of this.button.getPixiChildren()){
            elements.push(e);
        }
        elements.push(this.container);
        return elements;
    }



    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        this.button.setVisible(visible);
        this.container.visible = visible;
    }
}
class SelectItem extends Asset{

    constructor(text, data=null) {
        super();
        this.selectRef = null;
        this.button = null;
        this.text = text;
        this.data = data;
        this.build(0,0,100);
    }


    build(x,y,width){
        this.button = new Button(x,y,this.text,0xFFFFFF,0x000000, false,width);
        this.button.setOnClick(function () {
            this.selectRef.onClick(this.data);
        }.bind(this));
    }


    getPixiChildren() {
        return this.button ? this.button.getPixiChildren():[];
    }

    setText(text){
        this.text = text;
    }

    setSelect(ref){
        this.selectRef = ref;
    }

    getData(){
        return data;
    }

    setData(data){
        this.data = data;
    }

    getHeight(){
        return this.button != null ? this.button.getHeight() : 0;
    }


    getY() {
        this.button.getY();
    }

    getX() {
        this.button.getX();
    }

    setVisible(visible) {
        this.button.setVisible(visible);
    }
}
class Shape extends Asset {
    constructor(x, y, width, height, image, valeur, toMove = false, onMove = null, endMove = null) {
        super();
        this.x = x;
        this.y = y;
        this.baseX = x;
        this.baseY = y;
        this.width = width;
        this.height = height;
        this.image = image;
        this.valeur = valeur;
        this.toMove = toMove;
        this.onMove = onMove;
        this.endMove = endMove;
        this.stopMove = false;
        this.visible = true;

        this.container = new PIXI.Container();
        this.isFirstClick = true;
        this.data = undefined;
        this.shape = undefined;
        this.display();
    }

    display() {
        //Supprimer le contenu et setup de notre objet graphique
        this.container.removeChildren();
        this.container.x = 0;
        this.container.y = 0;
        this.container.width = 600;
        this.container.height = 600;

        this.shape = null;
        //Si la valeur est 2X ou 3X et que l'image est null du text sera mis dans la forme en blanc gras taille 30
        if(this.image == null && (this.valeur == "2X" || this.valeur == "3X")){
            this.shape = new PIXI.Text(this.valeur,{fontFamily: "\"Comic Sans MS\", cursive, sans-serif", fontVariant: "small-caps", fontWeight: 900, fontSize: 30, fill : 0xffffff, align : 'center'});
        }else{
            //Créer un "Sprite" avec une image en base64.
            //Le type d'image est "IMG" donc pour récupérer l'image, il faut faire
            //this.image.image.src
            this.shape = PIXI.Sprite.fromImage(this.image.image.src);  
        }
    
        //Définir sa position et l'ancre sur l'image, ici au centre de l'image
        this.shape.anchor.x = 0.5;
        this.shape.anchor.y = 0.5;
        this.shape.x = this.x;
        this.shape.y = this.y;
        this.shape.width = this.width + 10;
        this.shape.height = this.height + 10;

        //couleurs
        this.red = 0xc0504d;
        this.redContour = 0x8c3836;
        this.blue = 0x4f81bd;
        this.blueContour = 0x385e8b;
        this.green = 0x9bbb59;
        this.greenContour = 0x71893f;
        this.purple = 0x8064a2;
        this.purpleContour = 0x5b4675;
        
        //Ajouter notre forme sur l'élément graphique
        if(this.toMove){
            this.graphics = new PIXI.Graphics();
            switch(this.valeur) {
                case "red":
                    this.graphics.beginFill(this.red);
                    this.graphics.lineStyle(2, this.redContour, 2);
                    break;
                case "blue":
                    this.graphics.beginFill(this.blue);
                    this.graphics.lineStyle(2, this.blueContour, 2);
                    break;
                case "green":
                    this.graphics.beginFill(this.green);
                    this.graphics.lineStyle(2, this.greenContour, 2);
                    break;
                case "2X":
                    this.graphics.beginFill(this.purple);
                    this.graphics.lineStyle(2, this.purpleContour, 2);
                    break;
                case "3X":
                    this.graphics.beginFill(this.purple);
                    this.graphics.lineStyle(2, this.purpleContour, 2);
                    break;
                } 
            //Si c'est 2X ou 3X un pentagone sera dessiné au lieu du cercle.
            if(this.valeur == "3X" || this.valeur == "2X"){
                this.graphics.drawPolygon(this.x - 32, this.y - 7, this.x, this.y - 35, this.x + 32, this.y - 7, this.x + 20, this.y + 28, this.x - 20, this.y + 28);
                this.graphics.endFill();
            }else{
                this.graphics.drawCircle(this.x, this.y, this.width);
                this.graphics.endFill();
            }

            //Ajouter les listeners
            this.graphics.interactive = true;
            this.graphics.on('mousedown', this.onDragStart.bind(this))
                        .on('touchstart', this.onDragStart.bind(this))
                        .on('mouseup', this.onDragEnd.bind(this))
                        .on('mouseupoutside', this.onDragEnd.bind(this))
                        .on('touchend', this.onDragEnd.bind(this))
                        .on('touchendoutside', this.onDragEnd.bind(this))
                        .on('mousemove', this.onDragMove.bind(this))
                        .on('touchmove', this.onDragMove.bind(this));
            this.graphics.addChild(this.shape);
            this.container.addChild(this.graphics);
        }else{
            this.container.addChild(this.shape);
        }
    }

    onDragStart(event) {
        this.data = event.data;
    }


    onDragEnd() {
        this.data = null;
        if (this.endMove) {
            this.endMove(this);
        }
    }

    onDragMove() {
        if (this.data) {
            //Regarde si la forme peut bouger
            if(this.toMove == true){
                if(!this.stopMove){
                    //Position actuelle
                    let newPosition = this.data.getLocalPosition(this.shape.parent);

                    //Empêcher la forme de sortir du canvas et de devenir invisible
                    newPosition.x = newPosition.x < 45 ? 45 : newPosition.x;
                    newPosition.y = newPosition.y < 400 ? 400 : newPosition.y;
                    newPosition.x = newPosition.x > 555 ? 555 : newPosition.x;
                    newPosition.y = newPosition.y > 555 ? 555 : newPosition.y;

                    //Mise à jour la position de la forme
                    this.y = newPosition.y;
                    this.x = newPosition.x;
                    this.display();

                    //Appel au callback s'il y en a un
                    if (this.onMove) {
                        this.onMove(this);
                    }
                }
            }
        }
        
    }

    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    getBaseX(){
        return this.baseX;
    }

    getBaseY(){
        return this.baseY;
    }

    setBaseX(baseX){
        this.baseX = baseX;
        this.setX(baseX);
    }

    setBaseY(baseY){
        this.baseY = baseY;
        this.setY(baseY);
    }

    setY(y) {
        this.y = y;
        //Mise à jour de l'affichage
        this.display();
    }

    setX(x) {
        this.x = x;
        //Mise à jour de l'affichage
        this.display();
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

    getValeur(){
        return this.valeur;
    }

    setValeur(valeur){
        this.valeur = valeur;
    }

    getName(){
        return this.name;
    }

    setName(name){
        this.name = name;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

    setStopMove(stopMove){
        this.stopMove = stopMove;
    }
}
class ShapeCreate extends Asset {
    constructor(x, y, width, height, image, valeur, toMove = false, onMove = null, endMove = null) {
        super();
        this.x = x;
        this.y = y;
        this.baseX = x;
        this.baseY = y;
        this.width = width;
        this.height = height;
        this.image = image;
        this.valeur = valeur;
        this.toMove = toMove;
        this.onMove = onMove;
        this.endMove = endMove;
        this.stopMove = false;
        this.visible = true;

        this.container = new PIXI.Container();
        this.isFirstClick = true;
        this.data = undefined;
        this.shape = undefined;
        this.display();

        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bouton */
        this.onClick = function () {
            console.log('Replace this action with button.setOnClick')
        };
    }

    display() {
        //Supprimer le contenu et setup de notre objet graphique
        this.container.removeChildren();
        this.container.x = 0;
        this.container.y = 0;
        this.container.width = 600;
        this.container.height = 600;

        this.shape = null;
        //Si la valeur est 2X ou 3X et que l'image est null du text sera mis dans la forme en blanc gras taille 30
        if (this.image == null && (this.valeur == "2X" || this.valeur == "3X")) {
            this.shape = new PIXI.Text(this.valeur, { fontFamily: "\"Comic Sans MS\", cursive, sans-serif", fontVariant: "small-caps", fontWeight: 900, fontSize: 30, fill: 0xffffff, align: 'center' });
        } else {
            //Créer un "Sprite" avec une image en base64.
            //Le type d'image est "IMG" donc pour récupérer l'image, il faut faire
            //this.image.image.src
            this.shape = PIXI.Sprite.fromImage(this.image.image.src);
        }

        //Définir sa position et l'ancre sur l'image, ici au centre de l'image
        this.shape.anchor.x = 0.5;
        this.shape.anchor.y = 0.5;
        this.shape.x = this.x;
        this.shape.y = this.y;
        this.shape.width = this.width + 10;
        this.shape.height = this.height + 10;

        //couleurs
        this.red = 0xc0504d;
        this.redContour = 0x8c3836;
        this.blue = 0x4f81bd;
        this.blueContour = 0x385e8b;
        this.green = 0x9bbb59;
        this.greenContour = 0x71893f;
        this.purple = 0x8064a2;
        this.purpleContour = 0x5b4675;

        //Ajouter notre forme sur l'élément graphique
        if (this.toMove == false) {
            this.graphics = new PIXI.Graphics();
            switch (this.valeur) {
                case "red":
                    this.graphics.beginFill(this.red);
                    this.graphics.lineStyle(2, this.redContour, 2);
                    this.graphics.buttonMode = true;
                    this.graphics.on('pointerdown', function () {
                        this.onClick()
                    }.bind(this));
                    break;
                case "blue":
                    this.graphics.beginFill(this.blue);
                    this.graphics.lineStyle(2, this.blueContour, 2);
                    this.graphics.buttonMode = true;
                    this.graphics.on('pointerdown', function () {
                        this.onClick()
                    }.bind(this));
                    break;
                case "green":
                    this.graphics.beginFill(this.green);
                    this.graphics.lineStyle(2, this.greenContour, 2);
                    this.graphics.buttonMode = true;
                    this.graphics.on('pointerdown', function () {
                        this.onClick()
                    }.bind(this));
                    break;
                case "2X":
                    this.graphics.beginFill(this.purple);
                    this.graphics.lineStyle(2, this.purpleContour, 2);
                    this.graphics.buttonMode = true;
                    this.graphics.on('pointerdown', function () {
                        this.onClick()
                    }.bind(this));
                    break;
                case "3X":
                    this.graphics.beginFill(this.purple);
                    this.graphics.lineStyle(2, this.purpleContour, 2)
                        ; this.graphics.buttonMode = true;
                    this.graphics.on('pointerdown', function () {
                        this.onClick()
                    }.bind(this));
                    break;
            }
            //Si c'est 2X ou 3X un pentagone sera dessiné au lieu du cercle.
            if (this.valeur == "3X" || this.valeur == "2X") {
                this.graphics.drawPolygon(this.x - 32, this.y - 7, this.x, this.y - 35, this.x + 32, this.y - 7, this.x + 20, this.y + 28, this.x - 20, this.y + 28);
                this.graphics.endFill();
            } else {
                this.graphics.drawCircle(this.x, this.y, this.width);
                this.graphics.endFill();
            }

            //Ajouter les listeners
            this.graphics.interactive = true;
            this.graphics.on('mousedown', this.onDragStart.bind(this))
                .on('touchstart', this.onDragStart.bind(this))
                .on('mouseup', this.onDragEnd.bind(this))
                .on('mouseupoutside', this.onDragEnd.bind(this))
                .on('touchend', this.onDragEnd.bind(this))
                .on('touchendoutside', this.onDragEnd.bind(this))
                .on('mousemove', this.onDragMove.bind(this))
                .on('touchmove', this.onDragMove.bind(this));
            this.graphics.addChild(this.shape);
            this.container.addChild(this.graphics);
        } else {
            this.container.addChild(this.shape);
        }
    }

    onDragStart(event) {
        this.data = event.data;
    }


    onDragEnd() {
        this.data = null;
        if (this.endMove) {
            this.endMove(this);
        }
    }

    onDragMove() {
        if (this.data) {
            //Regarde si la forme peut bouger
            if (this.toMove == true) {
                if (!this.stopMove) {
                    //Position actuelle
                    let newPosition = this.data.getLocalPosition(this.shape.parent);

                    //Empêcher la forme de sortir du canvas et de devenir invisible
                    newPosition.x = newPosition.x < 45 ? 45 : newPosition.x;
                    newPosition.y = newPosition.y < 400 ? 400 : newPosition.y;
                    newPosition.x = newPosition.x > 555 ? 555 : newPosition.x;
                    newPosition.y = newPosition.y > 555 ? 555 : newPosition.y;

                    //Mise à jour la position de la forme
                    this.y = newPosition.y;
                    this.x = newPosition.x;
                    this.display();

                    //Appel au callback s'il y en a un
                    if (this.onMove) {
                        this.onMove(this);
                    }
                }
            }
        }

    }

    /**
         * Défini la fonction de callback à appeler après un click sur le bouton
         * @param {function} onClick La fonction de callback
         */
    setOnClick(onClick) {
        this.onClick = onClick;
    }

    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    getBaseX() {
        return this.baseX;
    }

    getBaseY() {
        return this.baseY;
    }

    setBaseX(baseX) {
        this.baseX = baseX;
        this.setX(baseX);
    }

    setBaseY(baseY) {
        this.baseY = baseY;
        this.setY(baseY);
    }

    setY(y) {
        this.y = y;
        //Mise à jour de l'affichage
        this.display();
    }

    setX(x) {
        this.x = x;
        //Mise à jour de l'affichage
        this.display();
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

    getValeur() {
        return this.valeur;
    }

    setValeur(valeur) {
        this.valeur = valeur;
    }

    getName() {
        return this.name;
    }

    setName(name) {
        this.name = name;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

    setStopMove(stopMove) {
        this.stopMove = stopMove;
    }
}
(function (pkg){

    const PIXI = pkg.PIXI

    class TextInput extends PIXI.Container{
        constructor(styles){
            super()
            this._input_style = Object.assign(
                {
                    position: 'absolute',
                    background: 'none',
                    border: 'none',
                    outline: 'none',
                    transformOrigin: '0 0',
                    lineHeight: '1'
                },
                styles.input
            )

            if(styles.box)
                this._box_generator = typeof styles.box === 'function' ? styles.box : new DefaultBoxGenerator(styles.box)
            else
                this._box_generator = null

            if(this._input_style.hasOwnProperty('multiline')){
                this._multiline = !!this._input_style.multiline
                delete this._input_style.multiline
            }else
                this._multiline = false

            this._box_cache = {}
            this._previous = {}
            this._dom_added = false
            this._dom_visible = true
            this._placeholder = ''
            this._placeholderColor = 0xa9a9a9
            this._selection = [0,0]
            this._restrict_value = ''
            this._createDOMInput()
            this.substituteText = true
            this._setState('DEFAULT')
            this._addListeners()
        }



        // GETTERS & SETTERS

        get substituteText(){
            return this._substituted
        }

        set substituteText(substitute){
            if(this._substituted==substitute)
                return

            this._substituted = substitute

            if(substitute){
                this._createSurrogate()
                this._dom_visible = false
            }else{
                this._destroySurrogate()
                this._dom_visible = true
            }
            this.placeholder = this._placeholder
            this._update()
        }

        get placeholder(){
            return this._placeholder
        }

        set placeholder(text){
            this._placeholder = text
            if(this._substituted){
                this._updateSurrogate()
                this._dom_input.placeholder = ''
            }else{
                this._dom_input.placeholder = text
            }
        }

        get disabled(){
            return this._disabled
        }

        set disabled(disabled){
            this._disabled = disabled
            this._dom_input.disabled = disabled
            this._setState(disabled ? 'DISABLED' : 'DEFAULT')
        }

        get maxLength(){
            return this._max_length
        }

        set maxLength(length){
            this._max_length = length
            this._dom_input.setAttribute('maxlength', length)
        }

        get restrict(){
            return this._restrict_regex
        }

        set restrict(regex){
            if(regex instanceof RegExp){
                regex = regex.toString().slice(1,-1)

                if(regex.charAt(0) !== '^')
                    regex = '^'+regex

                if(regex.charAt(regex.length-1) !== '$')
                    regex = regex+'$'

                regex = new RegExp(regex)
            }else{
                regex = new RegExp('^['+regex+']*$')
            }

            this._restrict_regex = regex
        }

        get text(){
            return this._dom_input.value
        }

        set text(text){
            this._dom_input.value = text
            if(this._substituted) this._updateSurrogate()
        }

        get htmlInput(){
            return this._dom_input
        }

        focus(){
            if(this._substituted && !this.dom_visible)
                this._setDOMInputVisible(true)

            this._dom_input.focus()

        }

        blur(){
            this._dom_input.blur()
        }

        select(){
            this.focus()
            this._dom_input.select()
        }

        setInputStyle(key,value){
            this._input_style[key] = value
            this._dom_input.style[key] = value

            if(this._substituted && (key==='fontFamily' || key==='fontSize'))
                this._updateFontMetrics()

            if(this._last_renderer)
                this._update()
        }

        destroy(options){
            this._destroyBoxCache()
            super.destroy(options)
        }


        // SETUP

        _createDOMInput(){
            if(this._multiline){
                this._dom_input = document.createElement('textarea')
                this._dom_input.style.resize = 'none'
            }else{
                this._dom_input = document.createElement('input')
                this._dom_input.type = 'text'
            }

            for(let key in this._input_style){
                this._dom_input.style[key] = this._input_style[key]
            }
        }

        _addListeners(){
            this.on('added',this._onAdded.bind(this))
            this.on('removed',this._onRemoved.bind(this))
            this._dom_input.addEventListener('keydown', this._onInputKeyDown.bind(this))
            this._dom_input.addEventListener('input', this._onInputInput.bind(this))
            this._dom_input.addEventListener('keyup', this._onInputKeyUp.bind(this))
            this._dom_input.addEventListener('focus', this._onFocused.bind(this))
            this._dom_input.addEventListener('blur', this._onBlurred.bind(this))
        }

        _onInputKeyDown(e){
            this._selection = [
                this._dom_input.selectionStart,
                this._dom_input.selectionEnd
            ]

            this.emit('keydown',e.keyCode)
        }

        _onInputInput(e){
            if(this._restrict_regex)
                this._applyRestriction()

            if(this._substituted)
                this._updateSubstitution()

            this.emit('input',this.text)
        }

        _onInputKeyUp(e){
            this.emit('keyup',e.keyCode)
        }

        _onFocused(){
            this._setState('FOCUSED')
            this.emit('focus')
        }

        _onBlurred(){
            this._setState('DEFAULT')
            this.emit('blur')
        }

        _onAdded(){
            document.body.appendChild(this._dom_input)
            this._dom_input.style.display = 'none'
            this._dom_added = true
        }

        _onRemoved(){
            document.body.removeChild(this._dom_input)
            this._dom_added = false
        }

        _setState(state){
            this.state = state
            this._updateBox()
            if(this._substituted)
                this._updateSubstitution()
        }



        // RENDER & UPDATE

        // for pixi v4
        renderWebGL(renderer){
            super.renderWebGL(renderer)
            this._renderInternal(renderer)
        }

        // for pixi v4
        renderCanvas(renderer){
            super.renderCanvas(renderer)
            this._renderInternal(renderer)
        }

        // for pixi v5
        render(renderer){
            super.render(renderer)
            this._renderInternal(renderer)
        }

        _renderInternal(renderer){
            this._resolution = renderer.resolution
            this._last_renderer = renderer
            this._canvas_bounds = this._getCanvasBounds()
            if(this._needsUpdate())
                this._update()
        }

        _update(){
            this._updateDOMInput()
            if(this._substituted) this._updateSurrogate()
            this._updateBox()
        }

        _updateBox(){
            if(!this._box_generator)
                return

            if(this._needsNewBoxCache())
                this._buildBoxCache()

            if(this.state==this._previous.state
                && this._box==this._box_cache[this.state])
                return

            if(this._box)
                this.removeChild(this._box)

            this._box = this._box_cache[this.state];
            this.addChildAt(this._box,0)
            this._previous.state = this.state
        }

        _updateSubstitution(){
            if(this.state==='FOCUSED'){
                this._dom_visible = true
                this._surrogate.visible = this.text.length===0
            }else{
                this._dom_visible = false
                this._surrogate.visible = true
            }
            this._updateDOMInput()
            this._updateSurrogate()
        }

        _updateDOMInput(){
            if(!this._canvas_bounds)
                return

            this._dom_input.style.top = this._canvas_bounds.top+'px'
            this._dom_input.style.left = this._canvas_bounds.left+'px'
            this._dom_input.style.transform = this._pixiMatrixToCSS(this._getDOMRelativeWorldTransform())
            this._dom_input.style.opacity = this.worldAlpha
            this._setDOMInputVisible(this.worldVisible && this._dom_visible)

            this._previous.canvas_bounds = this._canvas_bounds
            this._previous.world_transform = this.worldTransform.clone()
            this._previous.world_alpha = this.worldAlpha
            this._previous.world_visible = this.worldVisible
        }

        _applyRestriction(){
            if(this._restrict_regex.test(this.text)){
                this._restrict_value = this.text
            }else{
                this.text = this._restrict_value
                this._dom_input.setSelectionRange(
                    this._selection[0],
                    this._selection[1]
                )
            }
        }


        // STATE COMPAIRSON (FOR PERFORMANCE BENEFITS)

        _needsUpdate(){
            return (
                !this._comparePixiMatrices(this.worldTransform,this._previous.world_transform)
                || !this._compareClientRects(this._canvas_bounds,this._previous.canvas_bounds)
                || this.worldAlpha != this._previous.world_alpha
                || this.worldVisible != this._previous.world_visible
            )
        }

        _needsNewBoxCache(){
            let input_bounds = this._getDOMInputBounds()
            return (
                !this._previous.input_bounds
                || input_bounds.width != this._previous.input_bounds.width
                || input_bounds.height != this._previous.input_bounds.height
            )
        }


        // INPUT SUBSTITUTION

        _createSurrogate(){
            this._surrogate_hitbox = new PIXI.Graphics()
            this._surrogate_hitbox.alpha = 0
            this._surrogate_hitbox.interactive = true
            this._surrogate_hitbox.cursor = 'text'
            this._surrogate_hitbox.on('pointerdown',this._onSurrogateFocus.bind(this))
            this.addChild(this._surrogate_hitbox)

            this._surrogate_mask = new PIXI.Graphics()
            this.addChild(this._surrogate_mask)

            this._surrogate = new PIXI.Text('',{})
            this.addChild(this._surrogate)

            this._surrogate.mask = this._surrogate_mask

            this._updateFontMetrics()
            this._updateSurrogate()
        }

        _updateSurrogate(){
            let padding = this._deriveSurrogatePadding()
            let input_bounds = this._getDOMInputBounds()

            this._surrogate.style = this._deriveSurrogateStyle()
            this._surrogate.style.padding = Math.max.apply(Math,padding)
            this._surrogate.y = this._multiline ? padding[0] : (input_bounds.height-this._surrogate.height)/2
            this._surrogate.x = padding[3]
            this._surrogate.text = this._deriveSurrogateText()

            this._updateSurrogateHitbox(input_bounds)
            this._updateSurrogateMask(input_bounds,padding)
        }

        _updateSurrogateHitbox(bounds){
            this._surrogate_hitbox.clear()
            this._surrogate_hitbox.beginFill(0)
            this._surrogate_hitbox.drawRect(0,0,bounds.width,bounds.height)
            this._surrogate_hitbox.endFill()
            this._surrogate_hitbox.interactive = !this._disabled
        }

        _updateSurrogateMask(bounds,padding){
            this._surrogate_mask.clear()
            this._surrogate_mask.beginFill(0)
            this._surrogate_mask.drawRect(padding[3],0,bounds.width-padding[3]-padding[1],bounds.height)
            this._surrogate_mask.endFill()
        }

        _destroySurrogate(){
            if(!this._surrogate) return

            this.removeChild(this._surrogate)
            this.removeChild(this._surrogate_hitbox)

            this._surrogate.destroy()
            this._surrogate_hitbox.destroy()

            this._surrogate = null
            this._surrogate_hitbox = null
        }

        _onSurrogateFocus(){
            this._setDOMInputVisible(true)
            //sometimes the input is not being focused by the mouseclick
            setTimeout(this._ensureFocus.bind(this),10)
        }

        _ensureFocus(){
            if(!this._hasFocus())
                this.focus()
        }

        _deriveSurrogateStyle(){
            let style = new PIXI.TextStyle()

            for(var key in this._input_style){
                switch(key){
                    case 'color':
                        style.fill = this._input_style.color
                        break
                    case 'fontFamily':
                    case 'fontSize':
                    case 'fontWeight':
                    case 'fontVariant':
                    case 'fontStyle':
                        style[key] = this._input_style[key]
                        break
                    case 'letterSpacing':
                        style.letterSpacing = parseFloat(this._input_style.letterSpacing)
                        break
                }
            }

            if(this._multiline){
                style.lineHeight = parseFloat(style.fontSize)
                style.wordWrap = true
                style.wordWrapWidth = this._getDOMInputBounds().width
            }

            if(this._dom_input.value.length === 0)
                style.fill = this._placeholderColor

            return style
        }

        _deriveSurrogatePadding(){
            let indent = this._input_style.textIndent ? parseFloat(this._input_style.textIndent) : 0

            if(this._input_style.padding && this._input_style.padding.length>0){
                let components = this._input_style.padding.trim().split(' ')

                if(components.length==1){
                    let padding = parseFloat(components[0])
                    return [padding,padding,padding,padding+indent]
                }else if(components.length==2){
                    let paddingV = parseFloat(components[0])
                    let paddingH = parseFloat(components[1])
                    return [paddingV,paddingH,paddingV,paddingH+indent]
                }else if(components.length==4){
                    let padding = components.map(component => {
                        return parseFloat(component)
                    })
                    padding[3] += indent
                    return padding
                }
            }

            return [0,0,0,indent]
        }

        _deriveSurrogateText(){
            return this._dom_input.value.length === 0 ? this._placeholder : this._dom_input.value
        }

        _updateFontMetrics(){
            const style = this._deriveSurrogateStyle()
            const font = style.toFontString()

            this._font_metrics = PIXI.TextMetrics.measureFont(font)
        }


        // CACHING OF INPUT BOX GRAPHICS

        _buildBoxCache(){
            this._destroyBoxCache()

            let states = ['DEFAULT','FOCUSED','DISABLED']
            let input_bounds = this._getDOMInputBounds()

            for(let i in states){
                this._box_cache[states[i]] = this._box_generator(
                    input_bounds.width,
                    input_bounds.height,
                    states[i]
                )
            }

            this._previous.input_bounds = input_bounds
        }

        _destroyBoxCache(){
            if(this._box){
                this.removeChild(this._box)
                this._box = null
            }

            for(let i in this._box_cache){
                this._box_cache[i].destroy()
                this._box_cache[i] = null
                delete this._box_cache[i]
            }
        }


        // HELPER FUNCTIONS

        _hasFocus(){
            return document.activeElement===this._dom_input
        }

        _setDOMInputVisible(visible){
            this._dom_input.style.display = visible ? 'block' : 'none'
        }

        _getCanvasBounds(){
            let rect = this._last_renderer.view.getBoundingClientRect()
            let bounds = {top: rect.top, left: rect.left, width: rect.width, height: rect.height}
            bounds.left += window.scrollX
            bounds.top += window.scrollY
            return bounds
        }

        _getDOMInputBounds(){
            let remove_after = false

            if(!this._dom_added){
                document.body.appendChild(this._dom_input)
                remove_after = true
            }

            let org_transform = this._dom_input.style.transform
            let org_display = this._dom_input.style.display
            this._dom_input.style.transform = ''
            this._dom_input.style.display = 'block'
            let bounds = this._dom_input.getBoundingClientRect()
            this._dom_input.style.transform = org_transform
            this._dom_input.style.display = org_display

            if(remove_after)
                document.body.removeChild(this._dom_input)

            return bounds
        }

        _getDOMRelativeWorldTransform(){
            let canvas_bounds = this._last_renderer.view.getBoundingClientRect()
            let matrix = this.worldTransform.clone()

            matrix.scale(this._resolution,this._resolution)
            matrix.scale(canvas_bounds.width/this._last_renderer.width,
                canvas_bounds.height/this._last_renderer.height)
            return matrix
        }

        _pixiMatrixToCSS(m){
            return 'matrix('+[m.a,m.b,m.c,m.d,m.tx,m.ty].join(',')+')'
        }

        _comparePixiMatrices(m1,m2){
            if(!m1 || !m2) return false
            return (
                m1.a == m2.a
                && m1.b == m2.b
                && m1.c == m2.c
                && m1.d == m2.d
                && m1.tx == m2.tx
                && m1.ty == m2.ty
            )
        }

        _compareClientRects(r1,r2){
            if(!r1 || !r2) return false
            return (
                r1.left == r2.left
                && r1.top == r2.top
                && r1.width == r2.width
                && r1.height == r2.height
            )
        }
    }


    function DefaultBoxGenerator(styles){
        styles = styles || {fill: 0xcccccc}

        if(styles.default){
            styles.focused = styles.focused || styles.default
            styles.disabled = styles.disabled || styles.default
        }else{
            let temp_styles = styles
            styles = {}
            styles.default = styles.focused = styles.disabled = temp_styles
        }

        return function(w,h,state){
            let style = styles[state.toLowerCase()]
            let box = new PIXI.Graphics()

            if(style.fill)
                box.beginFill(style.fill)

            if(style.stroke)
                box.lineStyle(
                    style.stroke.width || 1,
                    style.stroke.color || 0,
                    style.stroke.alpha || 1
                )

            if(style.rounded)
                box.drawRoundedRect(0,0,w,h,style.rounded)
            else
                box.drawRect(0,0,w,h)

            box.endFill()
            box.closePath()

            return box
        }
    }

    pkg.exportTo[0][pkg.exportTo[1]] = TextInput

})(
    typeof PIXI === 'object'
        ? { PIXI: PIXI, exportTo: [PIXI,'TextInput'] }
        : (
            typeof module === 'object'
                ? { PIXI: require('pixi.js'), exportTo: [module,'exports'] }
                : console.warn('[PIXI.TextInput] could not attach to PIXI namespace. Make sure to include this plugin after pixi.js') || {}
        )
)
/**
 * @classdesc Asset bouton
 * @author Vincent Audergon
 * @version 1.0
 */
class ToggleButton extends Asset {

    /**
     * Constructeur de l'asset bouton
     * @param {double} x Coordonnée X
     * @param {double} y Coordonnée Y
     * @param {string} label Le texte du bouton
     * @param {boolean} fitToText Taille du bouton en fonction du texte
     * @param {number} width Taille forcée du bouton
     */
    constructor(x, y, label, fitToText=false, width = 150) {
        super();

        this.fitToText = fitToText;
        this.refToggleGroup = null;
        this.width = width;
        this.height = 0;
        this._data = null;
        this.currentState = 'inactive';
        /** @type {double} la coordonée x */
        this.x = x;
        /** @type {double} la coordonée y */
        this.y = y;
        /** @type {string} le texte du bouton */
        this.text = label;

        this.lastOnClick = function(){
            this.toggle();
            if(this.refToggleGroup != null){
                this.refToggleGroup.updateList(this);
            }
        };

        this.states = {
            active: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics:new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, { fontFamily: 'Arial', fontSize: 18, fill: 0xFFFFFF, align: 'center' })
            },
            inactive: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics:new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, { fontFamily: 'Arial', fontSize: 18, fill: 0x000000, align: 'center' })
            },
            disabled: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics:new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, { fontFamily: 'Arial', fontSize: 18, fill: 0x666666, align: 'center' })
            }
        };

        this.onClick = this.lastOnClick;
        this.init();
        this.updateView();

    }

    /**
     * Initialise les éléments qui composent le bouton
     */
    init() {
        this.setText(this.text);
        for (let state of Object.keys(this.states)) {
            this.states[state].graphics.interactive = true;
            this.states[state].graphics.buttonMode = true;
            this.states[state].graphics.on('pointerdown', function () {
                this.onClick()
            }.bind(this));
        }
    }

    /**
     * Défini le texte à afficher sur le bouton
     * @param {string} text Le texte à afficher
     */
    setText(text) {
        for (let state of Object.keys(this.states)){
            this.states[state].lbl.text = text;
        }
        this.update();
    }


    update(){
        for (let state of Object.keys(this.states)){
            let buttonWidth = this.fitToText ? (20 + this.states[state].lbl.width): this.width;
            let buttonHeight = this.states[state].lbl.height * 1.5;
            this.height = buttonHeight;
            this.states[state].graphics.clear();
            if(state === 'active')
                this.states[state].graphics.beginFill(0x00AA00);
            else if(state === 'inactive')
                this.states[state].graphics.beginFill(0xDDDDDD);
            else
                this.states[state].graphics.beginFill(0xCCCCCC);
            this.states[state].graphics.drawRect(this.x - buttonWidth / 2, this.y - buttonHeight / 2, buttonWidth, buttonHeight);
            this.states[state].graphics.endFill();
            this.states[state].lbl.anchor.set(0.5);
            this.states[state].lbl.x = this.x;
            this.states[state].lbl.y = this.y;
        }
    }

    toggle(){
        this.currentState = (this.currentState === 'active') ? 'inactive' : 'active';
        this.updateView();
    }

    /**
     * Défini la fonction de callback à appeler après un click sur le bouton
     * @param {function} onClick La fonction de callback
     */
    setOnClick(onClick) {
        this.onClick = function () {
            onClick(this);
            this.toggle();
            if(this.refToggleGroup != null){
                this.refToggleGroup.updateList(this);
            }
        };
        this.lastOnClick = this.onClick;
    }

    /**
     * Retourne les éléments PIXI du bouton
     * @return {Object[]} les éléments PIXI qui composent le bouton
     */
    getPixiChildren() {
        return [this.states.inactive.graphics,
            this.states.inactive.lbl,
            this.states.active.graphics,
            this.states.active.lbl,
            this.states.disabled.graphics,
            this.states.disabled.lbl];
    }

    updateView(){
        for (let state of Object.keys(this.states)){
            if(state === this.currentState){
                this.states[state].lbl.visible = true;
                this.states[state].graphics.visible = true;
            } else{
                this.states[state].lbl.visible = false;
                this.states[state].graphics.visible = false;
            }
        }
    }

    isActive(){
        return this.currentState === 'active';
    }

    isEnabled(){
        return this.currentState !== 'disabled';
    }

    set data(data){
        this._data = data;
    }

    get data(){
        return this._data;
    }


    show(){
        this.updateView();
    }

    hide(){
        for (let element of this.getPixiChildren()){
            element.visible = false;
        }
    }

    disable(){
        this.currentState = 'disabled';
        this.onClick = function () {};
        this.updateView();
    }

    enable(){
        this.currentState = 'inactive';
        this.onClick = this.lastOnClick;
        this.updateView();
    }

    setRefToggleGroup(ref){
        this.refToggleGroup = ref;
    }

    updateFont(font){
        for(let state of Object.keys(this.states)){
            this.states[state].lbl.style.fontFamily = font;
        }
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        visible = true;
        console.log(this.currentState);
        for (let element of this.getPixiChildren()){
            element.visible = visible;
        }
    }


    setY(y) {
        this.y = y;
        this.update();
    }

    setX(x) {
        this.x = x;
        this.update();
    }


    getHeight() {
        return this.height;
    }

    getWidth() {
        return this.width;
    }
}
class ToggleGroup extends Asset {

    /**
     *
     * @param mode
     * @param buttons
     */
    constructor(mode = 'single', buttons = []) {
        super();
        this.mode = mode;
        this.buttons = buttons;
        for (let btn of buttons) {
            btn.setRefToggleGroup(this);
        }
    }

    addButton(button) {
        if (button != null) {
            button.setRefToggleGroup(this);
            this.buttons.push(button);
        }
    }

    setButtons(buttons = []) {
        this.buttons = buttons;
        for (let btn of buttons) {
            btn.setRefToggleGroup(this);
        }
    }

    getButtons() {
        return this.buttons;
    }

    getActives() {
        let actives = [];
        for (let btn of this.buttons)
            if (btn.currentState === 'active')
                actives.push(btn);
        return actives;
    }

    getInactives() {
        let inactives = [];
        for (let btn of this.buttons)
            if (btn.currentState === 'inactive')
                inactives.push(btn);
        return inactives;
    }

    getPixiChildren() {
        let elements = [];
        for (let toggleButton of this.buttons)
            for (let element of toggleButton.getPixiChildren())
                elements.push(element);
        return elements;
    }

    updateList(button) {
        if (this.mode === 'single' && button.isActive()) {
            for (let btn of this.buttons) {
                if (btn !== button && btn.isActive()) {
                    btn.toggle();
                }
            }
        }
    }

    reset(){
        for (let btn of this.buttons){
            if(btn.isActive())
                btn.toggle();
        }
    }

    show() {
        for (let btn of this.buttons)
            btn.show();
    }

    hide() {
        for (let btn of this.buttons)
            btn.hide();
    }

    updateFont(font){
        for (let btn of this.buttons) {
            btn.updateFont(font);
        }
    }

    clearButtons(){
        this.buttons = [];
    }


    getY() {
        return 0;
    }

    getX() {
        return 0;
    }

    setVisible(visible) {
        for (let btn of this.buttons)
            btn.setVisible(visible);
    }
}

/**
 * @classdesc Définition d'une interface (ihm)
 * @author Vincent Audergon
 * @version 1.0
 */
class Interface {

    /**
     * Constructeur d'une ihm
     * @param {Game} refGame la référence vers la classe de jeu
     * @param {string} scene le nom de la scène utilisée par l'ihm
     */
    constructor(refGame, scene) {
        this.refGame = refGame;
        /** @type {PIXI.Container} la scène PIXI sur laquelle dessiner l'ihm */
        this.scene = refGame.scenes[scene];
        /** @type {string[]} les textes de l'interface dans toutes les langues */
        this.texts = [];
        /** @type {Object[]} les éléments qui composent l'ihm */
        this.elements = [];
    }

    /**
     * Défini la scène de l'ihm
     * @param {PIXI.Container} scene la scène PIXI
     */
    setScene(scene) {
        this.scene = scene;
    }

    /**
     * Définis les éléments PIXI contenus dans la scène
     * @param {Objects} elements la liste des éléments PIXI
     */
    setElements(elements) {
        this.elements = elements;
    }

    /**
     * Défini la liste des textes de l'ihm dans la langue courante
     * @param {string[]} texts la liste des textes
     */
    setTexts(texts) {
        this.texts = texts;
    }

    /**
     * Retourne un texte de l'ihm dans la langue courante
     * @param {string} key la clé du texte
     * @return {string} le texte
     */
    getText(key) {
        return this.texts[key];
    }

    /**
     * Fonction de callback appelée lors d'un changement de langue
     * @param {string} lang
     */
    refreshLang(lang) { }

    /**
     * Retire tous les éléments de l'ihm de la scène PIXI
     */
    clear() {
        for (let element in this.elements) {
            let el = this.elements[element];
            if (el instanceof Asset) {
                for (let c of el.getPixiChildren()) {
                    this.scene.removeChild(c);
                }
            } else {
                this.scene.removeChild(el);
            }
        }
    }

    /**
     * Ajoute tous les éléments de l'ihm dans la scène PIXI
     */
    init() {
        for (let element in this.elements) {
            let el = this.elements[element];
            if (el instanceof Asset) {
                for (let c of el.getPixiChildren()) {
                    this.scene.addChild(c);
                }
            } else {
                if(el != null){
                this.scene.addChild(el);
                }
            }
        }
        this.refGame.showScene(this.scene);
    }

    /**
     * Affiche l'ihm
     */
    show() { }

}
/**
 * @classdesc IHM de la création d'exercice
 * @author Niederhauser Léo
 * @version 1.1
 */


//Constantes pour les couleurs utilisés sur les différents boutons
const BLUE_COLOR = 0x007bff;
const WITHE_COLOR = 0xffffff;
const GREEN_COLOR = 0x28a745;
const GREY_COLOR = 0x6c757d;
const BLACK_COLOR = 0x000000;

//Boutons pour valider et revenir en arrière dans la création de l'exercice
const nextbtn = new Button(400, 585, "Valider", BLUE_COLOR, WITHE_COLOR, true);
const returnbtn = new Button(200, 585, "Retour", GREY_COLOR, WITHE_COLOR, true);

//Tableau qui contient les informations de l'exercice
let tableauInfos = { map: "", dog: "", niche: "", biscuits: [], guide: "", solution: [] };

//Constantes des positions dans le canvas pour les emplacements des boutons sur la carte de jeu
const FLATNATURE_XYPLACEMENTSBTN = [[75, 75], [300, 75], [525, 75], [75, 275], [300, 275], [525, 275]];
const BEACH_XYPLACEMENTSBTN = [[75, 180], [225, 95], [225, 250], [375, 180], [525, 65], [525, 280]];
const OCEAN_XYPLACEMENTSBTN = [[225, 65], [75, 180], [225, 180], [375, 180], [525, 180], [375, 300]];

//Constantes de Médor qui sera déplacé sur les emplacements des boutons de la carte de jeu
let dogBrown = null;
let dogHouse = null;
let doggyBiscuit = null;

//Constantes des positions dans le canvas pour les emplacements des shapes sur le guide
const XYGuides4Tokens = [[165, 400, ""], [255, 400, ""], [345, 400, ""], [435, 400, ""]];
const XYGuides5Tokens = [[155, 405, "2X"], [250, 405, ""], [250, 480, ""], [345, 450, ""], [440, 450, "2X"]];
const XYGuides6Tokens = [[145, 395, ""], [225, 395, ""], [260, 470, ""], [340, 470, ""], [370, 395, ""], [450, 395, ""]];


class Create extends Interface {

    constructor(refGame) {

        super(refGame, "create");


    }

    show() {

        //retire tous les éléments du canvas
        this.clear();
        //retire tous les éléments du tableau qui contient les éléments qui seront poussés sur la canvas à la fin grâce au this.init()
        this.elements = [];

        //change la langue du texte du tutoriel selon langue sélecionnée
        this.refreshLang(this.refGame.global.resources.getLanguage());
        //change le texte du tutoriel pour les explications du mode créer
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('startCreate'));
        //désactive tous les boutons inutile pour le mode créer
        this.refGame.global.util.hideTutorialInputs('btnResetTr', 'btnAudioGame', 'next', 'btnReset');

        //efface le tableau quand l'utilisateur revient à l'écran d'accueil
        tableauInfos.map = "";
        tableauInfos.dog = "";
        tableauInfos.niche = "";
        tableauInfos.biscuits = [];
        tableauInfos.guide = "";

        //creation du bouton pour démarrer la création de l'exercice et ajout de ce dernier sur le canvas
        this.startButton = new Button(300, 300, this.refGame.global.resources.getOtherText('start'), BLUE_COLOR, WITHE_COLOR, true);
        //ajout d'un evenement au bouton afin qu'il passe à la 1ère étape de la création de l'exercice
        this.startButton.setOnClick(this.choisirCarteDeJeu.bind(this));
        //ajout du bouton sur le canvas
        this.elements.push(this.startButton);

        //creation du texte de bienvenue
        let txtCreatMode = new PIXI.Text(this.refGame.global.resources.getOtherText('welcome'), {
            // fontFamily: 'Arial',
            fontSize: 24,
            fill: 0x000000,
            align: 'left',
            wordWrap: true,
            wordWrapWidth: 580,
            fontWeight: "bolder"
        });
        txtCreatMode.x = 135;
        txtCreatMode.y = 150;
        //ajout du texte sur le canvas
        this.elements.push(txtCreatMode)

        //ajout des élements du tableau sur le canvas
        this.init();
    }

    refreshLang(lang) {

    }

    refreshFont(isOpenDyslexic) {

    }

    /**
     * @desc Crée la page de choix de la carte de jeu avec tous les bons composants et leur fonctionnalités
     */
    choisirCarteDeJeu() {
        this.clear();
        this.elements = [];
        //changement du texte du tutoriel 
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('ChoixCarte'));

        //creation des boutons pour choisir la carte de jeu et ajout de ceux-ci dans le tableau qui sera poussé sur le canvas
        this.flatnaturebtn = new Button(100, 400, this.refGame.global.resources.getOtherText('FlatNature'), BLUE_COLOR, WITHE_COLOR, true);
        this.elements.push(this.flatnaturebtn);
        this.oceanbtn = new Button(300, 400, this.refGame.global.resources.getOtherText('Ocean'), BLUE_COLOR, WITHE_COLOR, true);
        this.elements.push(this.oceanbtn);
        this.beachbtn = new Button(500, 400, this.refGame.global.resources.getOtherText('Beach'), BLUE_COLOR, WITHE_COLOR, true);
        this.elements.push(this.beachbtn);

        //ajout d'un evenement à chaque bouton pour qu'il affiche la carte de jeu correspondante
        this.flatnaturebtn.setOnClick(this.afficheCarteDeJeu.bind(this, "flat_nature"));
        this.beachbtn.setOnClick(this.afficheCarteDeJeu.bind(this, "beach"));
        this.oceanbtn.setOnClick(this.afficheCarteDeJeu.bind(this, "ocean"));

        //ajout d'un evenement au bouton retour pour revenir à l'écran d'accueil et ajout d'un evenement au bouton valider pour passer à l'étape suivante
        nextbtn.setOnClick(this.choisirEmplacementMedor.bind(this));
        returnbtn.setOnClick(this.show.bind(this));

        //test pour savoir si l'utilisateur a déjà choisi une carte de jeu et si oui, affiche la carte de jeu correspondante
        if (tableauInfos.map != "") {
            this.afficheCarteDeJeu(tableauInfos.map);
        } else {
            nextbtn.setdisabled(true);
        }

        //ajout des boutons dans le tableau qui sera poussé sur le canvas
        this.elements.push(returnbtn, nextbtn);

        //ajout les éléments du tableau sur le canvas
        this.init();

    }

    /**
     * @desc Affiche la carte de jeu correspondante au bouton cliqué
     * @param {string} carte - nom de la carte de jeu à afficher
     * @example afficheCarteDeJeu("flat_nature") 
     */
    afficheCarteDeJeu(carte) {

        //test si l'utilisateur a choisi une carte différente. Si oui alors on efface les données du chien et de la niche dans le tableau d'informations de l'exercice
        if (tableauInfos.map != carte) {
            tableauInfos.dog = "";
            tableauInfos.niche = "";
            tableauInfos.biscuits = [];
            tableauInfos.guide = "";
        }

        //appel de la classe CarteNeutre pour afficher la carte de jeu correspondante
        this.map = new CarteNeutre(this.refGame, carte);

        //ajout de la carte de jeu dans le tableau pour former le JSON plus tard
        tableauInfos.map = carte;

        //afficher la carte de jeu sur la canvas
        this.elements.push(this.map)
        this.init();

        //test pour changer la couleur du bouton en fonction de s'il est sélectionné ou non
        if (carte == "flat_nature") {
            this.flatnaturebtn.setbgColor(GREEN_COLOR);
            this.beachbtn.setbgColor(BLUE_COLOR);
            this.oceanbtn.setbgColor(BLUE_COLOR);
        } else if (carte == "beach") {
            this.flatnaturebtn.setbgColor(BLUE_COLOR);
            this.beachbtn.setbgColor(GREEN_COLOR);
            this.oceanbtn.setbgColor(BLUE_COLOR);

        } else if (carte == "ocean") {
            this.flatnaturebtn.setbgColor(BLUE_COLOR);
            this.beachbtn.setbgColor(BLUE_COLOR);
            this.oceanbtn.setbgColor(GREEN_COLOR);
        }
    }

    choisirEmplacementMedor() {
        if (tableauInfos.map != "") {
            this.clear();
            this.elements = [];
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText('start'));
            this.map = new CarteNeutre(this.refGame, tableauInfos.map);
            returnbtn.setOnClick(this.choisirCarteDeJeu.bind(this));
            nextbtn.setOnClick(this.choisirEmplacementNiche.bind(this));
            this.elements.push(returnbtn, nextbtn, this.map);

            //creation de l'image du chien puis affichage de celle-ci sur le canvas
            dogBrown = new Shape(300, 450, 50, 50, this.refGame.global.resources.getImage('dog_brown'), '')

            // this.elements.push(dogBrown);

            if (tableauInfos.map === "flat_nature") {

                this.XYPlacements = FLATNATURE_XYPLACEMENTS;

            } else if (tableauInfos.map === "beach") {

                this.XYPlacements = BEACH_XYPLACEMENTS;

            } else if (tableauInfos.map === "ocean") {

                this.XYPlacements = OCEAN_XYPLACEMENTS;

            }

            //création des emplacements sous forme de boutons qui seront sur la carte de jeu 
            this.firstPlacement = new PlacementBtn(this.XYPlacements[0][0], this.XYPlacements[0][1], 0, this.refGame);
            this.secondPlacement = new PlacementBtn(this.XYPlacements[1][0], this.XYPlacements[1][1], 1, this.refGame);
            this.thirdPlacement = new PlacementBtn(this.XYPlacements[2][0], this.XYPlacements[2][1], 2, this.refGame);
            this.fourthPlacement = new PlacementBtn(this.XYPlacements[3][0], this.XYPlacements[3][1], 3, this.refGame);
            this.fifthPlacement = new PlacementBtn(this.XYPlacements[4][0], this.XYPlacements[4][1], 4, this.refGame);
            this.sixthPlacement = new PlacementBtn(this.XYPlacements[5][0], this.XYPlacements[5][1], 5, this.refGame);


            //ajout des emplacements dasn le tableau qui sera poussé sur le canvas
            this.elements.push(this.firstPlacement, this.secondPlacement, this.thirdPlacement, this.fourthPlacement, this.fifthPlacement, this.sixthPlacement);

            //ajout d'un evenement à chaque emplacement pour qu'il affiche le chien au bon emplacmeent
            this.firstPlacement.setOnClick(this.modifierChien.bind(this, "0"));
            this.secondPlacement.setOnClick(this.modifierChien.bind(this, "1"));
            this.thirdPlacement.setOnClick(this.modifierChien.bind(this, "2"));
            this.fourthPlacement.setOnClick(this.modifierChien.bind(this, "3"));
            this.fifthPlacement.setOnClick(this.modifierChien.bind(this, "4"));
            this.sixthPlacement.setOnClick(this.modifierChien.bind(this, "5"));

            if (tableauInfos.dog == "") {
                this.elements.push(dogBrown);
            } else {
                this.modifierChien(tableauInfos.dog);
            }


            this.init();

        } else {

            //affiche une alerte si l'utilisateur n'a pas choisi de carte de jeu
            Swal.fire(
                'Erreur !',
                'Veuillez choisir une carte de jeu !',
                'error'
            )

            //appel de la fonction choisirCarteDeJeu pour revenir à l'écran de choix de la carte de jeu
            this.choisirCarteDeJeu();
        }


    }

    modifierChien(num) {

        if (tableauInfos.niche == num) {
            tableauInfos.niche = "";
        }

        tableauInfos.biscuits.forEach(biscuit => {
            if (biscuit == num) {
                tableauInfos.biscuits.splice(tableauInfos.biscuits.indexOf(num), 1);
            }
        });


        tableauInfos.biscuits.forEach(biscuit => {
            if (biscuit == num) {
                tableauInfos.biscuits[biscuit] = "";
            }
        });

        //ajout du numéro de l'emplacemenz du chien dans le tableau des informations de l'exercice
        tableauInfos.dog = num;

        //test pour savoir si l'utilisateur a déjà choisi un emplacement pour le chien
        if (tableauInfos.dog != "") {
            //si oui, on rend le chien d'exemple invisible
            dogBrown.setVisible(false);
        }

        //suppression de l'image du chien sur tous les emplacements
        this.firstPlacement.removeImage();
        this.secondPlacement.removeImage();
        this.thirdPlacement.removeImage();
        this.fourthPlacement.removeImage();
        this.fifthPlacement.removeImage();
        this.sixthPlacement.removeImage();

        //ajout de l'image du chien sur l'emplacement choisi
        switch (num) {
            case "0":
                this.firstPlacement.setImage(['dog_brown']);
                break;
            case "1":
                this.secondPlacement.setImage(['dog_brown']);
                break;
            case "2":
                this.thirdPlacement.setImage(['dog_brown']);
                break;
            case "3":
                this.fourthPlacement.setImage(['dog_brown']);
                break;
            case "4":
                this.fifthPlacement.setImage(['dog_brown']);
                break;
            case "5":
                this.sixthPlacement.setImage(['dog_brown']);
                break;
        }
    }

    choisirEmplacementNiche() {
        if (tableauInfos.dog != "") {
            this.clear();
            this.elements = [];
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText('end'));
            this.map = new CarteNeutre(this.refGame, tableauInfos.map);
            returnbtn.setOnClick(this.choisirEmplacementMedor.bind(this));
            nextbtn.setOnClick(this.choisirEmplacementBiscuits.bind(this));
            this.elements.push(returnbtn, nextbtn, this.map);

            //creation de l'image de la niche puis affichage de celle-ci sur le canvas
            dogHouse = new Shape(300, 450, 50, 50, this.refGame.global.resources.getImage('dog_house'), '')

            if (tableauInfos.map === "flat_nature") {

                this.XYPlacements = FLATNATURE_XYPLACEMENTS;

            } else if (tableauInfos.map === "beach") {

                this.XYPlacements = BEACH_XYPLACEMENTS;

            } else if (tableauInfos.map === "ocean") {

                this.XYPlacements = OCEAN_XYPLACEMENTS;

            }

            //création des emplacements sous forme de boutons qui seront sur la carte de jeu 
            this.firstPlacement = new PlacementBtn(this.XYPlacements[0][0], this.XYPlacements[0][1], 0, this.refGame);
            this.secondPlacement = new PlacementBtn(this.XYPlacements[1][0], this.XYPlacements[1][1], 1, this.refGame);
            this.thirdPlacement = new PlacementBtn(this.XYPlacements[2][0], this.XYPlacements[2][1], 2, this.refGame);
            this.fourthPlacement = new PlacementBtn(this.XYPlacements[3][0], this.XYPlacements[3][1], 3, this.refGame);
            this.fifthPlacement = new PlacementBtn(this.XYPlacements[4][0], this.XYPlacements[4][1], 4, this.refGame);
            this.sixthPlacement = new PlacementBtn(this.XYPlacements[5][0], this.XYPlacements[5][1], 5, this.refGame);


            //ajout des emplacements dasn le tableau qui sera poussé sur le canvas
            this.elements.push(this.firstPlacement, this.secondPlacement, this.thirdPlacement, this.fourthPlacement, this.fifthPlacement, this.sixthPlacement);

            //ajout d'un evenement à chaque emplacement pour qu'il affiche la niche au bon emplacmeent
            this.firstPlacement.setOnClick(this.modifierNiche.bind(this, "0"));
            this.secondPlacement.setOnClick(this.modifierNiche.bind(this, "1"));
            this.thirdPlacement.setOnClick(this.modifierNiche.bind(this, "2"));
            this.fourthPlacement.setOnClick(this.modifierNiche.bind(this, "3"));
            this.fifthPlacement.setOnClick(this.modifierNiche.bind(this, "4"));
            this.sixthPlacement.setOnClick(this.modifierNiche.bind(this, "5"));

            if (tableauInfos.niche == "") {
                this.elements.push(dogHouse);
                this.modifierChien(tableauInfos.dog);
            } else {
                this.modifierNiche(tableauInfos.niche);
            }

            this.init();

        } else {

            //affiche une alerte si l'utilisateur n'a pas choisi d'emplacement pour le chien
            Swal.fire(
                'Erreur !',
                'Veuillez choisir un emplacement pour le chien !',
                'error'
            )

            //appel de la fonction choisirEmplacementMedor pour revenir à l'écran de choix de l'emplacement du chien
            this.choisirEmplacementMedor();
        }


    }

    modifierNiche(num) {

        if (tableauInfos.dog != num) {
            //ajout du numéro de l'emplacement de la niche dans le tableau des informations de l'exercice
            tableauInfos.niche = num;

            tableauInfos.biscuits.forEach(biscuit => {
                if (biscuit == num) {
                    tableauInfos.biscuits.splice(tableauInfos.biscuits.indexOf(num), 1);
                }
            });

            //test pour savoir si l'utilisateur a déjà choisi un emplacement pour la niche
            if (tableauInfos.niche != "") {
                //si oui, on rend la niche d'exemple invisible
                dogHouse.setVisible(false);
            }

            //suppression de l'image de la niche sur tous les emplacements
            this.firstPlacement.removeImage();
            this.secondPlacement.removeImage();
            this.thirdPlacement.removeImage();
            this.fourthPlacement.removeImage();
            this.fifthPlacement.removeImage();
            this.sixthPlacement.removeImage();

            //ajout de l'image de la niche sur l'emplacement choisi
            switch (num) {
                case "0":
                    this.firstPlacement.setImage(['dog_house']);
                    break;
                case "1":
                    this.secondPlacement.setImage(['dog_house']);
                    break;
                case "2":
                    this.thirdPlacement.setImage(['dog_house']);
                    break;
                case "3":
                    this.fourthPlacement.setImage(['dog_house']);
                    break;
                case "4":
                    this.fifthPlacement.setImage(['dog_house']);
                    break;
                case "5":
                    this.sixthPlacement.setImage(['dog_house']);
                    break;
            }

            switch (tableauInfos.dog) {
                case "0":
                    this.firstPlacement.setImage(['dog_brown']);
                    break;
                case "1":
                    this.secondPlacement.setImage(['dog_brown']);
                    break;
                case "2":
                    this.thirdPlacement.setImage(['dog_brown']);
                    break;
                case "3":
                    this.fourthPlacement.setImage(['dog_brown']);
                    break;
                case "4":
                    this.fifthPlacement.setImage(['dog_brown']);
                    break;
                case "5":
                    this.sixthPlacement.setImage(['dog_brown']);
                    break;
            }
        } else {
            Swal.fire(
                'Erreur !',
                'Veuillez choisir un emplacement different pour la niche !',
                'error'
            )
        }
    }

    /*
    * Fonction qui permet de choisir l'emplacement des biscuits
    */
    choisirEmplacementBiscuits() {
        if (tableauInfos.niche != "") {
            this.clear();
            this.elements = [];
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText('biscuit'));
            this.map = new CarteNeutre(this.refGame, tableauInfos.map);
            returnbtn.setOnClick(this.choisirEmplacementNiche.bind(this));
            nextbtn.setOnClick(this.choisirGuide.bind(this));
            this.elements.push(returnbtn, nextbtn, this.map);

            //creation de l'image du biscuit puis affichage de celle-ci sur le canvas
            doggyBiscuit = new Shape(300, 450, 50, 50, this.refGame.global.resources.getImage('doggy_biscuit'), '')
            this.elements.push(doggyBiscuit);

            if (tableauInfos.map === "flat_nature") {

                this.XYPlacements = FLATNATURE_XYPLACEMENTS;

            } else if (tableauInfos.map === "beach") {

                this.XYPlacements = BEACH_XYPLACEMENTS;

            } else if (tableauInfos.map === "ocean") {

                this.XYPlacements = OCEAN_XYPLACEMENTS;

            }

            //création des emplacements sous forme de boutons qui seront sur la carte de jeu 
            this.firstPlacement = new PlacementBtn(this.XYPlacements[0][0], this.XYPlacements[0][1], 0, this.refGame);
            this.secondPlacement = new PlacementBtn(this.XYPlacements[1][0], this.XYPlacements[1][1], 1, this.refGame);
            this.thirdPlacement = new PlacementBtn(this.XYPlacements[2][0], this.XYPlacements[2][1], 2, this.refGame);
            this.fourthPlacement = new PlacementBtn(this.XYPlacements[3][0], this.XYPlacements[3][1], 3, this.refGame);
            this.fifthPlacement = new PlacementBtn(this.XYPlacements[4][0], this.XYPlacements[4][1], 4, this.refGame);
            this.sixthPlacement = new PlacementBtn(this.XYPlacements[5][0], this.XYPlacements[5][1], 5, this.refGame);


            //ajout des emplacements dasn le tableau qui sera poussé sur le canvas
            this.elements.push(this.firstPlacement, this.secondPlacement, this.thirdPlacement, this.fourthPlacement, this.fifthPlacement, this.sixthPlacement);

            //ajout d'un evenement à chaque emplacement pour qu'il affiche la niche au bon emplacement
            this.firstPlacement.setOnClick(this.ajouterBiscuit.bind(this, "0"));
            this.secondPlacement.setOnClick(this.ajouterBiscuit.bind(this, "1"));
            this.thirdPlacement.setOnClick(this.ajouterBiscuit.bind(this, "2"));
            this.fourthPlacement.setOnClick(this.ajouterBiscuit.bind(this, "3"));
            this.fifthPlacement.setOnClick(this.ajouterBiscuit.bind(this, "4"));
            this.sixthPlacement.setOnClick(this.ajouterBiscuit.bind(this, "5"));
            if (tableauInfos.biscuits.length == 0) {
                doggyBiscuit.setVisible(true);
                this.modifierNiche(tableauInfos.niche);
            } else {
                doggyBiscuit.setVisible(false);
                this.modifierBiscuits(tableauInfos.biscuits);
            }

            this.init();

        } else {

            //affiche une alerte si l'utilisateur n'a pas choisi d'emplacement pour la niche
            Swal.fire(
                'Erreur !',
                'Veuillez choisir un emplacement pour la niche !',
                'error'
            )

            //appel de la fonction choisirEmplacementNiche pour revenir à l'écran de choix de l'emplacement de la niche
            this.choisirEmplacementNiche();
        }


    }

    ajouterBiscuit(num) {
        if (tableauInfos.biscuits.find(element => element === num)) {
            tableauInfos.biscuits.splice(tableauInfos.biscuits.indexOf(num), 1);
        } else if (num != tableauInfos.niche && num != tableauInfos.dog) {
            tableauInfos.biscuits.push(num);
        } else {
            Swal.fire(
                'Erreur !',
                'Veuillez choisir un emplacement different pour le biscuit !',
                'error'
            )
        }

        this.modifierBiscuits(tableauInfos.biscuits);
    }

    modifierBiscuits(biscuits) {

        //test pour savoir si l'utilisateur a déjà choisi un emplacement pour les biscuits
        if (biscuits.length > 0) {
            //si oui, on rend le biscuit d'exemple invisible
            doggyBiscuit.setVisible(false);
        } else {
            //si non, on rend le biscuit d'exemple visible
            doggyBiscuit.setVisible(true);
        }

        //suppression de l'image de la niche sur tous les emplacements
        this.firstPlacement.removeImage();
        this.secondPlacement.removeImage();
        this.thirdPlacement.removeImage();
        this.fourthPlacement.removeImage();
        this.fifthPlacement.removeImage();
        this.sixthPlacement.removeImage();

        biscuits.forEach(biscuit => {
            if (biscuit != "") {


                //ajout de l'image du biscuit sur l'emplacement choisi
                switch (biscuit) {
                    case "0":
                        this.firstPlacement.setImage(['doggy_biscuit']);
                        break;
                    case "1":
                        this.secondPlacement.setImage(['doggy_biscuit']);
                        break;
                    case "2":
                        this.thirdPlacement.setImage(['doggy_biscuit']);
                        break;
                    case "3":
                        this.fourthPlacement.setImage(['doggy_biscuit']);
                        break;
                    case "4":
                        this.fifthPlacement.setImage(['doggy_biscuit']);
                        break;
                    case "5":
                        this.sixthPlacement.setImage(['doggy_biscuit']);
                        break;
                }
            }

        });


        //ajout de l'image de la niche sur l'emplacement choisi
        switch (tableauInfos.niche) {
            case "0":
                this.firstPlacement.setImage(['dog_house']);
                break;
            case "1":
                this.secondPlacement.setImage(['dog_house']);
                break;
            case "2":
                this.thirdPlacement.setImage(['dog_house']);
                break;
            case "3":
                this.fourthPlacement.setImage(['dog_house']);
                break;
            case "4":
                this.fifthPlacement.setImage(['dog_house']);
                break;
            case "5":
                this.sixthPlacement.setImage(['dog_house']);
                break;
        }

        switch (tableauInfos.dog) {
            case "0":
                this.firstPlacement.setImage(['dog_brown']);
                break;
            case "1":
                this.secondPlacement.setImage(['dog_brown']);
                break;
            case "2":
                this.thirdPlacement.setImage(['dog_brown']);
                break;
            case "3":
                this.fourthPlacement.setImage(['dog_brown']);
                break;
            case "4":
                this.fifthPlacement.setImage(['dog_brown']);
                break;
            case "5":
                this.sixthPlacement.setImage(['dog_brown']);
                break;
        }

    }

    choisirGuide() {
        this.clear();
        this.elements = [];
        //changement du texte du tutoriel 
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('ChoixGuide'));
        this.map = new CarteNeutre(this.refGame, tableauInfos.map);

        //ajout d'un evenement au bouton retour pour revenir à l'écran d'accueil et ajout d'un evenement au bouton valider pour passer à l'étape suivante
        nextbtn.setOnClick(this.choisirSolution.bind(this));
        returnbtn.setOnClick(this.choisirEmplacementBiscuits.bind(this));

        //ajout des boutons dans le tableau qui sera poussé sur le canvas
        this.elements.push(returnbtn, nextbtn, this.map);

        //creation des boutons pour choisir la carte de jeu et ajout de ceux-ci dans le tableau qui sera poussé sur le canvas
        this.tokens4 = new Button(100, 540, this.refGame.global.resources.getOtherText('4Tokens'), BLUE_COLOR, WITHE_COLOR, true);
        this.elements.push(this.tokens4);
        this.tokens5 = new Button(300, 540, this.refGame.global.resources.getOtherText('5Tokens'), BLUE_COLOR, WITHE_COLOR, true);
        this.elements.push(this.tokens5);
        this.tokens6 = new Button(500, 540, this.refGame.global.resources.getOtherText('6Tokens'), BLUE_COLOR, WITHE_COLOR, true);
        this.elements.push(this.tokens6);

        if (tableauInfos.map === "flat_nature") {

            this.XYPlacements = FLATNATURE_XYPLACEMENTS;

        } else if (tableauInfos.map === "beach") {

            this.XYPlacements = BEACH_XYPLACEMENTS;

        } else if (tableauInfos.map === "ocean") {

            this.XYPlacements = OCEAN_XYPLACEMENTS;

        }

        //création des emplacements sous forme de boutons qui seront sur la carte de jeu 
        this.firstPlacement = new Placement(this.XYPlacements[0][0], this.XYPlacements[0][1], 0, this.refGame);
        this.secondPlacement = new Placement(this.XYPlacements[1][0], this.XYPlacements[1][1], 1, this.refGame);
        this.thirdPlacement = new Placement(this.XYPlacements[2][0], this.XYPlacements[2][1], 2, this.refGame);
        this.fourthPlacement = new Placement(this.XYPlacements[3][0], this.XYPlacements[3][1], 3, this.refGame);
        this.fifthPlacement = new Placement(this.XYPlacements[4][0], this.XYPlacements[4][1], 4, this.refGame);
        this.sixthPlacement = new Placement(this.XYPlacements[5][0], this.XYPlacements[5][1], 5, this.refGame);


        //ajout des emplacements dasn le tableau qui sera poussé sur le canvas
        this.elements.push(this.firstPlacement, this.secondPlacement, this.thirdPlacement, this.fourthPlacement, this.fifthPlacement, this.sixthPlacement);

        //ajout d'un evenement à chaque bouton pour qu'il affiche la carte de jeu correspondante
        this.tokens4.setOnClick(this.afficherGuide.bind(this, "4Tokens"));
        this.tokens5.setOnClick(this.afficherGuide.bind(this, "5Tokens"));
        this.tokens6.setOnClick(this.afficherGuide.bind(this, "6Tokens"));

        //test pour savoir si l'utilisateur a déjà choisi une carte de jeu et si oui, affiche la carte de jeu correspondante
        if (tableauInfos.guide != "") {
            this.afficherGuide(tableauInfos.guide);
        } else {
            this.afficherTous();
        }

        //ajout les éléments du tableau sur le canvas
        this.init();

    }

    /**
     * @desc Affiche la carte de jeu correspondante au bouton cliqué
     * @param {string} carte - nom de la carte de jeu à afficher
     * @example afficheCarteDeJeu("flat_nature") 
     */
    afficherGuide(guide) {

        this.clear();
        this.elements = [];
        this.map = new CarteNeutre(this.refGame, tableauInfos.map);
        this.elements.push(this.map, nextbtn, returnbtn, this.tokens4, this.tokens5, this.tokens6);
        //ajout des emplacements dasn le tableau qui sera poussé sur le canvas
        this.elements.push(this.firstPlacement, this.secondPlacement, this.thirdPlacement, this.fourthPlacement, this.fifthPlacement, this.sixthPlacement);

        //test si l'utilisateur a choisi un guide différent. Si oui alors on efface les données de la solution dans le tableau d'informations de l'exercice
        if (tableauInfos.guide != guide) {
            // tableauInfos.solution = "";
        }

        //création du guide
        this.guide = new GuideTokenBasic(this.refGame, guide);

        //ajout du guide dans le tableau pour former le JSON plus tard
        tableauInfos.guide = guide;

        //afficher le guide sur la canvas
        this.elements.push(this.guide)
        this.init();

        //test pour changer la couleur du bouton en fonction de s'il est sélectionné ou non
        if (guide == "4Tokens") {
            this.tokens4.setbgColor(GREEN_COLOR);
            this.tokens5.setbgColor(BLUE_COLOR);
            this.tokens6.setbgColor(BLUE_COLOR);
        } else if (guide == "5Tokens") {
            this.tokens4.setbgColor(BLUE_COLOR);
            this.tokens5.setbgColor(GREEN_COLOR);
            this.tokens6.setbgColor(BLUE_COLOR);

        } else if (guide == "6Tokens") {
            this.tokens4.setbgColor(BLUE_COLOR);
            this.tokens5.setbgColor(BLUE_COLOR);
            this.tokens6.setbgColor(GREEN_COLOR);
        }
        this.afficherTous();
    }

    afficherTous() {
        //ajout de l'image de la niche sur l'emplacement choisi
        switch (tableauInfos.niche) {
            case "0":
                this.firstPlacement.setImage(['dog_house']);
                break;
            case "1":
                this.secondPlacement.setImage(['dog_house']);
                break;
            case "2":
                this.thirdPlacement.setImage(['dog_house']);
                break;
            case "3":
                this.fourthPlacement.setImage(['dog_house']);
                break;
            case "4":
                this.fifthPlacement.setImage(['dog_house']);
                break;
            case "5":
                this.sixthPlacement.setImage(['dog_house']);
                break;
        }

        switch (tableauInfos.dog) {
            case "0":
                this.firstPlacement.setImage(['dog_brown']);
                break;
            case "1":
                this.secondPlacement.setImage(['dog_brown']);
                break;
            case "2":
                this.thirdPlacement.setImage(['dog_brown']);
                break;
            case "3":
                this.fourthPlacement.setImage(['dog_brown']);
                break;
            case "4":
                this.fifthPlacement.setImage(['dog_brown']);
                break;
            case "5":
                this.sixthPlacement.setImage(['dog_brown']);
                break;
        }

        tableauInfos.biscuits.forEach(biscuit => {
            if (biscuit != "") {


                //ajout de l'image du biscuit sur l'emplacement choisi
                switch (biscuit) {
                    case "0":
                        this.firstPlacement.setImage(['doggy_biscuit']);
                        break;
                    case "1":
                        this.secondPlacement.setImage(['doggy_biscuit']);
                        break;
                    case "2":
                        this.thirdPlacement.setImage(['doggy_biscuit']);
                        break;
                    case "3":
                        this.fourthPlacement.setImage(['doggy_biscuit']);
                        break;
                    case "4":
                        this.fifthPlacement.setImage(['doggy_biscuit']);
                        break;
                    case "5":
                        this.sixthPlacement.setImage(['doggy_biscuit']);
                        break;
                }
            }
        });

    }

    choisirSolution() {

        if (tableauInfos.guide != "") {

            this.clear();
            this.elements = [];
            //changement du texte du tutoriel 
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('ChoixSolution'));
            this.map = new CarteNeutre(this.refGame, tableauInfos.map);

            //ajout d'un evenement au bouton retour pour revenir à l'écran d'accueil et ajout d'un evenement au bouton valider pour passer à l'étape suivante
            nextbtn.setOnClick(this.createJSON.bind(this));
            returnbtn.setOnClick(this.choisirGuide.bind(this));

            //ajout des boutons dans le tableau qui sera poussé sur le canvas
            this.elements.push(returnbtn, nextbtn, this.map);

            if (tableauInfos.map === "flat_nature") {

                this.XYPlacements = FLATNATURE_XYPLACEMENTS;

            } else if (tableauInfos.map === "beach") {

                this.XYPlacements = BEACH_XYPLACEMENTS;

            } else if (tableauInfos.map === "ocean") {

                this.XYPlacements = OCEAN_XYPLACEMENTS;

            }

            //création des emplacements sous forme de boutons qui seront sur la carte de jeu 
            this.firstPlacement = new Placement(this.XYPlacements[0][0], this.XYPlacements[0][1], 0, this.refGame);
            this.secondPlacement = new Placement(this.XYPlacements[1][0], this.XYPlacements[1][1], 1, this.refGame);
            this.thirdPlacement = new Placement(this.XYPlacements[2][0], this.XYPlacements[2][1], 2, this.refGame);
            this.fourthPlacement = new Placement(this.XYPlacements[3][0], this.XYPlacements[3][1], 3, this.refGame);
            this.fifthPlacement = new Placement(this.XYPlacements[4][0], this.XYPlacements[4][1], 4, this.refGame);
            this.sixthPlacement = new Placement(this.XYPlacements[5][0], this.XYPlacements[5][1], 5, this.refGame);


            //ajout des emplacements dasn le tableau qui sera poussé sur le canvas
            this.elements.push(this.firstPlacement, this.secondPlacement, this.thirdPlacement, this.fourthPlacement, this.fifthPlacement, this.sixthPlacement);

            //ajout du guide dans le tableau qui sera poussé sur le canvas
            this.guide = new GuideTokenBasic(this.refGame, tableauInfos.guide);
            this.elements.push(this.guide);

            //rajout de tous les objets précédenment créés sur le canvas (chien, niche, biscuits)
            this.afficherTous();

            //appel pour afficher directement les tokens par défaut sur le guide et ainsi de suite créer les événements sur les tokens pour changer la valeur dès qu'on click dessus
            switch (tableauInfos.guide) {
                case "4Tokens":
                    tableauInfos.solution = ["", "", "", ""];
                    for (let i = 0; i < XYGuides4Tokens.length; i++) {
                        this.changerContenuTokens(XYGuides4Tokens[i][0], XYGuides4Tokens[i][1], XYGuides4Tokens[i][2], tableauInfos.solution[i], i);
                    };
                    break;
                case "5Tokens":
                    tableauInfos.solution = ["", "", "", "", ""];
                    for (let i = 0; i < XYGuides5Tokens.length; i++) {
                        this.changerContenuTokens(XYGuides5Tokens[i][0], XYGuides5Tokens[i][1], XYGuides5Tokens[i][2], tableauInfos.solution[i], i);
                    };
                    break;
                case "6Tokens":
                    tableauInfos.solution = ["", "", "", "", "", ""];
                    for (let i = 0; i < XYGuides6Tokens.length; i++) {
                        this.changerContenuTokens(XYGuides6Tokens[i][0], XYGuides6Tokens[i][1], XYGuides6Tokens[i][2], tableauInfos.solution[i], i);
                    };
                    break;
            }

            //ajout les éléments du tableau sur le canvas
            this.init();

        } else {

            //affiche une alerte si l'utilisateur n'a pas choisi de guide
            Swal.fire(
                'Erreur !',
                'Veuillez choisir un guide dabord !',
                'error'
            )

            //retour à l'écran de choix de l'emplacement du guide
            this.choisirGuide();
        }

    }

    //fonction qui permet de changer le contenu des tokens selon quel était le token précédent
    changerContenuTokens(placementX, placementY, type, currentToken, index) {
        //tokens
        let tokensIMG = {
            "blue": this.refGame.global.resources.getImage('dog_brown_sit'),
            "red": this.refGame.global.resources.getImage('dog_brown_jump'),
            "green": this.refGame.global.resources.getImage('dog_brown_bend')
        };
        let shape = null;
        let width = 35;

        if (tableauInfos.guide == "4Tokens") {
            width = 40;
        }

        if (type == "2X" || type == "3X") {
            width = 25;
            switch (currentToken) {
                case "":
                    shape = new ShapeCreate(placementX, placementY, width, width, null, "2X");
                    tableauInfos.solution[index] = "2X";
                    shape.setOnClick(this.changerContenuTokens.bind(this, placementX, placementY, type, tableauInfos.solution[index], index));
                    this.elements.push(shape);
                    break;
                case "2X":
                    shape = new ShapeCreate(placementX, placementY, width, width, null, "3X");
                    tableauInfos.solution[index] = "3X";
                    shape.setOnClick(this.changerContenuTokens.bind(this, placementX, placementY, type, tableauInfos.solution[index], index));
                    this.elements.push(shape);
                    break;
                case "3X":
                    shape = new ShapeCreate(placementX, placementY, width, width, null, "2X");
                    tableauInfos.solution[index] = "2X";
                    shape.setOnClick(this.changerContenuTokens.bind(this, placementX, placementY, type, tableauInfos.solution[index], index));
                    this.elements.push(shape);
                    break;
            }
        } else {
            switch (currentToken) {
                case "":
                    shape = new ShapeCreate(placementX, placementY, width, width, tokensIMG["green"], "green");
                    tableauInfos.solution[index] = "green";
                    shape.setOnClick(this.changerContenuTokens.bind(this, placementX, placementY, type, tableauInfos.solution[index], index));
                    this.elements.push(shape);
                    break;
                case "green":
                    shape = new ShapeCreate(placementX, placementY, width, width, tokensIMG["blue"], "blue");
                    tableauInfos.solution[index] = "blue";
                    shape.setOnClick(this.changerContenuTokens.bind(this, placementX, placementY, type, tableauInfos.solution[index], index));
                    this.elements.push(shape);
                    break;
                case "blue":
                    shape = new ShapeCreate(placementX, placementY, width, width, tokensIMG["red"], "red");
                    tableauInfos.solution[index] = "red";
                    shape.setOnClick(this.changerContenuTokens.bind(this, placementX, placementY, type, tableauInfos.solution[index], index));
                    this.elements.push(shape);
                    break;
                case "red":
                    shape = new ShapeCreate(placementX, placementY, width, width, tokensIMG["green"], "green");
                    tableauInfos.solution[index] = "green";
                    shape.setOnClick(this.changerContenuTokens.bind(this, placementX, placementY, type, tableauInfos.solution[index], index));
                    this.elements.push(shape);
                    break;
            }

        }

        this.init();
    }

    //fonction qui permet de créer le JSON à partir des données du tableau
    createJSON() {

        //Création variable
        let JSONNewExercice = {};
        let image;


        JSONNewExercice = '{"name":{';
        JSONNewExercice += '"fr":"exerciceMedor",';
        JSONNewExercice += '"de":"",';
        JSONNewExercice += '"en":"",';
        JSONNewExercice += '"it":""';
        JSONNewExercice += '},';

        if (tableauInfos.map == "flat_nature") {
            tableauInfos.map = "flatNature";
        }
        JSONNewExercice += '"map":"' + tableauInfos.map + '",';
        JSONNewExercice += '"placements":[';
        //Pour chaque emplacement on va vérifier s'il faut afficher une image
        //Emplacement 0
        image = null;
        JSONNewExercice += '{';
        JSONNewExercice += '"num":"0",';

        if (tableauInfos.dog == "0") {
            image = '"image":["dog_brown"]';
        } else if (tableauInfos.niche == "0") {
            image = '"image":["dog_house"]';
        }
        tableauInfos.biscuits.forEach(biscuit => {
            if (biscuit == "0") {
                image = '"image":["doggy_biscuit"]';
            }
        });
        if (image == null) {
            JSONNewExercice += '"image":[]';
        } else {
            JSONNewExercice += image;
        }
        JSONNewExercice += '},';

        //Emplacement 1
        image = null;
        JSONNewExercice += '{';
        JSONNewExercice += '"num":"1",';

        if (tableauInfos.dog == "1") {
            image = '"image":["dog_brown"]';
        } else if (tableauInfos.niche == "1") {
            image = '"image":["dog_house"]';
        }
        tableauInfos.biscuits.forEach(biscuit => {
            if (biscuit == "1") {
                image = '"image":["doggy_biscuit"]';
            }
        });
        if (image == null) {
            JSONNewExercice += '"image":[]';
        } else {
            JSONNewExercice += image;
        }
        JSONNewExercice += '},';

        //Emplacement 2
        image = null;
        JSONNewExercice += '{';
        JSONNewExercice += '"num":"2",';

        if (tableauInfos.dog == "2") {
            image = '"image":["dog_brown"]';
        } else if (tableauInfos.niche == "2") {
            image = '"image":["dog_house"]';
        }
        tableauInfos.biscuits.forEach(biscuit => {
            if (biscuit == "2") {
                image = '"image":["doggy_biscuit"]';
            }
        });
        if (image == null) {
            JSONNewExercice += '"image":[]';
        } else {
            JSONNewExercice += image;
        }
        JSONNewExercice += '},';

        //Emplacement 3
        image = null;
        JSONNewExercice += '{';
        JSONNewExercice += '"num":"3",';

        if (tableauInfos.dog == "3") {
            image = '"image":["dog_brown"]';
        } else if (tableauInfos.niche == "3") {
            image = '"image":["dog_house"]';
        }
        tableauInfos.biscuits.forEach(biscuit => {
            if (biscuit == "3") {
                image = '"image":["doggy_biscuit"]';
            }
        });
        if (image == null) {
            JSONNewExercice += '"image":[]';
        } else {
            JSONNewExercice += image;
        }
        JSONNewExercice += '},';

        //Emplacement 4
        image = null;
        JSONNewExercice += '{';
        JSONNewExercice += '"num":"4",';

        if (tableauInfos.dog == "4") {
            image = '"image":["dog_brown"]';
        } else if (tableauInfos.niche == "4") {
            image = '"image":["dog_house"]';
        }
        tableauInfos.biscuits.forEach(biscuit => {
            if (biscuit == "4") {
                image = '"image":["doggy_biscuit"]';
            }
        });
        if (image == null) {
            JSONNewExercice += '"image":[]';
        } else {
            JSONNewExercice += image;
        }
        JSONNewExercice += '},';

        //Emplacement 5
        image = null;
        JSONNewExercice += '{';
        JSONNewExercice += '"num":"5",';

        if (tableauInfos.dog == "5") {
            image = '"image":["dog_brown"]';
        } else if (tableauInfos.niche == "5") {
            image = '"image":["dog_house"]';
        }
        tableauInfos.biscuits.forEach(biscuit => {
            if (biscuit == "5") {
                image = '"image":["doggy_biscuit"]';
            }
        });
        if (image == null) {
            JSONNewExercice += '"image":[]';
        } else {
            JSONNewExercice += image;
        }
        JSONNewExercice += '}';

        JSONNewExercice += '],';
        JSONNewExercice += '"tokens":[';

        for (let i = 0; i < tableauInfos.solution.length - 1; i++) {
            JSONNewExercice += '"' + tableauInfos.solution[i] + '",';
        }
        JSONNewExercice += '"' + tableauInfos.solution[tableauInfos.solution.length - 1] + '"';
        JSONNewExercice += '],';
        JSONNewExercice += '"guides":[';

        if (tableauInfos.guide == "5Tokens") {
            tableauInfos.guide = "5TokensLoop";
        }
        JSONNewExercice += '"' + tableauInfos.guide + '",';

        if (tableauInfos.guide == "4Tokens") {

            for (let i = 0; i < tableauInfos.solution.length - 1; i++) {
                JSONNewExercice += '{';
                JSONNewExercice += '"x":"' + XYGuides4Tokens[i][0] + '",';
                JSONNewExercice += '"y":"' + XYGuides4Tokens[i][1] + '",';
                JSONNewExercice += '"value":"' + tableauInfos.solution[i] + '"';
                JSONNewExercice += '},';
            };

            JSONNewExercice += '{';
            JSONNewExercice += '"x":"' + XYGuides4Tokens[tableauInfos.solution.length - 1][0] + '",';
            JSONNewExercice += '"y":"' + XYGuides4Tokens[tableauInfos.solution.length - 1][1] + '",';
            JSONNewExercice += '"value":"' + tableauInfos.solution[tableauInfos.solution.length - 1] + '"';
            JSONNewExercice += '}';

        } else if (tableauInfos.guide == "5TokensLoop") {

            for (let i = 0; i < tableauInfos.solution.length - 1; i++) {
                JSONNewExercice += '{';
                JSONNewExercice += '"x":"' + XYGuides5Tokens[i][0] + '",';
                JSONNewExercice += '"y":"' + XYGuides5Tokens[i][1] + '",';
                JSONNewExercice += '"value":"' + tableauInfos.solution[i] + '"';
                JSONNewExercice += '},';
            };

            JSONNewExercice += '{';
            JSONNewExercice += '"x":"' + XYGuides5Tokens[tableauInfos.solution.length - 1][0] + '",';
            JSONNewExercice += '"y":"' + XYGuides5Tokens[tableauInfos.solution.length - 1][1] + '",';
            JSONNewExercice += '"value":"' + tableauInfos.solution[tableauInfos.solution.length - 1] + '"';
            JSONNewExercice += '}';

        } else if (tableauInfos.guide == "6Tokens") {

            for (let i = 0; i < tableauInfos.solution.length - 1; i++) {
                JSONNewExercice += '{';
                JSONNewExercice += '"x":"' + XYGuides6Tokens[i][0] + '",';
                JSONNewExercice += '"y":"' + XYGuides6Tokens[i][1] + '",';
                JSONNewExercice += '"value":"' + tableauInfos.solution[i] + '"';
                JSONNewExercice += '},';
            };

            JSONNewExercice += '{';
            JSONNewExercice += '"x":"' + XYGuides6Tokens[tableauInfos.solution.length - 1][0] + '",';
            JSONNewExercice += '"y":"' + XYGuides6Tokens[tableauInfos.solution.length - 1][1] + '",';
            JSONNewExercice += '"value":"' + tableauInfos.solution[tableauInfos.solution.length - 1] + '"';
            JSONNewExercice += '}';
        }

        JSONNewExercice += ']';
        JSONNewExercice += '}';

        this.refGame.global.resources.saveExercice(JSONNewExercice, function (result) {
            Swal.fire(
                result.message,
                undefined,
                result.type
            );
            if (result) {
                // On réinitialise toute la page
                this.clear();
                this.elements = [];
                this.show();
            }
        }.bind(this));


    }


}
class Explore extends Interface {

    constructor(refGame) {

        super(refGame, "explore");
        this.refGame = refGame;
        this.currentText = 'gameExplore';
        this.background = new PIXI.Container();
        this.background.width = 600;
        this.background.height = 600;
        let img_background = refGame.global.resources.getImage("flat_nature");
        let sprite_background = PIXI.Sprite.fromImage(img_background.image.src);
        sprite_background.anchor.x = 0.5;
        sprite_background.y = 0;
        sprite_background.x = 300;
        let largeur = 350 / img_background.getHeight() * img_background.getWidth(); 
        sprite_background.height = 350;
        sprite_background.width = largeur;
        this.background.addChild(sprite_background);

        //add background
        this.elements.push(this.background);

        //colors
        this.blue = 0x4f81bd;
        this.green = 0x9bbb59;
        this.red = 0xc0504d;
        this.black = 0x000000;

        this.XYPlacements = [[75,75],[300,75],[525,75],[75,275],[300,275],[525,275]];
        
        //Placements 
        this.firstPlacement = new Placement(this.XYPlacements[0][0], this.XYPlacements[0][1], 0, this.refGame, ['doggy_biscuit']);
        this.secondPlacement = new Placement(this.XYPlacements[1][0], this.XYPlacements[1][1], 1, this.refGame);
        this.thirdPlacement = new Placement(this.XYPlacements[2][0], this.XYPlacements[2][1], 2, this.refGame, ['dog_brown']);
        this.fourthPlacement = new Placement(this.XYPlacements[3][0], this.XYPlacements[3][1], 3, this.refGame);
        this.fifthPlacement = new Placement(this.XYPlacements[4][0], this.XYPlacements[4][1], 4, this.refGame, ['dog_house']);
        this.sixthPlacement = new Placement(this.XYPlacements[5][0], this.XYPlacements[5][1], 5, this.refGame);
        
        //Arrows between Placements
        this.arrow_first_second_red = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[1][0], this.XYPlacements[1][1], this.red);
        this.arrow_first_fifth_blue = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[4][0], this.XYPlacements[4][1], this.blue);
        this.arrow_second_third_blue = new Arrow(this.XYPlacements[1][0], this.XYPlacements[1][1], this.XYPlacements[2][0], this.XYPlacements[2][1], this.blue);
        this.arrow_second_fouth_green = new Arrow(this.XYPlacements[1][0], this.XYPlacements[1][1], this.XYPlacements[3][0], this.XYPlacements[3][1], this.green );
        this.arrow_fourth_fifth_red = new Arrow(this.XYPlacements[3][0], this.XYPlacements[3][1], this.XYPlacements[4][0], this.XYPlacements[4][1], this.red );
        this.arrow_fifth_sixth_green = new Arrow(this.XYPlacements[4][0], this.XYPlacements[4][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.green);
        this.arrow_first_third_green = new Arrow(this.XYPlacements[0][0], this.XYPlacements[0][1], this.XYPlacements[2][0], this.XYPlacements[2][1], this.green, false, [100,0,500,0]);
        this.arrow_fourth_sixth_blue = new Arrow(this.XYPlacements[3][0], this.XYPlacements[3][1], this.XYPlacements[5][0], this.XYPlacements[5][1], this.blue, false, [100,350,500,350]);

        //add arrows between Placements
        this.elements.push(this.arrow_first_second_red, this.arrow_first_fifth_blue, this.arrow_second_third_blue, this.arrow_second_fouth_green, this.arrow_fourth_fifth_red, this.arrow_fifth_sixth_green, this.arrow_first_third_green, this.arrow_fourth_sixth_blue);
      
        //add placements
        this.elements.push(this.firstPlacement, this.secondPlacement, this.thirdPlacement, this.fourthPlacement, this.fifthPlacement, this.sixthPlacement);
        
        //Guides
        this.firtGuide = new Guide(165, 400, 40, "green");
        this.secondGuide = new Guide(255, 400, 40, "blue");
        this.thirdGuide = new Guide(345, 400, 40, "red");
        this.fourthGuide = new Guide(435, 400, 40, "red");
        
        //Arrow under guides
        this.arrowUnderGuides = new Arrow(100, 400, 550, 400, this.black, true);

        //add arrow under guides
        this.elements.push(this.arrowUnderGuides)

        //add guides
        this.elements.push(this.firtGuide, this.secondGuide, this.thirdGuide, this.fourthGuide);

        //other images
        this.dogBrown = new Shape(50, 400, 50, 50, this.refGame.global.resources.getImage('dog_brown'), '');
        this.dogHouse = new Shape(550, 400, 50, 50, this.refGame.global.resources.getImage('dog_house'), '');

        //Rect and arrow for tutorial
        this.rect = new PIXI.Graphics();
        this.triangle = new PIXI.Graphics();
        this.arrow_tokens_guides_Red = new Arrow(477, 550, 465, 430, this.red, true, [570,500,472,420]);
        this.arrow_tokens_guides_Red.setVisible(false);

        //add Rect and arrow for tutorial
        this.elements.push(this.rect, this.triangle, this.arrow_tokens_guides_Red);
        
        //tokens
        this.dogBrownSit = new Shape(0, 0, 40, 40, this.refGame.global.resources.getImage('dog_brown_sit'), 'blue', true, this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));
        this.dogBrownBend = new Shape(0, 0, 40, 40, this.refGame.global.resources.getImage('dog_brown_bend'), 'green', true, this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));
        this.dogBrownJump = new Shape(0, 0, 40, 40, this.refGame.global.resources.getImage('dog_brown_jump'), 'red', true, this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));
        this.dogBrownJumpSecond = new Shape(0, 0, 40, 40, this.refGame.global.resources.getImage('dog_brown_jump'), 'red', true, this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));

        this.dogBrownSit.setStopMove(true);
        this.dogBrownBend.setStopMove(true);
        this.dogBrownJump.setStopMove(true);
        this.dogBrownJumpSecond.setStopMove(true);

        this.shapes = [this.dogBrownSit, this.dogBrownJumpSecond, this.dogBrownBend, this.dogBrownJump];
        this.cells = [this.firtGuide, this.secondGuide, this.thirdGuide, this.fourthGuide];
         
        //add tokens
        this.elements.push(this.dogBrownSit, this.dogBrownJumpSecond, this.dogBrownBend, this.dogBrownJump);

        //add others images
        this.elements.push(this.dogBrown, this.dogHouse);

        let startBtn = new PIXI.Graphics();
            startBtn.beginFill(0x4bacc6, 1);
            startBtn.lineStyle(2, 0x357d91, 1);  
            startBtn.moveTo(35, 385)       
            startBtn.lineTo(65, 400)
            startBtn.lineTo(35, 415)
            startBtn.lineTo(35, 385);
            startBtn.endFill();
            startBtn.interactive = true;
            startBtn.on('pointerdown', this.onClick.bind(this));

        //add startBtn
        this.elements.push(startBtn);
        this.numAttemps = 0;

        //variables for tutorial
        this.XYRectangle = [[10, 10, 580, 330],[120,350,360,100],[120,505,360,90],[252,210,115,115],[120,505,360,90],[10,360,80,80]];
        this.texts = ["map","guide","token","kennel","tokensToGuide","play"];
        this.position = 0;       
        this.allTokensInGuide = true;
    }

    onClick(){
        if(this.currentText === "play"){
            this.rect.x = 0;
            this.rect.y = 0;
            document.getElementById("next").style.visibility = "hidden"; 
        }
        let juste = 0;
        let allFull = true;
        for (let i = 0; i < this.cells.length; i++) {
            let cell = this.cells[i];
            if(cell.getShape() != null){
                if(cell.getValue() === cell.getShape().getName()){
                    juste++;
                }
            }else{
                allFull = false;
            }
        }
        if(juste === this.cells.length){
            this.numAttemps++;
            this.endGame();
        }else if(allFull){
            this.numAttemps++;
            this.refGame.global.util.showAlert(
                //Type d'alerte
                'error',
                //Texte
                this.refGame.global.resources.getOtherText('blocked'),
                //Titre
                this.refGame.global.resources.getOtherText('error'),
                //Footer
                undefined,
                //Callback
                undefined
            );
        }else{
            this.refGame.global.util.showAlert(
                //Type d'alerte
                'error',
                //Texte
                this.refGame.global.resources.getOtherText('notFull'),
                //Titre
                this.refGame.global.resources.getOtherText('error'),
                //Footer
                undefined,
                //Callback
                undefined
            );
        }
    }

    show() {
        this.refGame.global.listenerManager.addListenerOn(document.getElementById('next'), ['click'], this.handleNext.bind(this));
        this.refGame.global.util.showTutorialInputs('next', 'btnReset');
        this.clear();
        this.initGame();
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    handleNext() {
        let ok = false;
        if(this.position < this.XYRectangle.length){
            this.rect.clear();
            this.rect.lineStyle(6, this.red, 1)
                .drawRoundedRect(this.XYRectangle[this.position][0], this.XYRectangle[this.position][1], this.XYRectangle[this.position][2], this.XYRectangle[this.position][3], 30);
            this.currentText = this.texts[this.position];
            if(this.currentText === "tokensToGuide"){
                if(this.allTokensInGuide){
                    this.triangle.beginFill(this.red, 1)
                        .lineStyle(1, this.red, 1)
                        .moveTo(457,423)
                        .lineTo(478,421)
                        .lineTo(471,440)
                        .lineTo(457,423)
                        .endFill();
                    this.arrow_tokens_guides_Red.setVisible(true);
                    this.allTokensInGuide = false;
                }
                this.dogBrownSit.setStopMove(false);
                this.dogBrownBend.setStopMove(false);
                this.dogBrownJump.setStopMove(false);
                this.dogBrownJumpSecond.setStopMove(false);
            }else{
                this.triangle.clear();
                this.arrow_tokens_guides_Red.setVisible(false);
            }
        }else{
            this.rect.x = 0;
            this.rect.y = 0;
            this.rect.width = 0;
            this.rect.height = 0;
        }
        if(this.allTokensInGuide){
            this.position++;
            document.getElementById("next").style.visibility = "visible"; 
        }else{
            this.position++;
            document.getElementById("next").style.visibility = "hidden"; 
        }
        if(this.currentText === "play"){
            document.getElementById("next").style.visibility = "hidden"; 
        }
        if(ok){
            this.handleNext();
        }
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
    }

    refreshLang(lang) {
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
    }

    refreshFont(isOpenDyslexic){

    }

    onShapeClick(shape) {
        Swal.fire(this.refGame.global.resources.getOtherText(shape.name), undefined, 'info');
    }

    onShapeMove(shape) {
        for (let i = 0; i < this.cells.length; i++) {
            let cell = this.cells[i];
            let minX = cell.x - cell.width / 1.7;
            let maxX = cell.x + cell.width / 1.7;
            let minY = cell.y - cell.width / 1.7;
            let maxY = cell.y + cell.width / 1.7;

            if(cell.getShape() === shape && !(minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY())){
                this.cells[i].setShape(null);
            }
            
            if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY() && (this.cells[i].getShape() === shape || !this.cells[i].getShape())) {
                shape.setX(cell.x);
                shape.setY(cell.y);
                this.cells[i].setShape(shape);
            }
        }
    }

    onShapeEndMove(shape){
        for (let i = 0; i < this.cells.length; i++) {
            let cell = this.cells[i];
            let minX = cell.x - cell.width / 1.7;
            let maxX = cell.x + cell.width / 1.7;
            let minY = cell.y - cell.width / 1.7;
            let maxY = cell.y + cell.width / 1.7;
            if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY()){
                shape.setX(cell.x);
                shape.setY(cell.y);
                if(this.cells[i].getShape() != null){
                    if(this.cells[i].getShape() !== shape){
                        this.cells[i].getShape().setX(this.cells[i].getShape().getBaseX());
                        this.cells[i].getShape().setY(this.cells[i].getShape().getBaseY());
                    }
                }
                this.cells[i].setShape(shape);
            }
        }
        let allIN = true;
        for(let i = 0; i < this.cells.length; i++){
            if(this.cells[i].getShape() == null){
                allIN = false;
            }
        }
        if(this.currentText === "tokensToGuide" && allIN){
            document.getElementById("next").style.visibility = "visible";
            this.allTokensInGuide = true;
        } 
    }

    initGame() {
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
        this.numAttemps = 0;
        for (let i = 0; i < this.cells.length; i++) {
            this.cells[i].setShape(null);
        }

        let xy = [165,255,345,435];
        for (let i = 0; i < this.shapes.length; i++) {
            let random = Math.floor(Math.random()*xy.length);
            this.shapes[i].setBaseX(xy[random]);
            this.shapes[i].setBaseY(550);
            xy.splice(random, 1);
        }
        //Réinitialiser le timer
        this.timer = undefined;
        this.end = false;
    }
    
    endGame() {
        if (!this.end) {
            let text = this.refGame.global.resources.getOtherText('victory', {count: this.numAttemps});
            this.refGame.global.util.showAlert(
                //Type d'alerte
                'success',
                //Texte
                text,
                //Titre
                this.refGame.global.resources.getOtherText('success'),
                //Footer
                undefined,
                //Callback
                this.initGame.bind(this)
            );
        }
        this.end = true;
    }

}
/**
 * @classdesc IHM pour tester les exercices non valides
 * @author Léo Niederhauser
 * @version 1.0
 */

class Play extends Interface {

    /**
     * Constructeur de l'ihm de la création d'exercice
     * @param {Game} refGame la référence vers la classe de jeu
     */

    constructor(refGame) {

        super(refGame, "play");

    }

    /**
    * Point d'entrée pour l'affichage
    */
    show() {
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText('play'));
        this.exeSelected = -1;
        this.clear();
        this.exercices = {};
        this.elements = {
            title: new PIXI.Text('', { fontFamily: 'Arial', fontSize: 16, fill: 0x000000, align: 'left' }),
            scrollPane: new ScrollPane(0, 60, 600, 480),
            btn: new Button(300, 570, 'Jouer', 0x0088FF, 0x000000, true)
        };
        this.elements.btn.setOnClick(function () {

            if (this.exeSelected != -1) {
                this.refGame.trainMode.init(false, 'playMode');
                this.refGame.trainMode.setRefPlay(this);
                this.refGame.trainMode.show(this.exeSelected, this.exercice[this.exeSelected].exercice);
            }
        }.bind(this));
        this.elements.title.anchor.set(0.5);
        this.elements.title.x = 300;
        this.elements.title.y = 30;
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }
    init() {
        this.refGame.global.util.hideTutorialInputs('btnReset');
        super.init();
    }


    startExercices(lang) {
        //choose exe
        this.exercice = this.refGame.global.resources.getExercicesEleves();
        let tg = new ToggleGroup('single');
        if (this.exercice) {
            this.exercice = JSON.parse(this.exercice);
            for (let exID of Object.keys(this.exercice)) {
                let ex = this.exercice[exID];
                if (ex.exercice) {
                    let label = (ex.name);
                    let btn = new ToggleButton(0, 0, label, false, 580);
                    btn.enable();
                    tg.addButton(btn);
                    btn.setOnClick(function () {
                        this.exeSelected = exID;
                    }.bind(this));
                    this.elements.scrollPane.addElements(btn);
                }

            }
            this.elements.scrollPane.init();
        }

    }


    ///////////////////////
    refreshLang(lang) {
        this.startExercices(lang)
    }



}
class Train extends Interface {

    constructor(refGame) {
        super(refGame, "train");
        this.refGame = refGame;
        this.evaluate = false;
        this.background = new PIXI.Container();
        this.background.width = 600;
        this.background.height = 600;
        this.numAttemps = 0;
        this.evaluate = false;
        this.level = null;
        this.title = new PIXI.Text('', { fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center' });
        this.currentText = 'gameTrain';
    }

    setRefPlay(refPlay) {
        this.refPlay = refPlay
    }


    show(evaluate, id, exercice) {

        this.evaluate = evaluate;
        this.level = this.refGame.global.resources.getExercice();
        if (exercice == undefined) {
            this.currentExerciceId = this.level.id;
            this.level = JSON.parse(this.level.exercice);
        } else {
            this.currentExerciceId = id;
            this.level = JSON.parse(JSON.stringify(exercice));
        }




        this.refGame.global.util.hideTutorialInputs('next', 'btnReset');
        this.clear();
        this.elements.splice(0, this.elements.length);
        this.initGame();
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
        this.setupSignalement();
    }

    initGame() {
        this.map = null;
        this.guide = null;
        if (this.evaluate) {
            this.refGame.global.statistics.addStats(2);
        }
        switch (this.level["map"]) {
            case "flatNature":
                this.map = new FlatNature(this.refGame, this.level);
                break;
            case "ocean":
                this.map = new Ocean(this.refGame, this.level);
                break;
            case "beach":
                this.map = new Beach(this.refGame, this.level);
                break;
        }

        switch (this.level["guides"][0]) {
            case "4Tokens":
                this.guide = new Guide4Tokens(this.refGame, this.level, this.endGame.bind(this));
                break;
            case "6Tokens":
                this.guide = new Guide6Tokens(this.refGame, this.level, this.endGame.bind(this));
                break;
            case "5TokensLoop":
                this.guide = new Guide5TokensLoop(this.refGame, this.level, this.endGame.bind(this));
                break;
        }

        this.elements.push(this.map, this.guide);
        this.end = false;
    }

    endGame(numAttemps) {
        this.numAttemps = numAttemps;
        if (!this.end) {
            let text = this.refGame.global.resources.getOtherText('victory', { count: this.numAttemps });
            this.end = true;
            //calcule le nombre de points
            if (this.evaluate) {
                let mark = 0;
                let barem = JSON.parse(this.refGame.global.resources.getEvaluation()).notes;
                for (let b of barem) {
                    if (b.min <= this.numAttemps && b.max >= this.numAttemps) {
                        mark = b.note;
                    }
                }
                this.refGame.global.statistics.updateStats(mark);
            }

            this.refGame.global.util.showAlert(
                //Type d'alerte
                'success',
                //Texte
                text,
                //Titre
                this.refGame.global.resources.getOtherText('success'),
                //Footer
                undefined,
                //Callback
                this.show(this.evaluate)
            );
        }
    }


    refreshLang(lang) {
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText(this.currentText));
    }

    setupSignalement() {
        this.refGame.inputs.signal.style.display = 'block';
        this.refGame.inputs.signal.onclick = function () {
            Swal.fire({
                title: this.refGame.global.resources.getOtherText('signalementTitle'),
                cancelButtonText: this.refGame.global.resources.getOtherText('signalementCancel'),
                showCancelButton: true,
                html:
                    '<label>' + this.refGame.global.resources.getOtherText('signalementReason') + '</label><textarea id="swal-reason" class="swal2-textarea"></textarea>' +
                    '<label>' + this.refGame.global.resources.getOtherText('signalementPassword') + '</label><input type="password" id="swal-password" class="swal2-input">',
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        resolve([
                            $('#swal-reason').val(),
                            $('#swal-password').val()
                        ])
                    })
                },
                onOpen: function () {
                    $('#swal-reason').focus()
                }
            }).then(function (result) {
                if (result && result.value) {
                    this.refGame.global.resources.signaler(result.value[0], result.value[1], this.currentExerciceId, function (res) {
                        Swal.fire(res.message, '', res.type);
                        if (res.type === 'success') {
                            this.init(this.evaluation);
                            this.show();
                        }
                    }.bind(this));
                }
            }.bind(this));
        }.bind(this);
    }
}
/**
 * @classdesc Représente un mode de jeu
 * @author Vincent Audergon
 * @version 1.0
 */
class Mode {

    /**
     * Constructeur d'un mode de jeu
     * @param {string} name le nom du mode de jeu (référencé dans le JSON de langues)
     */
    constructor(name) {
        this.name = name;
        this.interfaces = [];
        this.texts = [];
    }

    /**
     * Défini la liste des interfaces utilisées par le mode de jeu
     * @param {Interface[]} interfaces la liste des interfaces
     */
    setInterfaces(interfaces) {
        this.interfaces = interfaces;
    }

    /**
     * Ajoute une interface au mode de jeu
     * @param {string} name le nom de l'interface
     * @param {Interface} inter l'interface
     */
    addInterface(name, inter) {
        this.interfaces[name] = inter;
    }

    /**
     * Retourne un texte dans la langue courrante
     * @param {string} key la clé du texte
     * @return {string} le texte
     */
    getText(key){
        return this.texts[key];
    }

    /**
     * Méthode de callback appelée lors d'un changement de langue.
     * Indique à toutes les interfaces du mode de jeu de changer les textes en fonction de la nouvelle langue
     * @param {string} lang la langue
     */
    onLanguageChanged(lang) {
        this.texts = this.refGame.global.resources.getOtherText(this.name)
        for (let i in this.interfaces) {
            let inter = this.interfaces[i];
            if (inter instanceof Interface) {
                inter.setTexts(this.texts);
                inter.refreshLang(lang);
            }
        }
    }

}

class CreateMode extends Mode{

    constructor(refGame){
        super('create');
        this.refGame = refGame;
        this.setInterfaces({
            create: new Create(refGame)
        });
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init(){
    }

    /**
     * Affiche la page de garde de la création
     */
    show(){
        this.interfaces.create.show();
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.interfaces.create.refreshLang(lang);
    }


    onFontChange(isOpendyslexic){
        this.interfaces.create.refreshFont(isOpendyslexic)
    }
}

class ExploreMode extends Mode{

    constructor(refGame){
        super('explore');
        this.refGame = refGame;
        this.setInterfaces({
            explore: new Explore(refGame)
        });
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init(){
    }

    /**
     * Affiche la page de garde de la création
     */
    show(){
        this.interfaces.explore.show();
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.interfaces.explore.refreshLang(lang);
    }


    onFontChange(isOpendyslexic){
        this.interfaces.explore.refreshFont(isOpendyslexic)
    }
}

class PlayMode extends Mode {

    constructor(refGame) {
        super('play');
        this.refGame = refGame;
        this.setInterfaces({
            play: new Play(refGame)
        });
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init() {
    }

    /**
     * Affiche la page de garde de la création
     */
    show() {
        this.interfaces.play.show();
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.interfaces.play.refreshLang(lang);
    }


    onFontChange(isOpendyslexic) {
        this.interfaces.play.refreshFont(isOpendyslexic)
    }
}

class TrainMode extends Mode {

    /**
     * Constructeur du mode exploration
     * @param {Game} refGame La référence vers la classe de jeu
     */
    constructor(refGame) {
        super('train');
        this.refGame = refGame;
        this.evaluate = false;
        this.setInterfaces({ train: new Train(refGame) })
    }

    /**
     * Initialise le mode de jeu et charge les polygones
     */
    init(evaluate) {
        this.evaluate = evaluate;
    }

    /**
     * Affiche le mode explorer dans le mode "dessin" ou "découverte"
     * @param {boolean} draw si c'est le mode dessin qui doit être affiché
     */
    show(id = undefined, exercice = undefined) {
        this.interfaces.train.show(this.evaluate, id, exercice);
    }

    setRefPlay(refPlay) {
        this.interfaces.train.setRefPlay(refPlay);
    }

    quit() {
        this.interfaces.train.quit();
    }
}
/**
 * @classdesc Classe principale du jeu
 * @author Vincent Audergon
 * @version 1.0
 */

const VERSION = 1.0;

class Game {

    /**
     * Constructeur du jeu
     * @param {Object} global Un objet contenant les différents objets du Framework
     */
    constructor(global) {
        this.global = global;
        this.global.util.callOnGamemodeChange(this.onGamemodeChanged.bind(this));
        this.global.resources.callOnLanguageChange(this.onLanguageChanged.bind(this));
        this.global.resources.callOnFontChange(this.onFontChange.bind(this));
        this.scenes = {
            explore:new PIXI.Container(),
            train:new PIXI.Container(),
            create:new PIXI.Container(),
            play:new PIXI.Container()
        };
        this.inputs = {
            signal: document.getElementById('signal')
        };
        this.exploreMode = new ExploreMode(this);
        this.trainMode = new TrainMode(this);
        this.createMode = new CreateMode(this);
        this.playMode = new PlayMode(this);
        this.oldGamemode = undefined;
    }

    /**
     * Affiche une scène sur le canvas
     * @param {PIXI.Container} scene La scène à afficher
     */
    showScene(scene) {
        for (let input in this.inputs) {
            this.inputs[input].style.display = 'none';
        }
        this.reset();
        this.global.pixiApp.stage.addChild(scene);
    }

    /**
     * Retire toutes les scènes du stage PIXI
     */
    reset() {
        for (let scene in this.scenes) {

            this.global.pixiApp.stage.removeChild(this.scenes[scene])
        }
    }

    /**
     * Affiche du texte sur la page (à gauche du canvas)
     * @param {string} text Le texte à afficher
     */
    showText(text) {
        this.global.util.setTutorialText(text);
    }

    /**
     * Fonction de callback appelée lors d'un chagement de langue.
     * Indiques à tous les modes de jeu le changement de langue
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.exploreMode.onLanguageChanged(lang);
        this.trainMode.onLanguageChanged(lang);
        this.createMode.onLanguageChanged(lang);
        this.playMode.onLanguageChanged(lang);
    }


    onFontChange(isOpendyslexic) {
        this.createMode.onFontChange(isOpendyslexic);
        this.exploreMode.onFontChange(isOpendyslexic);
        this.trainMode.onFontChange(isOpendyslexic);
        this.playMode.onFontChange(isOpendyslexic);
    }

    /**
     * Listener appelé par le Framework lorsque le mode de jeu change.
     * Démarre le bon mode de jeu en fonction de celui qui est choisi
     * @async
     */
    async onGamemodeChanged() {
        let gamemode = this.global.util.getGamemode();
        switch (gamemode) {
            case this.global.Gamemode.Evaluate:
                this.trainMode.init(true);
                this.trainMode.show();
                break;
            case this.global.Gamemode.Train:
                this.trainMode.init(false);
                this.trainMode.show();
                break;
            case this.global.Gamemode.Explore:
                this.exploreMode.init();
                this.exploreMode.show();
                break;
            case this.global.Gamemode.Create:
                this.createMode.init();
                this.createMode.show();
                break;
            case this.global.Gamemode.Play:
                this.playMode.init();
                this.playMode.show();
                break;
        }
        this.oldGamemode = gamemode;
    }
}
const global = {};
global.canvas = Canvas.getInstance();
global.Log = Log;
global.util = Util.getInstance();
global.pixiApp = new PixiFramework().getApp();
global.resources = Resources;
global.statistics = Statistics;
global.Gamemode = Gamemode;
global.listenerManager = ListenerManager.getInstance();
const game = new Game(global);
